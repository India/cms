/*======================================================================================================================================================================
                                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                                         Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_pff_operations.hxx

Description:  Header File containing declarations of function present in lmi_cms_pff_operations.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-Sept-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_PFF_OPERATIONS_HXX
#define LMI_PFF_OPERATIONS_HXX

int LMI_process_pff_operation
(
    char* pszOperation,
    char* pszPffName,
    char* pszExportPffName,
    char* pszFileName,
    char* pszFilePath
);

int LMI_create_file_for_pff_name
(
    char* pszFileName,
    char* pszFilePath
);

int LMI_export_pff
(
    char* pszPffName,
    char* pszFilePath
);

int LMI_obj_get_query_name
(
    tag_t  ptReferencers,
    char** ppszQueryName,
    char** ppszQueryDesc,
    char** ppszQueryClass,
    char*** pppszQueryClauses
);

int LMI_parse_xml_file_and_fill_structures
(
    char* pszFilePath,
    char* pszName,
    char* pszClass,
    char* pszDesc,
    char* pszClause,
    char* pszQueryName,
    char* pszQueryClass,
    char* pszQueryClauses,
    char* pszQueryDesc
);

int  LMI_save_xml_file
(
    xercesc_2_8::DOMDocument* domDoc,
    const char* filePath
);

#endif
