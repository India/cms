/*======================================================================================================================================================================
													Copyright 2020  LMTec India Consulting Pvt Ltd
													Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_errors.hxx

Description:  Header File for containing macros for custom errors

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-May-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/

#ifndef LMI_ERRORS_HXX
#define LMI_ERRORS_HXX

#include <common/emh_const.h>

#define ERROR_CODE_NOT_STORED_ON_STACK                              (EMH_USER_error_base + 1)
#define INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME                   (EMH_USER_error_base + 2)
#define ERROR_PARSING_XML_FILE                                      (EMH_USER_error_base + 3)
#define ERROR_HARDWARE_TAG_NOT_FOUND                                (EMH_USER_error_base + 4)

#endif


