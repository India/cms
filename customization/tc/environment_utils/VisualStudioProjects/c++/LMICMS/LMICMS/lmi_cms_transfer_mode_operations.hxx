/*======================================================================================================================================================================
                                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_transfer_mode_operations.hxx

Description:  Header File containing declarations of function present in lmi_cms_transfer_mode_operations.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-July-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_TRANSFER_MODE_OPERATIONS_HXX
#define LMI_TRANSFER_MODE_OPERATIONS_HXX

int LMI_process_transfer_mode_operation
(
    char* pszOperation,
    char* pszFileName,
    char* pszFilePath
);

int LMI_create_file_for_transfer_mode_name
(
    char* pszFileName,
    char* pszFilePath
);

#endif