/*======================================================================================================================================================================
													Copyright 2020  LMTec India Consulting Pvt Ltd
													Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_queryopeartions.cxx

Description:  This File contains custom functions for Query operations..

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-May-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#include <lmi_cms_library.hxx>
#include <lmi_cms_string_macros.hxx>
#include <lmi_cms_itk_mem_free.hxx>
#include <lmi_cms_itk_errors.hxx>
#include <lmi_cms_const.hxx>
#include <lmi_cms_query_operations.hxx>

/**
 * 
 * This function is used for query related opertaions which read all arguments value stored as input arg
 *  variables and passes to other functions.
 *
 * @param[in] pszOperation       Operation name 
 * @param[in] pszQueryname       Internal name of the queries      
 * @param[in] pszExportQueryname Display name of the queries
 * @param[in] pszFilename        Output Filename
 * @param[in] pszFilepath        Output Filepath
 *
 * @retval ITK_ok If everything went okay.
 */
int LMI_process_query_operation
(
	char* pszOperation,
	char* pszQueryName,
	char* pszExportQueryName,
	char* pszFileName,
	char* pszFilePath
)
{
	int    iRetCode              = ITK_ok;
	int    iQueryNameCount       = 0;
	int    iQueryDispNameCount   = 0;
	char** ppszQueryNameList     = NULL;
	char** ppszQueryDispNameList = NULL;
	
	if (tc_strcmp(pszOperation, LMI_EXPORTQUERYNAME) == 0)
	{	
		LMI_ITKCALL(iRetCode, LMI_create_file_for_query_name(pszFileName, pszFilePath));
	}
	else if(tc_strcmp(pszOperation, LMI_OPERATION_EXPORT) == 0)
	{
		LMI_ITKCALL(iRetCode, LMI_store_query_name(pszQueryName, pszExportQueryName, LMI_STRING_COMMA, &iQueryNameCount, &ppszQueryNameList, &iQueryDispNameCount, &ppszQueryDispNameList));

		LMI_ITKCALL(iRetCode, LMI_print_query_name_and_uid(iQueryNameCount, iQueryDispNameCount, pszFileName, pszFilePath, ppszQueryNameList, ppszQueryDispNameList));
	}
    
    LMI_MEM_TCFREE(ppszQueryNameList);
    LMI_MEM_TCFREE(ppszQueryDispNameList);
	return iRetCode;
}

/**
 *
 * This function stores all querynames and querydisplaynames in list.It tokenises QueryName and ExportQueryNames
 * with the seperator used.
 * List of QueryNames and QueryDisplayNames are provided as output from this function.
 * 
 * @param[in]    pszQueryName             Querynames
 * @param[in]    pszExportQueryName       Querydisplaynames
 * @param[in]    pszSeparator             Seperator used to tokenise querynames and exportquerynames
 * @param[out]   piQueryNameCount         Count of query internal name
 * @param[out/f] pppszQueryNamelist       List of querynames
 * @param[out]   piQueryDispNameCount     Count of query Display name
 * @param[out/f] pppszQueryDispNamelist   List of querydisplaynames
 *
 *
 * @retval ITK_ok If everything went okay.
 */
int LMI_store_query_name
(
	char*   pszQueryName,
    char*   pszExportQueryName,
	char*   pszSeparator,
    int*    piQueryNameCount,
    char*** pppszQueryNamelist,
    int*    piQueryDispNameCount,
	char*** pppszQueryDispNamelist
)
{
	int   iRetCode              = ITK_ok;
	int   iCounter              = 0;
	char* pszQueryNameToken     = NULL;
	char* pszQueryDispNameToken = NULL;


	/* Validate input */
	if (pszQueryName == NULL || pszSeparator == NULL || pszExportQueryName == NULL)
	{
		TC_write_syslog("ERROR: Missing input data: List passed or seprator is null\n");
	}
	else
	{
		/* Get the first token */
		pszQueryNameToken = tc_strtok(pszQueryName, pszSeparator);

		/* Tokenize the input string */
		while (pszQueryNameToken != NULL)
		{
			LMI_UPDATE_STRING_ARRAY((*piQueryNameCount), (*pppszQueryNamelist), pszQueryNameToken);

			/* Get next token: */
			pszQueryNameToken = tc_strtok(NULL, pszSeparator);
		}

		pszQueryDispNameToken = tc_strtok(pszExportQueryName, pszSeparator);

		while (pszQueryDispNameToken != NULL)
		{
            /*Fill this in tag array*/
			LMI_UPDATE_STRING_ARRAY((*piQueryDispNameCount), (*pppszQueryDispNamelist), pszQueryDispNameToken);

			/* Get next token: */
			pszQueryDispNameToken = tc_strtok(NULL, pszSeparator);
		}
	}

	/* Free the temporary memory used for tokenizing */
    LMI_MEM_TCFREE(pszQueryNameToken);
    LMI_MEM_TCFREE(pszQueryDispNameToken);
	return iRetCode;
}

/**
*
* The function prints all display names of querynames and uids in file.This function firstly provides tag of the query
* name and converts this tag to uid of the specified queryname.
* These uid's are filled in vector of string  along and querydisplaynames are also filled in seperate vecctor of
* string.
*
* @param[in] iQueryNameCount        Count of query internal name
* @param[in] iQueryDispNameCount    Count of query Display name
* @param[in] pszFilename            Name of the file to be created to print querynames and uid
* @param[in] pszFileloc             Location of file
* @param[in] ppszQueryNamelist      List of querynames
* @param[in] pppszQueryDispNamelist List of querydisplaynames
*
* @retval ITK_ok If everything went okay.
*/
int LMI_print_query_name_and_uid
(
	int    iQueryNameCount,
	int    iQueryDispNameCount,
	char*  pszFileName,
	char*  pszFileLoc,
	char** ppszQueryNameList,
	char** ppszQueryDispNameList   
)
{
    int    iRetCode      = ITK_ok;
    int    iUIDSize     = 0;
    tag_t  tQuery        = NULLTAG;
   
    vector<string> vExpQueryNames;
    vector<string> vExpQueryUid;

	for (int iDx = 0; iDx < iQueryNameCount && iRetCode == ITK_ok; iDx++) 
	{
        char* pszUid = NULL;
        tag_t tQuery = NULLTAG;
       
        /* Searches the given Queryname and returns its tag*/
		LMI_ITKCALL(iRetCode, QRY_find2(ppszQueryNameList[iDx], &tQuery));

        LMI_ITKCALL(iRetCode, POM_tag_to_uid(tQuery, &pszUid));

		if (iRetCode == ITK_ok)
		{
            vExpQueryNames.push_back(ppszQueryDispNameList[iDx]);

            vExpQueryUid.push_back(pszUid);   
		}
        
        LMI_MEM_TCFREE(pszUid);
	}

	/*file write operations*/
    LMI_ITKCALL(iRetCode, LMI_write_name_and_uid_to_file(vExpQueryNames, vExpQueryUid, pszFileLoc, pszFileName));
   
	return iRetCode;
}

/**
*
* This function executes the saved query with specified user entries.TAg of the queries are fetched for specified
* Querynames and no of found objects and object tags are returned.
*
* @param[in] iUserEntryCount      Count of user entries
* @param[in] piNoOfObjFound       Count of no of objects found
* @param[in] pszQueryName         Queryname
* @param[in] ppszUserEntriesName  Userentriesname
* @param[in] ppszUserEntriesValue Userentriesvalue
* @param[in] ptObjTag             Tag of object
*
* @retval ITK_ok If everything went okay.
*/
int LMI_execute_saved_query
(
	int     iUserEntryCount,
	int*    piNoOfObjFound,
	char*   pszQueryName,
	char**  ppszUserEntriesName,
	char**  ppszUserEntriesValue,
	tag_t** ptObjTag
)
{
	int iRetCode = ITK_ok;
    tag_t tQuery = NULLTAG;
	
    if (tc_strlen(pszQueryName) > 0)
    {
        /* Finding Query tag */
        LMI_ITKCALL(iRetCode, QRY_find2(pszQueryName, &tQuery));

        /* Executing query */
        LMI_ITKCALL(iRetCode, QRY_execute(tQuery, iUserEntryCount, ppszUserEntriesName, ppszUserEntriesValue, piNoOfObjFound, ptObjTag));
    }

	return iRetCode;
}

/**
*
* This function creates file  for querynames. Filename and Filepath are used as input argument to create file filled
* with querynames derived from the sysytem at the specified location coming in FilePath.
*
* @param[in] pszFilename Filename to write querynames
* @param[in] pszFilepath Location of file used for file creation
*
* @retval ITK_ok If everything went okay.
*/
int LMI_create_file_for_query_name
(
	char* pszFileName,
	char* pszFilePath
)
{  
	int    iRetCode           = ITK_ok;
	int	   iUserEntryValCnt   = 0;
	int	   iUserEntryNo       = 0;
	int	   iQueryResultCnt    = 0;
    tag_t  tQuery             = NULLTAG;
    tag_t* ptQueryResults     = NULL;
	char** ppszUserEntryName  = NULL;
	char** ppszUserEntryValue = NULL;
	
    vector<string> vQueryNames;

	if (pszFileName == NULL || pszFilePath == NULL)
	{
		TC_write_syslog("ERROR: Missing input data: Filename or Filepath is empty\n");
	}
	else
	{
        /*Searches for specified Queryname and returns its tag*/
		LMI_ITKCALL(iRetCode, QRY_find2(LMI_ASK_QUERY_NAME, &tQuery));

        /*Find all user entries of specified query*/
        LMI_ITKCALL(iRetCode, QRY_find_user_entries(tQuery, &iUserEntryValCnt, &ppszUserEntryName, &ppszUserEntryValue));

        if (iRetCode == ITK_ok)
        {
            /*Fill this in tag array*/
            LMI_UPDATE_STRING_ARRAY(iUserEntryNo, ppszUserEntryValue, LMI_ALL_VALUES);
        }
        /*Execute queries*/
        LMI_ITKCALL(iRetCode, LMI_execute_saved_query(iUserEntryNo, &iQueryResultCnt, LMI_ASK_QUERY_NAME, ppszUserEntryName, ppszUserEntryValue, &ptQueryResults));

        for (int iDx = 0; iDx < iQueryResultCnt && iRetCode == ITK_ok; iDx++)
        {
            char* pszName = NULL;

            LMI_ITKCALL(iRetCode, QRY_ask_name2(ptQueryResults[iDx], &pszName));

            if (iRetCode == ITK_ok)
            {
                vQueryNames.push_back(pszName);
            }

            LMI_MEM_TCFREE(pszName);
        }

        /*File write operations*/
        LMI_ITKCALL(iRetCode, LMI_write_data_to_file(vQueryNames, pszFileName, pszFilePath));

        /* Free the temporary memory used  */
		LMI_MEM_TCFREE(ppszUserEntryName);
		LMI_MEM_TCFREE(ppszUserEntryValue);
		LMI_MEM_TCFREE(ptQueryResults);
		return iRetCode;
	}
}
