/*======================================================================================================================================================================
													Copyright 2020  LMTec India Consulting Pvt Ltd
													Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_string_macros.hxx

Description:  This file contains macros for string's basic operation such as copy, concatination etc.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-May-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_STRING_MACROS_HXX
#define LMI_STRING_MACROS_HXX

#ifdef LMI_STRCPY
#undef LMI_STRCPY
#endif
#define LMI_STRCPY(pszDestStr, pszSourceStr){\
                    int iSourceStrLen = (int) (tc_strlen(pszSourceStr)+1);\
                    pszDestStr = (char*)MEM_alloc((int)(iSourceStrLen * sizeof(char)));\
                    tc_strcpy(pszDestStr, pszSourceStr);\
}

#ifdef LMI_STRCAT
#undef LMI_STRCAT
#endif
#define LMI_STRCAT(pszDestStr,pszSourceStr){\
                    int iSourceStrLen = (int) tc_strlen(pszSourceStr);\
                    int iDestStrLen = 0;\
                    if(pszDestStr != NULL){\
                        iDestStrLen = (int) tc_strlen (pszDestStr);\
                        pszDestStr = (char*)MEM_realloc(pszDestStr, (int)((iSourceStrLen + iDestStrLen + 1)* sizeof(char)));\
                    }\
                    else{\
                        pszDestStr = (char*)MEM_alloc((int)(sizeof(char) * (iSourceStrLen + 1)));\
                    }\
                    if(iDestStrLen == 0){\
                       tc_strcpy(pszDestStr, pszSourceStr);\
                    }\
                    else{\
                       tc_strcat(pszDestStr, pszSourceStr);\
                       tc_strcat(pszDestStr, '\0');\
                    }\
}

/**
 * This function adds input new string to the String Array. This also increases the value of the variable holding the
 * original size of array
 *
 */
#ifdef LMI_UPDATE_STRING_ARRAY
#undef LMI_UPDATE_STRING_ARRAY
#endif
#define LMI_UPDATE_STRING_ARRAY(iCurrentArrayLen, ppszStrArray, pszNewValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ppszStrArray = (char**) MEM_alloc(iCurrentArrayLen * sizeof(char*));\
            }\
            else{\
                ppszStrArray = (char**) MEM_realloc(ppszStrArray, iCurrentArrayLen * sizeof(char*));\
            }\
            ppszStrArray[iCurrentArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszNewValue) + 1)* sizeof(char)));\
            tc_strcpy(ppszStrArray[iCurrentArrayLen - 1], pszNewValue);\
}


 /* This macro add the new string to new index which is supplied to this macro as input argument.
	This macro does not increments the variable 'iNewArrayLen' instead expects the calling function to should
	provide the incremented size of the string array.
 */
#ifdef LMI_ADD_STRING_TO_ARRAY
#undef LMI_ADD_STRING_TO_ARRAY
#endif
#define LMI_ADD_STRING_TO_ARRAY(iNewArrayLen, ppszStrArray, pszNewValue){\
            if(iNewArrayLen == 1){\
                ppszStrArray = (char**) MEM_alloc(iNewArrayLen * sizeof(char*));\
            }\
            else{\
                ppszStrArray = (char**) MEM_realloc(ppszStrArray, iNewArrayLen * sizeof(char*));\
            }\
            ppszStrArray[iNewArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszNewValue) + 1)* sizeof(char)));\
            tc_strcpy(ppszStrArray[iNewArrayLen - 1], pszNewValue);\
}

#endif //LMI_STRING_MACROS_HXX






