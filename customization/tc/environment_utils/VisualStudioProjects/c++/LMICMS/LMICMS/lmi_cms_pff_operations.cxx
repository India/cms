/*======================================================================================================================================================================
                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                         Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_pff_operations.cxx

Description:  This File contains custom functions for PFF operations..

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-Sept-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#include <lmi_cms_library.hxx>
#include <lmi_cms_string_macros.hxx>
#include <lmi_cms_itk_mem_free.hxx>
#include <lmi_cms_itk_errors.hxx>
#include <lmi_cms_const.hxx>
#include <lmi_cms_errors.hxx>
#include <lmi_cms_pff_operations.hxx>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <string.h>

/**
 *
 * This function is used for PFF related opertaions which read all arguments value stored as input arg
 *  variables and passes to other functions.
 *
 * @param[in] pszOperation      Operation name
 * @param[in] pszPffUid         Uid of the Pff
 * @param[in] pszExportPffName  File Name of the Pff
 * @param[in] pszFilename       Output Filename
 * @param[in] pszFilepath       Output Filepath
 *
 * @retval ITK_ok If everything went okay.
 */
int LMI_process_pff_operation
(
    char* pszOperation,
    char* pszPffUid,
    char* pszExportPffName,
    char* pszFileName,
    char* pszFilePath
)
{
    int iRetCode = ITK_ok;

    if (tc_strcmp(pszOperation, LMI_EXPORTPFFNAME) == 0)
    {
        LMI_ITKCALL(iRetCode, LMI_create_file_for_pff_name(pszFileName, pszFilePath));
    }
    else if (tc_strcmp(pszOperation, LMI_OPERATION_EXPORT) == 0)
    {
        
        LMI_ITKCALL(iRetCode, LMI_export_pff(pszPffUid, pszFilePath));
    }

    return iRetCode;
}

/**
*
* This function executes the saved query with specified user entries.Tag of the queries are fetched for specified
* Querynames and no of found objects and object tags are returned.
*
* @param[in] pszQueryName         Queryname
* @param[in] iUserEntryCount      Count of user entries
* @param[in] ppszUserEntriesName  Userentriesname
* @param[in] ppszUserEntriesValue Userentriesvalue
* @param[out] piNoOfObjFound      Count of no of objects found
* @param[out] ptObjTag            Tag of object
*
* @retval ITK_ok If everything went okay.
*/
int LMI_execute_saved_pff
(
    char*   pszQueryName,
    int     iUserEntryCount,
    char**  ppszUserEntriesName,
    char**  ppszUserEntriesValue,
    int*    piNoOfObjFound,
    tag_t** ptObjTag
)
{
    int iRetCode   = ITK_ok;
    tag_t tQuery   = NULLTAG;
    int* piHrcyMap = NULL;

    if (tc_strlen(pszQueryName) > 0)
    {
        /* Finding Query tag */
        LMI_ITKCALL(iRetCode, QRY_find2(pszQueryName, &tQuery));

        /* Executing query */
        LMI_ITKCALL(iRetCode, QRY_execute_query(tQuery, iUserEntryCount, ppszUserEntriesName, ppszUserEntriesValue, 0, NULL, 0, piNoOfObjFound, &piHrcyMap, ptObjTag));
    }

    return iRetCode;
}

/**
*
* This function creates file  for pffnames. Filename and Filepath are used as input argument to create file filled
* with pffnames derived from the system at the specified location provided as input.
*
* @param[in] pszFilename    Filename to write pffnames
* @param[in] pszFilepath    Location of file used for file creation
*
* @retval ITK_ok If everything went okay.
*/
int LMI_create_file_for_pff_name
(
    char* pszFileName,
    char* pszFilePath
)
{
    int    iRetCode           = ITK_ok;
    int	   iUserEntryValCnt   = 0;
    int	   iUserEntryNo       = 0;
    int	   iQueryResultCnt    = 0;
    tag_t  tQuery             = NULLTAG;
    tag_t* ptQueryResults     = NULL;
    char** ppszUserEntryName  = NULL;
    char** ppszUserEntryValue = NULL;

    vector<string> vPffNames;
    vector<string> vPffUid;

    if (pszFileName == NULL || pszFilePath == NULL)
    {
        TC_write_syslog("ERROR: Missing input data: Filename or Filepath is empty\n");
    }
    else
    {
        /*Fill this in tag array*/
        LMI_UPDATE_STRING_ARRAY(iUserEntryNo, ppszUserEntryName, LMI_PFF_CLASS);

        LMI_ADD_STRING_TO_ARRAY(iUserEntryNo, ppszUserEntryValue, LMI_ALL_VALUES);
    }

    /*Execute queries*/
    LMI_ITKCALL(iRetCode, LMI_execute_saved_pff(LMI_ASK_PFF_NAME, iUserEntryNo, ppszUserEntryName, ppszUserEntryValue, &iQueryResultCnt, &ptQueryResults));

    for (int iDx = 0; iDx < iQueryResultCnt && iRetCode == ITK_ok; iDx++)
    {
        char* pszName = NULL;
        char* pszUid  = NULL;

        /* Get the value of property*/
        LMI_ITKCALL(iRetCode, AOM_ask_value_string(ptQueryResults[iDx], LMI_PFF_NAME, &pszName));

        LMI_ITKCALL(iRetCode, POM_tag_to_uid(ptQueryResults[iDx], &pszUid));

        if (iRetCode == ITK_ok)
        {
            vPffNames.push_back(pszName);
            
            vPffUid.push_back(pszUid);
        }

        LMI_MEM_TCFREE(pszName);
        LMI_MEM_TCFREE(pszUid);
    }

    /*File write operations*/
    LMI_ITKCALL(iRetCode, LMI_write_name_and_uid_to_file(vPffNames, vPffUid, pszFilePath, pszFileName));

    /* Free the temporary memory used  */
    LMI_MEM_TCFREE(ppszUserEntryName);
    LMI_MEM_TCFREE(ppszUserEntryValue);
    LMI_MEM_TCFREE(ptQueryResults);
    return iRetCode;
}

/**
*
* This function is used for PFF related operations. PFFUid, FilePath are used as input argument to create file for
* pff derived from the system at the specified location coming in FilePath.
*
* @param[in] pszPFFName       PFFName to write PFF File
* @param[in] pszFilepath      Location of file used for file creation
*
* @retval ITK_ok If everything went okay.
*/
int LMI_export_pff
(
    char* pszPffUid,
    char* pszFilePath
)
{
    int    iRetCode           = ITK_ok;
    int    iNum               = 0;
    int	   iUserEntryNo       = 0;
    int    iNoOfObjFound      = 0;
    int    iNoOfInstances     = 0;
    int    iNoOfRefClasses    = 0;
    int*   piLevels           = NULL;
    int*   piInstwhereFound   = NULL;
    int*   piClassLevels      = NULL;
    int*   piClassWhereFound  = NULL;
    tag_t  tPff               = NULL;
    tag_t* ptPffResults       = NULL;
    tag_t* ptReferencers      = NULL;
    tag_t* ptRefClasses       = NULL;
    char*  pszPffName         = NULL;
    char*  pszClass           = NULL;
    char*  pszDesc            = NULL;
    char** ppszUserEntryName  = NULL;
    char** ppszUserEntryValue = NULL;
    char** ppszClause         = NULL;

    LMI_ITKCALL(iRetCode, POM_string_to_tag(pszPffUid, &tPff));

    LMI_ITKCALL(iRetCode, AOM_ask_value_string(tPff, LMI_PFF_NAME, &pszPffName));

    LMI_ITKCALL(iRetCode, AOM_ask_value_string(tPff, LMI_PFF_CLASS, &pszClass));

    /* Get the value of Array property*/
    LMI_ITKCALL(iRetCode, AOM_ask_value_strings(tPff, LMI_PFF_CLAUSES, &iNum, &ppszClause));

    LMI_ITKCALL(iRetCode, AOM_ask_value_string(tPff, LMI_PFF_DESC, &pszDesc));

    /*Returns the classes and instances in database*/
    LMI_ITKCALL(iRetCode, POM_referencers_of_instance(tPff, 1, POM_in_db_only, &iNoOfInstances, &ptReferencers, &piLevels,

        &piInstwhereFound, &iNoOfRefClasses, &ptRefClasses, &piClassLevels, &piClassWhereFound));

    for (int iNx = 0; iNx < iNoOfInstances && iRetCode == ITK_ok; iNx++)
    {
        tag_t  tObjectType      = NULLTAG;
        tag_t  tDataFormatter   = NULLTAG;
        char*  pszName          = NULL;
        char*  pszType          = NULL;
        char*  pszQueryName     = NULL;
        char*  pszQueryDesc     = NULL;
        char*  pszQueryClass    = NULL;
        char*  pszNewUID        = NULL;
        char** ppszQueryClauses = NULL;

        LMI_ITKCALL(iRetCode, TCTYPE_ask_object_type(ptReferencers[iNx], &tObjectType));

        LMI_ITKCALL(iRetCode, TCTYPE_ask_name2(tObjectType, &pszType));

        if (tc_strcmp(pszType, LMI_REPORTDESIGN) == 0)
        {
            /*Returns a tag of property*/
            LMI_ITKCALL(iRetCode, AOM_ask_value_tag(ptReferencers[iNx], LMI_DATA_FORMATTER, &tDataFormatter));

            LMI_ITKCALL(iRetCode, AOM_ask_value_string(tDataFormatter, LMI_PFF_NAME, &pszName));

            LMI_ITKCALL(iRetCode, POM_tag_to_uid(tDataFormatter, &pszNewUID));

            if (tc_strcmp(pszPffUid, pszNewUID) == 0)
            {
                LMI_ITKCALL(iRetCode, LMI_obj_get_query_name(ptReferencers[iNx], &pszQueryName, &pszQueryDesc, &pszQueryClass, &ppszQueryClauses));

                LMI_ITKCALL(iRetCode, LMI_parse_xml_file_and_fill_structures(pszFilePath, pszPffName, pszClass, pszDesc, (*ppszClause), pszQueryName, pszQueryClass, (*ppszQueryClauses), pszQueryDesc));
            }
        }
        LMI_MEM_TCFREE(pszQueryName);
        LMI_MEM_TCFREE(pszQueryDesc);
        LMI_MEM_TCFREE(pszQueryClass);
        LMI_MEM_TCFREE(ppszQueryClauses);
        LMI_MEM_TCFREE(pszNewUID);
        LMI_MEM_TCFREE(pszType);
        LMI_MEM_TCFREE(pszName);
    }
    /* Free the temporary memory used  */
    LMI_MEM_TCFREE(ppszUserEntryName);
    LMI_MEM_TCFREE(ppszUserEntryValue);
    LMI_MEM_TCFREE(ptPffResults);
    LMI_MEM_TCFREE(ptReferencers);
    LMI_MEM_TCFREE(ptRefClasses);
    LMI_MEM_TCFREE(piLevels);
    LMI_MEM_TCFREE(piInstwhereFound);
    LMI_MEM_TCFREE(piClassLevels);
    LMI_MEM_TCFREE(piClassWhereFound);
    LMI_MEM_TCFREE(pszClass);
    LMI_MEM_TCFREE(ppszClause);
    LMI_MEM_TCFREE(pszDesc);
    LMI_MEM_TCFREE(pszPffName);
    return iRetCode;
}

/**
*
* This function gets the properties associated with iman Query BO.Tag of the objects are fetched for specified
* PFF and associated property value names are fetched.
*
* @param[in] ptReferencers     Tag of Referencers
* @param[in] pszQueryName      Querynames associated with Pff
* @param[in] pszQueryDesc      Query Description associated with Pff
* @param[in] pszQueryClass     QueryClass associated with Pff
* @param[in] pszQueryClauses   QueryClauses associated with Pff
*
* @retval ITK_ok If everything went okay.
*/
int LMI_obj_get_query_name
(
    tag_t   ptReferencers,
    char**  ppszQueryName,
    char**  ppszQueryDesc,
    char**  ppszQueryClass,
    char*** pppszQueryClauses
)
{
    int    iRetCode    = ITK_ok;
    int    iNum        = 0;
    tag_t  tImanQuery  = NULLTAG;
   
    LMI_ITKCALL(iRetCode, AOM_ask_value_tag((ptReferencers), LMI_IMAN_QUERY, &tImanQuery));

    LMI_ITKCALL(iRetCode, AOM_ask_value_string(tImanQuery, LMI_QUERY_NAME, ppszQueryName));

    LMI_ITKCALL(iRetCode, AOM_ask_value_string(tImanQuery, LMI_QUERY_CLASS, ppszQueryClass));

    LMI_ITKCALL(iRetCode, AOM_ask_value_string(tImanQuery, LMI_QUERY_DESC, ppszQueryDesc));

    LMI_ITKCALL(iRetCode, AOM_ask_value_strings(tImanQuery, LMI_QUERY_CLAUSES, &iNum, pppszQueryClauses));
    
    return iRetCode;
}

/**
*
* This function parses the xml file with specified user entries..
*
* @param[in] filePath        File Path for creating pff file
* @param[in] pszName         PFF Name associated with Pff
* @param[in] pszClass        PFF Class associated with Pff
* @param[in] pszDesc         PFF desccription associated with Pff
* @param[in] pszClause       Pff Clause associated with Pff
* @param[in] pszQueryName    QueryName associated with Pff
* @param[in] pszQueryClass   QueryClass associated with Pff
* @param[in] pszQueryClauses QueryClauses associated with Pff
* @param[in] pszQueryDesc    QueryDesc associated with Pff
*
* @retval ITK_ok If everything went okay.
*/
int LMI_parse_xml_file_and_fill_structures
(
    char* pszFilePath,
    char* pszName,
    char* pszClass,
    char* pszDesc,
    char* pszClause,
    char* pszQueryName,
    char* pszQueryClass,
    char* pszQueryClauses,
    char* pszQueryDesc
)
{
    int iRetCode = ITK_ok;
    
    if (pszFilePath != NULL)
    {
        /* Initializing xerces */
        XMLPlatformUtils::Initialize();

        XercesDOMParser* parser = new XercesDOMParser();

        try
        {
        parser->setValidationScheme(XercesDOMParser::Val_Never);

        parser->setDoNamespaces(false);

        parser->setDoSchema(false);

        parser->parse(pszFilePath);
        }
        catch (...)
        {
            iRetCode = ERROR_PARSING_XML_FILE;

            EMH_store_error(EMH_severity_warning, iRetCode);

            delete(parser);
        }

        if (iRetCode == ITK_ok)
        {
            try
            {
                // Getting DOM Document 
                xercesc_2_8::DOMDocument* domDoc = parser->getDocument();

                DOMElement* elementRoot = domDoc->getDocumentElement();
      
                DOMNode *nodepffname = NULL;
                DOMNode *nodepffclass = NULL;
                DOMNode *nodepffdesc = NULL;
                DOMNode *nodepffclause = NULL;
                DOMNode *nodequeryname = NULL;
                DOMNode *nodequeryclass = NULL;
                DOMNode *nodequerydesc = NULL;
                DOMNode *nodequeryclause = NULL;
                DOMNode *nodedesignname = NULL;
                DOMNode *nodedesigndesc = NULL;
               
                const XMLCh* xmlReportDesignsPffName = XMLString::transcode(LMI_TAG_PFFNAME);
                const XMLCh* xmlReportDesignsPffClass = XMLString::transcode(LMI_TAG_PFFCLASS);
                const XMLCh* xmlReportDesignsPffDesc = XMLString::transcode(LMI_TAG_PFFDESC);
                const XMLCh* xmlReportDesignsPffClause = XMLString::transcode(LMI_TAG_PFFCLAUSE);
                const XMLCh* xmlReportDesignsQueryName = XMLString::transcode(LMI_TAG_QUERYNAME);
                const XMLCh* xmlReportDesignsQueryDesc = XMLString::transcode(LMI_TAG_QUERYDESC);
                const XMLCh* xmlReportDesignsQueryClass = XMLString::transcode(LMI_TAG_QUERYCLASS);
                const XMLCh* xmlReportDesignsQueryClause = XMLString::transcode(LMI_TAG_QUERYCLAUSE);
                const XMLCh* xmlReportDesignsDesignName = XMLString::transcode(LMI_TAG_DESIGNNAME);
                const XMLCh* xmlReportDesignsDesignDesc = XMLString::transcode(LMI_TAG_DESIGNDESC);

                DOMNodeList* domListPffName = elementRoot->getElementsByTagName(xmlReportDesignsPffName);
                DOMNodeList* domListPffClass = elementRoot->getElementsByTagName(xmlReportDesignsPffClass);
                DOMNodeList* domListPffDesc = elementRoot->getElementsByTagName(xmlReportDesignsPffDesc);
                DOMNodeList* domListPffClause = elementRoot->getElementsByTagName(xmlReportDesignsPffClause);
                DOMNodeList* domListQueryName = elementRoot->getElementsByTagName(xmlReportDesignsQueryName);
                DOMNodeList* domListQueryDesc = elementRoot->getElementsByTagName(xmlReportDesignsQueryDesc);
                DOMNodeList* domListQueryClass = elementRoot->getElementsByTagName(xmlReportDesignsQueryClass);
                DOMNodeList* domListQueryClause = elementRoot->getElementsByTagName(xmlReportDesignsQueryClause);
                DOMNodeList* domListDesignName = elementRoot->getElementsByTagName(xmlReportDesignsDesignName);
                DOMNodeList* domListDesignDesc = elementRoot->getElementsByTagName(xmlReportDesignsDesignDesc);

                if (domListPffName->getLength() == 1 && domListPffName != NULL)
                {
                    nodepffname = domListPffName->item(0);

                    DOMText* pTextNodePffName = NULL;

                    const XMLCh* xmlpszName = XMLString::transcode(pszName);

                    pTextNodePffName = domDoc->createTextNode(xmlpszName);

                    nodepffname->appendChild(pTextNodePffName);
                }
                if (domListPffClass->getLength() == 1 && domListPffClass != NULL)
                {
                    nodepffclass = domListPffClass->item(0);

                    DOMText* pTextNodePffClass = NULL;

                    const XMLCh* xmlpszClass = XMLString::transcode(pszClass);

                    pTextNodePffClass = domDoc->createTextNode(xmlpszClass);

                    nodepffclass->appendChild(pTextNodePffClass);
                }
                if (domListPffDesc->getLength() == 1 && domListPffDesc != NULL)
                {
                    nodepffdesc = domListPffDesc->item(0);

                    DOMText* pTextNodePffDesc = NULL;

                    const XMLCh* xmlpszDesc = XMLString::transcode(pszDesc);

                    pTextNodePffDesc = domDoc->createTextNode(xmlpszDesc);

                    nodepffdesc->appendChild(pTextNodePffDesc);
                }
                if (domListPffClause->getLength() == 1 && domListPffClause != NULL)
                {
                    nodepffclause = domListPffClause->item(0);

                    DOMText* pTextNodePffClause = NULL;

                    const XMLCh* xmlpszClause = XMLString::transcode(pszClause);

                    pTextNodePffClause = domDoc->createTextNode(xmlpszClause);

                    nodepffclause->appendChild(pTextNodePffClause);
                }
                if (domListQueryName->getLength() == 1 && domListQueryName != NULL)
                {
                    nodequeryname = domListQueryName->item(0);

                    DOMText* pTextNodeQueryName = NULL;

                    const XMLCh* xmlpszQueryName = XMLString::transcode(pszQueryName);

                    pTextNodeQueryName = domDoc->createTextNode(xmlpszQueryName);

                    nodequeryname->appendChild(pTextNodeQueryName);
                }
                if (domListQueryClass->getLength() == 1 && domListQueryClass != NULL)
                {
                    nodequeryclass = domListQueryClass->item(0);

                    DOMText* pTextNodeQueryClass = NULL;

                    const XMLCh* xmlpszQueryClass = XMLString::transcode(pszQueryClass);

                    pTextNodeQueryClass = domDoc->createTextNode(xmlpszQueryClass);

                    nodequeryclass->appendChild(pTextNodeQueryClass);
                }
                if (domListQueryDesc->getLength() == 1 && domListQueryDesc != NULL)
                {
                    nodequerydesc = domListQueryDesc->item(0);

                    DOMText* pTextNodeQueryDesc = NULL;

                    const XMLCh* xmlpszQueryDesc = XMLString::transcode(pszQueryDesc);

                    pTextNodeQueryDesc = domDoc->createTextNode(xmlpszQueryDesc);

                    nodequerydesc->appendChild(pTextNodeQueryDesc);
                }
                if (domListQueryClause->getLength() == 1 && domListQueryClause != NULL)
                {
                    nodequeryclause = domListQueryClause->item(0);

                    DOMText* pTextNodeQueryClause = NULL;

                    const XMLCh* xmlpszQueryClause = XMLString::transcode(pszQueryClauses);

                    pTextNodeQueryClause = domDoc->createTextNode(xmlpszQueryClause);

                    nodequeryclause->appendChild(pTextNodeQueryClause);
                }
                if (domListDesignName->getLength() == 1 && domListDesignName != NULL)
                {
                    nodedesignname = domListDesignName->item(0);

                    DOMText* pTextNodeDesignName = NULL;

                    const XMLCh* xmlpszDesignName = XMLString::transcode(pszName);

                    pTextNodeDesignName = domDoc->createTextNode(xmlpszDesignName);

                    nodedesignname->appendChild(pTextNodeDesignName);
                }
                if (domListDesignDesc->getLength() == 1 && domListDesignDesc != NULL)
                {
                    nodedesigndesc = domListDesignDesc->item(0);

                    DOMText* pTextNodeDesignDesc = NULL;

                    const XMLCh* xmlpszDesignDesc = XMLString::transcode(pszDesc);

                    pTextNodeDesignDesc = domDoc->createTextNode(xmlpszDesignDesc);

                    nodedesigndesc->appendChild(pTextNodeDesignDesc);
                }
                
                LMI_ITKCALL(iRetCode, LMI_save_xml_file(domDoc, pszFilePath));
            }
                
            catch (...)
            {
                iRetCode = ERROR_HARDWARE_TAG_NOT_FOUND;

                EMH_store_error(EMH_severity_warning, iRetCode);

                delete(parser);
            }
        }
    }
    return iRetCode;
}

/**
*
* This function executes the output for xml file.
*
* @param[in] dom Doc     xerces Dom Document
* @param[in] pszfilePath Path of xml file
*
* @retval ITK_ok If everything went okay.
*/

int  LMI_save_xml_file
(
    xercesc_2_8::DOMDocument* domDoc,
    const char* pszFilePath
)
{
    DOMImplementation    *pImplement  = NULL;
    DOMWriter            *pSerializer = NULL;
    XMLFormatTarget        *pTarget   = NULL;
    int                    iRetCode   = ITK_ok;

    /*
    Return the first registered implementation that
    has the desired features. In this case, we are after
    a DOM implementation that has the LS feature... or Load/Save.
    */
    pImplement = DOMImplementationRegistry::getDOMImplementation(L"LS");

    /*
    From the DOMImplementation, create a DOMWriter.
    DOMWriters are used to serialize a DOM tree [back] into an XML document.
    */
    pSerializer = ((DOMImplementationLS*)pImplement)->createDOMWriter();

    /*
    This line is optional. It just sets a feature
    of the Serializer to make the output
    more human-readable by inserting line-feeds,
    without actually inserting any new elements/nodes
    into the DOM tree. (There are many different features to set.)
    Comment it out and see the difference.
    */
    pSerializer->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true);

    /*
    Choose a location for the serialized output. The 3 options are:
        1) StdOutFormatTarget     (std output stream -  good for debugging)
        2) MemBufFormatTarget     (to Memory)
        3) LocalFileFormatTarget  (save to file)
        (Note: You'll need a different header file for each one)
        Don't forget to escape file-paths with a backslash character, or
        just specify a file to be saved in the exe directory.

        eg. c:\\example\\subfolder\\pfile.xml

    */
    pTarget = new LocalFileFormatTarget(pszFilePath);

    // Write the serialized output to the target.
    pSerializer->writeNode(pTarget, *domDoc);
   
    return iRetCode;
}
    

    
    

