/*======================================================================================================================================================================
                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                         Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_workflow_operations.cxx

Description:  This File contains custom functions for Workflow operations..

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-July-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#include <lmi_cms_library.hxx>
#include <lmi_cms_string_macros.hxx>
#include <lmi_cms_itk_mem_free.hxx>
#include <lmi_cms_itk_errors.hxx>
#include <lmi_cms_const.hxx>
#include <lmi_cms_workflow_operations.hxx>

/**
 *
 * This function is used for workflow related operations which read all arguments value stored as input arg
 * variables and passes to other functions.
 *
 * @param[in] pszOperation Operation name
 * @param[in] pszFilename  Output Filename
 * @param[in] pszFilepath  Output Filepath
 *
 * @retval ITK_ok If everything went okay.
 */
int LMI_process_workflow_operation
(
    char* pszOperation,
    char* pszFileName,
    char* pszFilePath
)
{  
    int iRetCode = ITK_ok;

    if (tc_strcmp(pszOperation, LMI_EXPORTWORKFLOWNAME) == 0)
    {
        LMI_ITKCALL(iRetCode, LMI_create_file_for_workflow_name(pszFileName, pszFilePath));
    }

    return iRetCode;
}

/**
*
* This function creates file  for workflownames. Filename and Filepath are used as input argument to create file filled
* with workflownames derived from the system at the specified location coming in FilePath.
*
* @param[in] pszFilename Filename to write workflownames
* @param[in] pszFilepath Location of file used for file creation
*
* @retval ITK_ok If everything went okay.
*/
int LMI_create_file_for_workflow_name
(
    char* pszFileName,
    char* pszFilePath
)
{
    int    iRetCode    = ITK_ok;
    int    iTemplCount = 0;
    tag_t* ptTempl     = NULL;

    vector<string> vWorkflowNames;

    if (pszFileName == NULL || pszFilePath == NULL)
    {
        TC_write_syslog("ERROR: Missing input data: Filename or Filepath is empty\n");
    }
    else
    {
        /*It returns the no and tags of workflow process templates */
        LMI_ITKCALL(iRetCode, EPM_ask_all_process_templates(LMI_WF_TEMPL_STAGE_READY_TO_USE, &iTemplCount, &ptTempl));

        for (int iDx = 0; iDx < iTemplCount && iRetCode == ITK_ok; iDx++)
        {
            if (ptTempl[iDx] != NULLTAG)
            {
                char* pszTemplateName = NULL;

                /* Returns the value of template names*/
                LMI_ITKCALL(iRetCode, AOM_ask_value_string(ptTempl[iDx], LMI_EXPORTWORKFLOWTEMPLNAME, &pszTemplateName));

                if (tc_strlen(pszTemplateName) > 0 && pszTemplateName != NULL && iRetCode == ITK_ok)
                {
                    vWorkflowNames.push_back(pszTemplateName);
                }

                LMI_MEM_TCFREE(pszTemplateName);
            }
        }

        /*File write operations*/
        LMI_ITKCALL(iRetCode, LMI_write_data_to_file(vWorkflowNames, pszFileName, pszFilePath));

        LMI_MEM_TCFREE(ptTempl);
        return iRetCode;
    }
}
    





        
    
