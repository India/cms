/*======================================================================================================================================================================
                                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_file_operation_utils.cxx

Description:  This File contains custom functions for file operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-May-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#include <lmi_cms_library.hxx>
#include <lmi_cms_errors.hxx>

/**
 * This function does filewriting. Read Querynames from vector of string line by line and add to text file.
 * File gets created with specified Filename and Filelocation coming as input argument.File contains 
 * Querynames derived from the system written line by line.
 *
 * @param[in] vname       Vector of string containing querynames
 * @param[in] pszFilename Name of file to be written with querynames
 * @param[in] pszFilepath Name of filepath where file is placed
 *
 */
int LMI_write_data_to_file
(
    vector<string> vName,
    char*          pszFileName,
    char*          pszFilePath
)
{   
    int iRetCode = ITK_ok;
    string strloc;
    string strFile ;
    int iSize = (int)(vName.size());

    strloc.assign(LMI_FILE_PATH);
    strFile.assign(pszFilePath);
    strFile.append(strloc);
    strFile.append(pszFileName);
 
    ofstream outfile(strFile);

    for (int iDx = 0; iDx < iSize; iDx++)
    {
        outfile << vName[iDx] << endl;
    }

    outfile.close();
    return iRetCode;
}

/**
 * This function does filewriting. Read Querynames and uid from vector of string line by line and add to text file.
 * File gets created with specified Filename and Filelocation coming as input argument and containing Querydisplay
 * names and Uid line by line.
 *
 * @param[in] vName       vector of string containing querynames
 * @param[in] vUid        vector of string containing uids
 * @param[in] pszFileloc  Location of file to be written with querynames
 * @param[in] pszFilename Name of file to be written with querynames and uids
 *
 */
int LMI_write_name_and_uid_to_file
(
    vector<string> vName,
    vector<string> vUid,
    char*          pszFileLoc,
    char*          pszFileName
)
{
    int iRetCode = ITK_ok;
    string strloc;
    string strFile;
    int iSizeName  = (int)(vName.size());
    int iSizeUid   = (int)(vUid.size());

    strloc.assign(LMI_FILE_PATH);
    strFile.assign(pszFileLoc);
    strFile.append(strloc);
    strFile.append(pszFileName);
  
    ofstream outfile(strFile);
 
    for (int iDx = 0; iDx < iSizeName && iDx < iSizeUid; iDx++)
    {
        outfile << vName[iDx];
        outfile << '\n';
        outfile << vUid[iDx] << endl;
    }

    outfile.close();
    return iRetCode;
}