/*======================================================================================================================================================================
                                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_stylesheet_operations.cxx

Description:  This File contains custom functions for Stylesheet operations..

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15-July-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#include <lmi_cms_library.hxx>
#include <lmi_cms_string_macros.hxx>
#include <lmi_cms_itk_mem_free.hxx>
#include <lmi_cms_itk_errors.hxx>
#include <lmi_cms_stylesheet_operations.hxx>
#include <lmi_cms_const.hxx>

/**
 *
 * This function is used for stylesheet related opertaions which read all arguments value stored as input arg
 *  variables and passes to other functions.
 *
 * @param[in] pszOperation            Operation name
 * @param[in] pszStylesheetName       Internal name of the Stylesheet
 * @param[in] pszExportStylesheetName Display name of the Stylesheet
 * @param[in] pszFilename             Output Filename
 * @param[in] pszFilepath             Output Filepath
 * @param[in] pszUserRequest          UserRequestedDataset
 *
 * @retval ITK_ok If everything went okay.
 */
int LMI_process_stylesheet_operation
(
    char* pszOperation,
    char* pszStylesheetName,
    char* pszExportStylesheetName,
    char* pszFileName,
    char* pszFilePath,
    char* pszUserRequest
)
{
    int    iRetCode                   = ITK_ok;
    int    iStylesheetNameCount       = 0;
    int    iStylesheetDispNameCount   = 0;
    char** ppszStylesheetNameList     = NULL;
    char** ppszStylesheetDispNameList = NULL;

    if (tc_strcmp(pszOperation, LMI_EXPORTSTYLESHEETNAME) == 0)
    {
        LMI_ITKCALL(iRetCode, LMI_create_file_for_stylesheet_name(pszFileName, pszFilePath, pszUserRequest));
    }
    else if (tc_strcmp(pszOperation, LMI_OPERATION_EXPORT) == 0)
    {
        LMI_ITKCALL(iRetCode, LMI_store_stylesheet_name(pszStylesheetName, pszExportStylesheetName, LMI_STRING_COMMA, &iStylesheetNameCount, &ppszStylesheetNameList, &iStylesheetDispNameCount, &ppszStylesheetDispNameList));

        LMI_ITKCALL(iRetCode, LMI_print_stylesheet_name_and_uid(iStylesheetNameCount, iStylesheetDispNameCount, pszFileName, pszFilePath, ppszStylesheetNameList, ppszStylesheetDispNameList));
    }

    LMI_MEM_TCFREE(ppszStylesheetNameList);
    LMI_MEM_TCFREE(ppszStylesheetDispNameList);
    return iRetCode;
}

/**
 *
 * This function stores all stylesheetnames and stylesheetdisplaynames in list.It tokenises StylesheetName and ExportStylesheetNames
 * with the seperator used.
 * List of StylesheetNames and StylesheetDisplayNames are provided as output from this function.
 *
 * @param[in]    pszStylesheetName           Stylesheetnames
 * @param[in]    pszExportStylesheetName     Stylesheetdisplaynames
 * @param[in]    pszSeparator                Seperator used to tokenise stylesheetnames and exportstylesheetnames
 * @param[out]   piStylesheetNameCount       Count of stylesheet internal name
 * @param[out/f] pppszStylesheetNamelist     List of stylesheetnames
 * @param[out]   piStylesheetDispNameCount   Count of stylesheet Display name
 * @param[out/f] pppszStylesheetDispNamelist List of stylesheetdisplaynames
 *
 *
 * @retval ITK_ok If everything went okay.
 */
int LMI_store_stylesheet_name
(
    char*   pszStylesheetName,
    char*   pszExportStylesheetName,
    char*   pszSeparator,
    int*    piStylesheetNameCount,
    char*** pppszStylesheetNamelist,
    int*    piStylesheetDispNameCount,
    char*** pppszStylesheetDispNamelist
)
{
    int   iRetCode                   = ITK_ok;
    int   iCounter                   = 0;
    char* pszStylesheetNameToken     = NULL;
    char* pszStylesheetDispNameToken = NULL;


    /* Validate input */
    if (pszStylesheetName == NULL || pszSeparator == NULL || pszExportStylesheetName == NULL)
    {
        TC_write_syslog("ERROR: Missing input data: List passed or seprator is null\n");
    }
    else
    {
        /* Get the first token */
        pszStylesheetNameToken = tc_strtok(pszStylesheetName, pszSeparator);

        /* Tokenize the input string */
        while (pszStylesheetNameToken != NULL)
        {
            LMI_UPDATE_STRING_ARRAY((*piStylesheetNameCount), (*pppszStylesheetNamelist), pszStylesheetNameToken);

            /* Get next token: */
            pszStylesheetNameToken = tc_strtok(NULL, pszSeparator);
        }

        pszStylesheetDispNameToken = tc_strtok(pszExportStylesheetName, pszSeparator);

        while (pszStylesheetDispNameToken != NULL)
        {
            /*Fill this in tag array*/
            LMI_UPDATE_STRING_ARRAY((*piStylesheetDispNameCount), (*pppszStylesheetDispNamelist), pszStylesheetDispNameToken);

            /* Get next token: */
            pszStylesheetDispNameToken = tc_strtok(NULL, pszSeparator);
        }
    }

    /* Free the temporary memory used for tokenizing */
    LMI_MEM_TCFREE(pszStylesheetNameToken);
    LMI_MEM_TCFREE(pszStylesheetDispNameToken);
    return iRetCode;
}

/**
*
* The function prints all display names of stylesheetnames and uids in file.This function firstly provides tag of the stylesheet
* name and converts this tag to uid of the specified stylesheetname.
* These uid's are filled in vector of string  along and stylesheetdisplaynames are also filled in seperate vecctor of
* string.
*
* @param[in] iQueryNameCount        Count of query internal name
* @param[in] iQueryDispNameCount    Count of query Display name
* @param[in] pszFilename            Name of the file to be created to print querynames and uid
* @param[in] pszFileloc             Location of file
* @param[in] ppszQueryNamelist      List of querynames
* @param[in] pppszQueryDispNamelist List of querydisplaynames
*
* @retval ITK_ok If everything went okay.
*/
int LMI_print_stylesheet_name_and_uid
(
    int    iStylesheetNameCount,
    int    iStylesheetDispNameCount,
    char*  pszFileName,
    char*  pszFileLoc,
    char** ppszStylesheetNameList,
    char** ppszStylesheetDispNameList
)
{
    int    iRetCode = ITK_ok;
    int    iUIDSize = 0;
    tag_t  tQuery   = NULLTAG;

    vector<string> vExpStylesheetNames;
    vector<string> vExpStylesheetUid;

    for (int iDx = 0; iDx < iStylesheetNameCount && iRetCode == ITK_ok; iDx++)
    {
        char* pszUid = NULL;
        tag_t tQuery = NULLTAG;

        /* Searches the given Queryname and returns its tag*/
        LMI_ITKCALL(iRetCode, AE_find_dataset2(ppszStylesheetNameList[iDx], &tQuery));

        LMI_ITKCALL(iRetCode, POM_tag_to_uid(tQuery, &pszUid));

        if (iRetCode == ITK_ok)
        {
            vExpStylesheetNames.push_back(ppszStylesheetDispNameList[iDx]);

            vExpStylesheetUid.push_back(pszUid);
        }

        LMI_MEM_TCFREE(pszUid);
    }

    /*file write operations*/
    LMI_ITKCALL(iRetCode, LMI_write_name_and_uid_to_file(vExpStylesheetNames, vExpStylesheetUid, pszFileLoc, pszFileName));

    return iRetCode;
}

/**
*
* This function executes the saved query with specified user entries.Tag of the queries are fetched for specified
* Querynames and no of found objects and object tags are returned.
*
* @param[in] pszQueryName         Queryname
* @param[in] iUserEntryCount      Count of user entries
* @param[in] ppszUserEntriesName  Userentriesname     
* @param[in] ppszUserEntriesValue Userentriesvalue
* @param[out] piNoOfObjFound      Count of no of objects found
* @param[out] ptObjTag            Tag of object
*
* @retval ITK_ok If everything went okay.
*/
int LMI_execute_saved_queries
(
    char*   pszQueryName,
    int     iUserEntryCount,
    char**  ppszUserEntriesName,
    char**  ppszUserEntriesValue,
    int*    piNoOfObjFound,
    tag_t** ptObjTag
)
{
    int iRetCode   = ITK_ok;
    tag_t tQuery   = NULLTAG;
    int* piHrcyMap = NULL;

    if (tc_strlen(pszQueryName) > 0)
    {
        /* Finding Query tag */
        LMI_ITKCALL(iRetCode, QRY_find2(pszQueryName, &tQuery));

        /* Executing query */
        LMI_ITKCALL(iRetCode, QRY_execute_query(tQuery, iUserEntryCount, ppszUserEntriesName, ppszUserEntriesValue, 0, NULL, 0, piNoOfObjFound, &piHrcyMap, ptObjTag));
    }

    return iRetCode;
}

/**
*
* This function creates file  for stylesheetnames. Filename and Filepath are used as input argument to create file filled
* with stylesheetnames derived from the sysytem at the specified location coming in FilePath.
*
* @param[in] pszFilename    Filename to write stylesheetnames
* @param[in] pszFilepath    Location of file used for file creation
* @param[in] pszUserRequest UserRequested Search for Stylesheet
*
* @retval ITK_ok If everything went okay.
*/
int LMI_create_file_for_stylesheet_name
(
    char* pszFileName,
    char* pszFilePath,
    char* pszUserRequest
)
{
    int    iRetCode           = ITK_ok;
    int	   iUserEntryValCnt   = 0;
    int	   iUserEntryNo       = 0;
    int	   iQueryResultCnt    = 0;
    tag_t  tQuery             = NULLTAG;
    tag_t* ptQueryResults     = NULL;
    char** ppszUserEntryName  = NULL;
    char** ppszUserEntryValue = NULL;

    vector<string> vStylesheetNames;

    if (pszFileName == NULL || pszFilePath == NULL)
    {
        TC_write_syslog("ERROR: Missing input data: Filename or Filepath is empty\n");
    }

    else if(tc_strlen(pszUserRequest) > 0)
    {
        /*Fill this in tag array*/
        LMI_UPDATE_STRING_ARRAY(iUserEntryNo, ppszUserEntryName, LMI_DATASET_TYPE);

        /*Add this in tag array*/
        LMI_ADD_STRING_TO_ARRAY(iUserEntryNo, ppszUserEntryValue, LMI_XMLRENDERINGSTYLESHEET);

        LMI_UPDATE_STRING_ARRAY(iUserEntryNo, ppszUserEntryName, LMI_NAME);

        LMI_ADD_STRING_TO_ARRAY(iUserEntryNo, ppszUserEntryValue, pszUserRequest);
    }
    else
    {
        /*Fill this in tag array*/
        LMI_UPDATE_STRING_ARRAY(iUserEntryNo, ppszUserEntryName, LMI_DATASET_TYPE);

        LMI_ADD_STRING_TO_ARRAY(iUserEntryNo, ppszUserEntryValue, LMI_XMLRENDERINGSTYLESHEET);
    }

    /*Execute queries*/
    LMI_ITKCALL(iRetCode, LMI_execute_saved_queries(LMI_EXTRACT_DATASET_NAME, iUserEntryNo, ppszUserEntryName, ppszUserEntryValue, &iQueryResultCnt, &ptQueryResults));

    for (int iDx = 0; iDx < iQueryResultCnt && iRetCode == ITK_ok; iDx++)
    {
        char* pszName = NULL;

        LMI_ITKCALL(iRetCode, AOM_ask_value_string(ptQueryResults[iDx], LMI_OBJECT_NAME, &pszName));

        if (iRetCode == ITK_ok)
        {
            vStylesheetNames.push_back(pszName);
        }

        LMI_MEM_TCFREE(pszName);
    }

    /*File write operations*/
    LMI_ITKCALL(iRetCode, LMI_write_data_to_file(vStylesheetNames, pszFileName, pszFilePath));

    /* Free the temporary memory used  */
    LMI_MEM_TCFREE(ppszUserEntryName);
    LMI_MEM_TCFREE(ppszUserEntryValue);
    LMI_MEM_TCFREE(ptQueryResults);
    return iRetCode;
}

    

