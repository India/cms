/*======================================================================================================================================================================
													Copyright 2020  LMTec India Consulting Pvt Ltd
													Unpublished - All rights reserved
=======================================================================================================================================================================
File Description:

Filename:     lmi_cms_library.hxx

Description:  Header File containing declarations of function spread across all LMI_<UTILITY NAME>_utilities.cxx
				  For e.g. LMI_dataset_utilities.cxx/LMI_object_utilities.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-May-20     Mohit Kumar        Initial Release

========================================================================================================================================================================*/
#ifndef LMI_LIBRARY_HXX
#define LMI_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <tccore/releasestatus.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <epm/signoff.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom/pom/pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me/me.h>
#include <form/form.h>
#include <tccore/project.h>
#include <tc/folder.h>
#include <tccore/tctype.h>
#include <fclasses/tc_date.h>
#include <res/res_itk.h>
#include <sa/am.h>
#include <map>
#include <string>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <lmi_cms_const.hxx>
#include <stdarg.h>
#include <string.h>
#include <tc/emh.h>
#include <pie/pie.h>
#include <tc/tc.h>
#include <tc\tc_macros.h>
#include <tc\tc_startup.h>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>



using namespace std;
using namespace xercesc_2_8;

/*        Utility Functions For Error Logging        */

int LMI_err_write_to_logfile
(
	char* pszErrorFunction,
	int   iErrorNumber,
	char* pszFileName,
	int   iLineNumber
);

int LMI_err_start_debug
(
	char* pszCallingFunction,
	int   iErrorNumber,
	char* pszFileName,
	int   iLineNumber
);

/*        Utility Functions For File Operations        */
int LMI_write_data_to_file
(
    vector<string> Queryname,
    char* pszFilename,
    char* pszFilepath
);

int LMI_write_name_and_uid_to_file
(
    vector<string> ExpQuerynames,
    vector<string> ExpQueryuid,
    char* pszFileloc,
    char* pszFilenam
);

#endif




