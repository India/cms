/*======================================================================================================================================================================
													Copyright 2020  LMTec India Consulting Pvt Ltd
													Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_itk_main.cxx

Description:  Header File for containing macros for custom errors

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-May-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#include <lmi_cms_library.hxx>
#include <lmi_cms_query_operations.hxx>
#include <lmi_cms_workflow_operations.hxx>
#include <lmi_cms_transfer_mode_operations.hxx>
#include <lmi_cms_stylesheet_operations.hxx>
#include <lmi_cms_pff_operations.hxx>
#include <lmi_cms_const.hxx>

#ifdef __cplusplus
extern "C" {
#endif

#include <lmi_cms_itk_main.hxx>
#include <lmi_cms_itk_errors.hxx>

int ITK_user_main(int iArgCount, char** ppszArgVal)
{
	int   iRetCode          = ITK_ok;
	char* pszUser           = NULL;
	char* pszPassword       = NULL;
	char* pszGroup          = NULL;
	char* pszMode           = NULL;
	char* pszOperation      = NULL;
	char* pszFileIntName    = NULL;
	char* pszExportFileName = NULL;
	char* pszFileName       = NULL;
	char* pszFilePath       = NULL;
    char* pszHelp           = NULL;
    char* pszSearchString   = NULL;

	pszUser           = ITK_ask_cli_argument("-u=");

	pszPassword       = ITK_ask_cli_argument("-p=");

	pszGroup          = ITK_ask_cli_argument("-g=");

	pszMode           = ITK_ask_cli_argument("-mode=");

	pszOperation      = ITK_ask_cli_argument("-operation=");
		
	pszFileIntName    = ITK_ask_cli_argument("-internalname=");

	pszExportFileName = ITK_ask_cli_argument("-exportfilename=");

	pszFileName       = ITK_ask_cli_argument("-tcconfigfilename=");
		
	pszFilePath       = ITK_ask_cli_argument("-filepath=");

    pszSearchString   = ITK_ask_cli_argument("-search_string=");

    /* Put - h CLI argument here to avoid starting the whole of iman just for a help message! */
    if (ITK_ask_cli_argument("-h") != 0)
    {
        LMICMSUTILS_display_usage();

        return iRetCode;
    }

	LMI_ITKCALL(iRetCode, TC_init_module(pszUser, pszPassword, pszGroup));

	if (tc_strcmp(pszMode, LMI_MODE_QUERY) == 0)
	{
		LMI_ITKCALL(iRetCode, LMI_process_query_operation(pszOperation, pszFileIntName, pszExportFileName, pszFileName, pszFilePath));
	}
    else if (tc_strcmp(pszMode, LMI_MODE_WORKFLOW) == 0)
    {
        LMI_ITKCALL(iRetCode, LMI_process_workflow_operation(pszOperation, pszFileName, pszFilePath));
    }
    else if (tc_strcmp(pszMode, LMI_MODE_TRANSFER_MODE) == 0)
    {
        LMI_ITKCALL(iRetCode, LMI_process_transfer_mode_operation(pszOperation, pszFileName, pszFilePath));
    }
    else if (tc_strcmp(pszMode, LMI_MODE_STYLESHEET) == 0)
    {
        LMI_ITKCALL(iRetCode, LMI_process_stylesheet_operation(pszOperation, pszFileIntName, pszExportFileName, pszFileName, pszFilePath, pszSearchString));
    }
    else if (tc_strcmp(pszMode, LMI_MODE_PFF) == 0)
    {
        LMI_ITKCALL(iRetCode, LMI_process_pff_operation(pszOperation, pszFileIntName, pszExportFileName, pszFileName, pszFilePath));
    }

    return iRetCode;
}

/**
*
* This function displays the usage of this Executable
*
*/
void LMICMSUTILS_display_usage()
{
    printf("     \n\t\t\n LMICMSUTILS Utility Usage \n");

    printf("     \t\t\n-------------------------------\n");

    printf("    -u                  = <user name> to specify Teamcenter user name\n");

    printf("    -p                  = <password> to specify password\n");

    printf("    -g                  = <group name> to specify group name\n");

    printf("    -mode               = Specifies the name of mode which is executed \n\t\t\n for query mode=query, for workflow mode=workflow, for transfermode mode = transfer_mode, for stylesheet mode = stylesheet and for PFF mode = pff\n");

    printf("    -operation          = Specifies name of the operation which is currently executed\n\t\t\n for query operation=export_query_name, for workflow operation=export_workflow_name, for transfermode operation = export_transfer_mode_name, for stylesheet operation = export_stylesheet_name\n");

    printf("    -internalname       = Specifies the query or workflow or transfermode  or stylesheet internal name\n");

    printf("    -exportfilename     = Specifies the query or workflow  or transfermode  or stylesheet display name which are to be exported\n");

    printf("    -tcconfigfilename   = Specifies filename to be made against operation\n");

    printf("    -filepath           = Specifies filepath to used against operation for making file\n");

    printf("    -search_string      = Specifies the Name of User Requested Stylesheet name\n");

    printf("    -h                    [Help]\n");
}

#ifdef __cplusplus
}
#endif