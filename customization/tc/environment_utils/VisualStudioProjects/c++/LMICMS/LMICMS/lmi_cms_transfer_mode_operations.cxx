/*======================================================================================================================================================================
                                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_transfer_mode_operations.cxx

Description:  This File contains custom functions for Transfer Mode operations..

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-July-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#include <lmi_cms_library.hxx>
#include <lmi_cms_string_macros.hxx>
#include <lmi_cms_itk_mem_free.hxx>
#include <lmi_cms_itk_errors.hxx>
#include <lmi_cms_const.hxx>
#include <lmi_cms_transfer_mode_operations.hxx>

/**
 *
 * This function is used for transfermode related opertaions which read all arguments value stored as input arg
 *  variables and passes to other functions.
 *
 * @param[in] pszOperation Operation name
 * @param[in] pszFilename  Output Filename
 * @param[in] pszFilepath  Output Filepath
 *
 * @retval ITK_ok If everything went okay.
 */
int LMI_process_transfer_mode_operation
(
    char* pszOperation,
    char* pszFileName,
    char* pszFilePath
)
{
    int iRetCode = ITK_ok;

    if (tc_strcmp(pszOperation, LMI_EXPORTTRANSFERMODENAME) == 0)
    {
        LMI_ITKCALL(iRetCode, LMI_create_file_for_transfer_mode_name(pszFileName, pszFilePath));
    }
   
    return iRetCode;
}

/**
*
* This function creates file  for transfermodenames. Filename and Filepath are used as input argument to create file filled
* with transfermodenames fetched from the system at the specified location coming in FilePath.
*
* @param[in] pszFilename Filename to write transfermode names
* @param[in] pszFilepath Location of file used for file creation
*
* @retval ITK_ok If everything went okay.
*/
int LMI_create_file_for_transfer_mode_name
(
    char* pszFileName,
    char* pszFilePath
)
{
    int    iRetCode        = ITK_ok;
    int    iTransferModeNo = 0;
    tag_t* ptTransferModes = NULL;

    vector<string> vTransferModeNames;
    vector<string> vTransferModeUids;

    /*It returns no and tag of transfer modes*/
    LMI_ITKCALL(iRetCode, PIE_extent_transfer_modes(&iTransferModeNo, &ptTransferModes));

    for (int iDx = 0; iDx < iTransferModeNo && iRetCode == ITK_ok; iDx++)
    {
        char* pszUid              = NULL;
        char* pszTransferModeName = NULL;

        /*It returns UID as string*/
        LMI_ITKCALL(iRetCode, POM_tag_to_uid(ptTransferModes[iDx], &pszUid));
       
        /*This function returns the name of the input WorkspaceObject.*/
        LMI_ITKCALL(iRetCode, WSOM_ask_name2(ptTransferModes[iDx], &pszTransferModeName));

        if (tc_strlen(pszTransferModeName) > 0 && pszTransferModeName != NULL && iRetCode == ITK_ok)
        {
            vTransferModeNames.push_back(pszTransferModeName);

            vTransferModeUids.push_back(pszUid);
        }

        LMI_MEM_TCFREE(pszUid);
        LMI_MEM_TCFREE(pszTransferModeName);
    }

    /*File write operations*/
    LMI_ITKCALL(iRetCode, LMI_write_name_and_uid_to_file(vTransferModeNames, vTransferModeUids, pszFilePath, pszFileName));

    LMI_MEM_TCFREE(ptTransferModes);
    return iRetCode;
}
