/*======================================================================================================================================================================
                                                    Copyright 2020  LMTec India Consulting Pvt Ltd
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename:     lmi_cms_stylesheet_operations.hxx

Description:  Header File containing declarations of functions present in lmi_cms_stylesheet_operations.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15-July-20    Mohit Kumar         Initial Release

========================================================================================================================================================================*/
#ifndef LMI_STYLESHEET_OPERATIONS_HXX
#define LMI_STYLESHEET_OPERATIONS_HXX

int LMI_store_stylesheet_name
(
    char*   pszQueryName,
    char*   pszExportQueryName,
    char*   pszSeparator,
    int*    piQueryNameCount,
    char*** pppszQueryNamelist,
    int*    piQueryDispNameCount,
    char*** pppszQueryDispNamelist
);

int LMI_print_stylesheet_name_and_uid
(
    int    iQueryCount,
    int    iExpQueryCount,
    char*  pszFilenam,
    char*  pszFileloc,
    char** pszQueryname,
    char** pszExpQueryname
);

int LMI_create_file_for_stylesheet_name
(
    char* pszFilename,
    char* pszFilepath,
    char* pszUserRequest
);

int LMI_execute_saved_queries
(
    char*   pszQueryName,
    int     iUserEntryCount,
    char**  ppszUserEntriesName,
    char**  ppszUserEntriesValue,
    int*    piNoOfObjFound,
    tag_t** ptObjTag
);

int LMI_process_stylesheet_operation
(
    char* pszOperation,
    char* pszStylesheetName,
    char* pszExportStylesheetName,
    char* pszFileName,
    char* pszFilePath,
    char* pszUserRequest
);

#endif
