﻿/*======================================================================================================================================================================
                                Copyright 2020  LMtec India
                                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Warning Display.xaml.cs

Description: This file contains operations related to Warning Display Dialog Box.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Windows;

namespace LMICMS.Dialogs
{
    public partial class WarningDisplay : LMIWindow
    {
       
        string strOutMessg = string.Empty;
        string strOutMsg   = string.Empty;
        public WarningDisplay(string strMessage, string strOperationMessage)
        { 
            strOutMessg = string.Empty;
            strOutMsg   = string.Empty;
            strOutMessg = strMessage;
            strOutMsg = strOperationMessage;
            InitializeComponent();
        }
        private void DialogLoaded(object sender, RoutedEventArgs e)
        {
            lblTopBar.Content = strOutMsg;
            tb.Text = strOutMessg;      
        }

        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnCloseClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
