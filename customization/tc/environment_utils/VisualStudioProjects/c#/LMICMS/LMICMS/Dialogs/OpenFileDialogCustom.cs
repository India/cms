﻿/*======================================================================================================================================================================
                Copyright 2020  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: OpenFileDialogCustom.cs

Description: This file contains operations related to OpenFileDialog.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using LMICMS.Common.Utilities;

namespace LMICMS.Dialogs
{
    public class OpenFileDialogCustom : CommonDialog
    {
        private OpenFileDialog dialog = new OpenFileDialog();

        public string ValidExtension { get; set; }
        public bool IsFileNew { get; set; }

        public bool ValidateNames
        {
            get { return dialog.ValidateNames; }
            set { dialog.ValidateNames = value; }
        }

        public bool Multiselect
        {
            get { return dialog.Multiselect; }
            set { dialog.Multiselect = value; }
        }

        public bool CheckFileExists
        {
            get { return dialog.CheckFileExists; }
            set { dialog.CheckFileExists = value; }
        }
       

        public bool CheckPathExists
        {
            get { return dialog.CheckPathExists; }
            set { dialog.CheckPathExists = value; }
        }

        public OpenFileDialog Dialog
        {
            get { return dialog; }
            set { dialog = value; }
        }

        public String Filter
        {
            get { return dialog.Filter; }
            set { dialog.Filter = value; }
        }

        public override void Reset()
        {
            dialog.Reset();
        }

        protected override bool RunDialog(IntPtr hwndOwner)
        {
            return true;
        }

        public override bool? ShowDialog()
        {
            bool flag = false;
            if (dialog.ShowDialog() == true)
            {
                if (File.Exists(dialog.FileName))
                {
                    flag = true;
                }
                else
                {
                    if (String.Compare(Path.GetExtension(dialog.FileName), ValidExtension) == 0)
                    {
                        String message = dialog.FileName + "doesn't exist. Create it?";
                        MessageBoxResult result = MessageBox.Show(message, "Create new file", MessageBoxButton.YesNo);
                        if (result == MessageBoxResult.Yes)
                        {
                            FileHelper.SaveFile(dialog.FileName, "");
                            flag = true;
                            IsFileNew = true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid file extension", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
            return flag;
        }
    }
}
