﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: CreateorUpdateRepo.xaml.cs

Description: This file contains operations for CreateorUpdateRepo.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Settings;
using LMICMS.Factories;
using LMICMS.Modules.Configuration;

namespace LMICMS.Dialogs
{
    public partial class CreateOrUpdateRepo : LMIWindow
    {
        public String Message { get; set; }

        public CreateOrUpdateRepo(string strRepoGuid = "")
        {
            InitializeComponent();
            if (!String.IsNullOrEmpty(strRepoGuid))
            {
                LMICMS.Modules.Configuration.CreateOrUpdateRepo objRepoModel = SettingsModelFactory.GetRepoDetailsModel(ConfigConstants.ActiveProjectGuid, strRepoGuid);
                if (objRepoModel != null)
                {
                    txtRepoName.Text = objRepoModel.RepoName;
                    txtRepoPath.Text = objRepoModel.RepoPath;
                    txtWorflowPath.Text = objRepoModel.WorkflowPath;
                    txtAuthorizationPath.Text = objRepoModel.AuthorizationPath;
                    txtOrganizationPath.Text = objRepoModel.OrganizationPath;
                    txtACLPath.Text = objRepoModel.ACLPath;
                    txtPFFPath.Text = objRepoModel.PFFPath;
                    txtStylesheetPath.Text = objRepoModel.StylesheetPath;
                    txtPreferencePath.Text = objRepoModel.PreferencePath;
                    txtQueryPath.Text = objRepoModel.QueryPath;
                    txtTransferModePath.Text = objRepoModel.TransferModePath;
                    btnSave.CommandParameter = strRepoGuid;
                }
            }
        }

      
        private void BtnSetPath(object sender, RoutedEventArgs e)
        {
            Button btnSave = (Button)e.OriginalSource;
            String strButtonName = Convert.ToString(btnSave.CommandParameter);

            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = txtRepoPath.Text.Trim();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (String.Compare("Repo", strButtonName) == 0)
                {
                    string strOldRepoPath = txtRepoPath.Text;
                    txtRepoPath.Text = dialog.SelectedPath;
                    if (String.Compare(strOldRepoPath, dialog.SelectedPath) != 0)
                    {
                        txtWorflowPath.Text = string.Empty;
                        txtAuthorizationPath.Text = string.Empty;
                        txtAuthorizationPath.Text = string.Empty;
                        txtACLPath.Text = string.Empty;
                        txtPFFPath.Text = string.Empty;
                        txtStylesheetPath.Text = string.Empty;
                        txtPreferencePath.Text = string.Empty;
                        txtQueryPath.Text = string.Empty;
                        txtTransferModePath.Text = string.Empty;
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(txtRepoPath.Text))
                    {
                        MessageBox.Show(ResourceHelper.FindResource("COUR.SetRepoPath"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        if (!dialog.SelectedPath.Contains(txtRepoPath.Text))
                        {
                            MessageBox.Show(ResourceHelper.FindResource("COUR.InvalidPath"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        else
                        {
                            switch (strButtonName)
                            {
                                case Constants.Workflow:
                                    txtWorflowPath.Text = dialog.SelectedPath;
                                    break;
                                case Constants.Authorization:
                                    txtAuthorizationPath.Text = dialog.SelectedPath;
                                    break;
                                case Constants.Organization:
                                    txtOrganizationPath.Text = dialog.SelectedPath;
                                    break;
                                case Constants.ACL:
                                    txtACLPath.Text = dialog.SelectedPath;
                                    break;
                                case Constants.PFF:
                                    txtPFFPath.Text = dialog.SelectedPath;
                                    break;
                                case Constants.Stylesheet:
                                    txtStylesheetPath.Text = dialog.SelectedPath;
                                    break;
                                case Constants.Preference:
                                    txtPreferencePath.Text = dialog.SelectedPath;
                                    break;
                                case Constants.Query:
                                    txtQueryPath.Text = dialog.SelectedPath;
                                    break;
                                case Constants.TransferMode:
                                    txtTransferModePath.Text = dialog.SelectedPath;
                                    break;
                            }
                        }
                    }
                }
            }

            e.Handled = true;
        }

        private void BtnSaveClick(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtRepoName.Text) && !String.IsNullOrEmpty(txtRepoPath.Text))
            {
                Button btnSave = (Button)e.OriginalSource;
                String strRepoGuid = Convert.ToString(btnSave.CommandParameter);

                Repo repo = new Repo();
                repo.RepoName = txtRepoName.Text;
                repo.RepoPath = txtRepoPath.Text;
                if (!String.IsNullOrEmpty(txtWorflowPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Constants.Workflow, AppDataPath = txtWorflowPath.Text });
                }
                if (!String.IsNullOrEmpty(txtAuthorizationPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Constants.Authorization, AppDataPath = txtAuthorizationPath.Text });
                }
                if (!String.IsNullOrEmpty(txtOrganizationPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Constants.Organization, AppDataPath = txtOrganizationPath.Text });
                }
                if (!String.IsNullOrEmpty(txtACLPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Constants.ACL, AppDataPath = txtACLPath.Text });
                }
                if (!String.IsNullOrEmpty(txtPFFPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Constants.PFF, AppDataPath = txtPFFPath.Text });
                }
                if (!String.IsNullOrEmpty(txtStylesheetPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Constants.Stylesheet, AppDataPath = txtStylesheetPath.Text });
                }
                if (!String.IsNullOrEmpty(txtPreferencePath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Constants.Preference, AppDataPath = txtPreferencePath.Text });
                }
                if (!String.IsNullOrEmpty(txtQueryPath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Constants.Query, AppDataPath = txtQueryPath.Text });
                }
                if (!String.IsNullOrEmpty(txtTransferModePath.Text))
                {
                    repo.AppDataLocs.Add(new AppData() { AppDataName = Constants.TransferMode, AppDataPath = txtTransferModePath.Text });
                }

                if (String.IsNullOrEmpty(strRepoGuid))
                {
                    repo.RepoGuid = Guid.NewGuid().ToString();
                    SettingsModule.SaveRepo(ConfigConstants.ActiveProjectGuid, repo);
                }
                else
                {
                    repo.RepoGuid = strRepoGuid;
                    SettingsModule.SaveRepo(ConfigConstants.ActiveProjectGuid, repo);
                }

                MessageBox.Show(ResourceHelper.FindResource("COUR.RepoSaved"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);

                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show(ResourceHelper.FindResource("COUR.PathNameMandate"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            e.Handled = true;
        }
        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            e.Handled = true;
        }
    }
}
