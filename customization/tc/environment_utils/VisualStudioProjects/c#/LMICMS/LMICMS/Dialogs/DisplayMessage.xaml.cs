﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: DisplayMessage.xaml.cs

Description: This file contains operations related to DisplayMessage

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Windows;
using System.Windows.Input;

namespace LMICMS.Dialogs
{
    public partial class DisplayMessage : LMIWindow
    {
        string strOutMsg = string.Empty;
        public DisplayMessage(string strMsg)
        {
            strOutMsg = string.Empty;
            strOutMsg = strMsg;
            InitializeComponent();
        }

        private void DialogLoaded(object sender, RoutedEventArgs e)
        {
            tb.Text = strOutMsg;
        }


        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnCloseClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case System.Windows.Input.Key.C:
                case System.Windows.Input.Key.V:
                case System.Windows.Input.Key.A:
                    if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                        return;
                    break;

                case Key.Left:
                case Key.Up:
                case Key.Right:
                case Key.Down:
                case Key.PageDown:
                case Key.PageUp:
                case Key.Home:
                case Key.End:
                    return;
            }
            e.Handled = true;
        }
    }
}
             
            
