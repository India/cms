﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: StylesheetExportUC.xaml.cs

Description: This file contains various operations for exporting Stylesheet.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Configuration;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using LMICMS.Common.Constants;

namespace LMICMS.Dialogs
{
    public class LMIWindow:Window
    {
        public LMIWindow()
        {
            this.FontFamily = new FontFamily(ConfigurationManager.AppSettings[ConfigConstants.DefaultFontFamily]);
            this.MouseDown += new MouseButtonEventHandler(WindowMouseDown);
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.ResizeMode = ResizeMode.NoResize;
            this.WindowStyle = WindowStyle.None;
            this.BorderThickness = new Thickness(1);
        }

        private void WindowMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

    }
}
