﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Display.xaml.cs

Description: This file contains operations related to Display Dialog Box.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Windows;

namespace LMICMS.Dialogs
{ 
    public partial class Display : LMIWindow
    {
        string strOutMessage = string.Empty;
        string strOutMessg   = string.Empty;
        string strOutMsg     = string.Empty;
        public Display( string strInpMessage, string strInpMessg, string strInpMsg)
        {
           strOutMessage = string.Empty;
           strOutMessg = string.Empty;
           strOutMsg = string.Empty;
           strOutMessage = strInpMessage;
           strOutMessg = strInpMessg;
           strOutMsg = strInpMsg; 
           InitializeComponent();
        }

        public Display()
        {
        }

        private void DialogLoaded(object sender, RoutedEventArgs e)
        {
            lblTopBar.Content = strOutMessg;
            tb.Text = strOutMsg;      
        }

        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnCloseClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Info_Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayMessage displayMessage = new DisplayMessage(strOutMessage);

            displayMessage.ShowDialog();
        }

       
    }
}
