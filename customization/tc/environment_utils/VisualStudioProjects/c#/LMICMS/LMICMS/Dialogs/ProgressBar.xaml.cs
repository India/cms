﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: ProgressBar.xaml.cs

Description: This file contains operations related to Progress Bar.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.ComponentModel;
using System.Windows;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Dialogs
{
    public partial class ProgressBar : LMIWindow
    {
        BackgroundWorker bgwProcessing = null;
        Object inpObj = new Object();
        int iEventType = Constants.REQ_EMPTY;
        Object val = new object();
        Object value = new object();

        public ProgressBar(Object inpObj, int iEventType, Object val, Object value)
        {
            this.inpObj = inpObj;

            this.iEventType = iEventType;

            this.val = val;

            this.value = value;

            InitializeComponent();
            bgwProcessing  = new BackgroundWorker();
            bgwProcessing.DoWork += bgwProcessing_DoWork; 
            bgwProcessing.RunWorkerCompleted += bgwProcessing_RunWorkerCompleted;
            bgwProcessing.RunWorkerAsync();
        }
        private void bgwProcessing_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ReqandResp reqandResp = new ReqandResp();
                reqandResp.processReqEvent(inpObj, iEventType, val, value);
            }
            catch (Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
        }
        private void bgwProcessing_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {  
                ReqandResp reqandResp = new ReqandResp();

                /* This is the case if any Exception has Occured during the background operation */
                if (e.Error != null)
                {
                    string strErrorString = null;

                    if (e.Error.InnerException != null && e.Error.InnerException.ToString().Length > 0)
                    {
                        strErrorString = e.Error.InnerException.ToString() + Constants.SEP_NEWLINE;
                    }

                    if (e.Error.Message != null && e.Error.Message.ToString().Length > 0)
                    {
                        strErrorString = strErrorString + e.Error.Message;
                    }

                    throw new Exception(strErrorString);
                }
                else if (e.Cancelled == true)
                {
                    reqandResp.handleResponseEvent(sender, Constants.REQ_CANCELLED);
                }
                else
                {
                    endProgress();
                }
            }
            catch (Exception ex)
            {
                /*  Any Exceptions that have occurred in background thread, are caught here, so no need to further throw exception from here,
                 *  Just displaying them in dialog.
                 **/
                Logger.getInstance().error(ex.Message);
                endProgress(); 
            }
            endProgress();
        }

        private void ProgressBar_Loaded(object sender, RoutedEventArgs e)
        {
            try
            { 
                /*  If thread is not busy and thread is not cancelled, then starting background worker thread */
                if (!bgwProcessing.IsBusy && !bgwProcessing.CancellationPending)
                {
                    bgwProcessing.RunWorkerAsync();
                }

            }
            catch (Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
        }
      
        /*  Progress Bar End */
        public void endProgress()
        {
            this.Close();
        }

        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

    

