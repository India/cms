﻿/*======================================================================================================================================================================
                                Copyright 2020  LMtec India
                                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: CreateorUpdateConfiguration.xaml.cs

Description: This file contains operations for CreateorUpdateConfiguration.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Settings;
using LMICMS.Factories;
using LMICMS.Modules.Configuration;

namespace LMICMS.Dialogs
{
    public partial class CreateOrUpdateConfiguration : LMIWindow
    {
        public String Message { get; set; }

        public CreateOrUpdateConfiguration(string strConfigGuid = "")
        {
            InitializeComponent();

            var lstAvilableUsers = SettingsModelFactory.PrepareAvailableUserModels(ConfigConstants.ActiveProjectGuid);
            cbUsers.ItemsSource = lstAvilableUsers;
            cbUsers.Items.Refresh();

            if (!String.IsNullOrEmpty(strConfigGuid))
            {
                TCConfiguration objConfig = SettingsModule.GetConfiguration(ConfigConstants.ActiveProjectGuid, strConfigGuid);
                if (objConfig != null)
                {
                    txtConfigName.Text = objConfig.ConfigName;
                    txtTCData.Text = objConfig.TCData;
                    txtTCRoot.Text = objConfig.TCRoot;
                    btnSave.CommandParameter = objConfig.ConfigGuid;
                    cbUsers.SelectedItem = lstAvilableUsers.FirstOrDefault(user => String.Compare(user.Guid, objConfig.LMIActiveUserGuid) == 0);              
                }
            }
        }

        private void CbUsersSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void BtnSaveClick(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtConfigName.Text) && !string.IsNullOrEmpty(txtTCData.Text) &&
                !string.IsNullOrEmpty(txtTCRoot.Text))
            {
                Button btnSave = (Button)e.OriginalSource;
                String strConfigGuid = Convert.ToString(btnSave.CommandParameter);

                TCConfiguration objConfig = new TCConfiguration();
                objConfig.ConfigName = txtConfigName.Text.Trim();
                objConfig.TCData = txtTCData.Text.Trim();
                objConfig.TCRoot = txtTCRoot.Text.Trim();
                var activeUser = (cbUsers.SelectedItem as AvailableUserModel);
                if (activeUser != null)
                {
                    objConfig.LMIActiveUserGuid = activeUser.Guid;
                }

                if (String.IsNullOrEmpty(strConfigGuid))
                {
                    objConfig.ConfigGuid = Guid.NewGuid().ToString();
                    SettingsModule.SaveConfiguration(ConfigConstants.ActiveProjectGuid, objConfig);
                }
                else
                {
                    objConfig.ConfigGuid = strConfigGuid;
                    SettingsModule.SaveConfiguration(ConfigConstants.ActiveProjectGuid, objConfig);
                }
                MessageBox.Show(ResourceHelper.FindResource("COUC.ConfiguarationSaved"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show(ResourceHelper.FindResource("COUC.AllFieldsMand"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            e.Handled = true;
        }

        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            e.Handled = true;
        }

        private void BtnTCRootClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = txtTCData.Text.Trim();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtTCRoot.Text = dialog.SelectedPath;
            }
        }

        private void BtnTCDataClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = txtTCData.Text.Trim();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtTCData.Text = dialog.SelectedPath;
            }
        }
    }
}
