﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: LMICMS.cs

Description: This file contains operations related to LMICMS

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Xml.Serialization;

namespace LMICMS.Settings
{
    [XmlRoot(ElementName = "LMI_CMS")]
    public class LMICMSRoot
    {
        [XmlElement(ElementName = "ACTIVE_PROJECT_GUID")]
        public string ActiveProjectGuid { get; set; }
        [XmlElement(ElementName = "LMI_TEMP_DOWNLOAD_PATH")]
        public string LMITempDownloadPath { get; set; }
        [XmlElement(ElementName = "LMI_CMS_EXE_LOC")]
        public string LMICMSExeLoc { get; set; }
        [XmlElement(ElementName = "LMI_CONFIG_FILE_LOC")]
        public string LMIConfigFileLoc { get; set; }
        [XmlElement(ElementName = "PROJECTS_DETAILS")]
        public ProjectsDetails ProjectsDetails { get; set; }
    }
}
