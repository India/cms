﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: TCConfiguration.cs

Description: This file contains operations related to TCConfiguration

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LMICMS.Settings
{
    [XmlRoot(ElementName = "TC_CONFIGURATIONS")]
    public class TCConfigurations
    {
        [XmlElement(ElementName = "TC_CONFIGURATION")]
        public List<TCConfiguration> TCConfigs { get; set; }
    }

    [XmlRoot(ElementName = "TC_CONFIGURATION")]
    public class TCConfiguration
    {
        [XmlAttribute(AttributeName = "CONFIG_GUID")]
        public string ConfigGuid { get; set; }
        [XmlAttribute(AttributeName = "CONFIG_NAME")]
        public string ConfigName { get; set; }
        [XmlElement(ElementName = "TC_ROOT")]
        public string TCRoot { get; set; }
        [XmlElement(ElementName = "TC_DATA")]
        public string TCData { get; set; }
        [XmlElement(ElementName = "LMI_ACTIVE_USER_GUID")]
        public string LMIActiveUserGuid { get; set; }
    }
}
