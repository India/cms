﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: TCUser.cs

Description: This file contains operations related to TCUser

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LMICMS.Settings
{
    [XmlRoot(ElementName = "TC_USERS_DETAILS")]
    public class TCUsersDetails
    {
        [XmlElement(ElementName = "TC_USER")]
        public List<TCUser> TCUsers { get; set; }
    }

    [XmlRoot(ElementName = "TC_USER")]
    public class TCUser
    {
        [XmlAttribute(AttributeName = "USER_GUID")]
        public string UserGuid { get; set; }
        [XmlAttribute(AttributeName = "USER_ID")]
        public string UserId { get; set; }
        [XmlAttribute(AttributeName = "USER_PASSWORD")]
        public string UserPassword { get; set; }
        [XmlAttribute(AttributeName = "USER_GROUP")]
        public string UserGroup { get; set; }
        [XmlAttribute(AttributeName = "USER_ROLE")]
        public string UserRole { get; set; }
    }
}
