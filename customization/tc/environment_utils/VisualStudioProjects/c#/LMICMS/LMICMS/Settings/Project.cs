﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: Project.cs

Description: This file contains operations related to Project

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LMICMS.Settings
{
    [XmlRoot(ElementName = "PROJECTS_DETAILS")]
    public class ProjectsDetails
    {
        [XmlElement(ElementName = "PROJECT")]
        public List<Project> Projects { get; set; }
    }

    [XmlRoot(ElementName = "PROJECT")]
    public class Project
    {
        [XmlAttribute(AttributeName = "PROJECT_GUID")]
        public string ProjectGuid { get; set; }
        [XmlAttribute(AttributeName = "PROJECT_NAME")]
        public string ProjectName { get; set; }
        [XmlElement(ElementName = "ACTIVE_CONFIG_GUID")]
        public string ActiveConfigGuid { get; set; }
        [XmlElement(ElementName = "ACTIVE_REPO_GUID")]
        public string ActiveRepoGuid { get; set; }
        [XmlElement(ElementName = "TC_CONFIGURATIONS")]
        public TCConfigurations TCConfigurations { get; set; }
        [XmlElement(ElementName = "TC_USERS_DETAILS")]
        public TCUsersDetails TCUsersDetails { get; set; }
        [XmlElement(ElementName = "SRC_CONTROL_LOC")]
        public SRCControlLocation SRCControlLocation { get; set; }
    }
}
