﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: SettingsModelFactory.cs

Description: This file contains operations related to SettingsModelFactory

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Modules.Configuration;

namespace LMICMS.Factories
{
    public static class SettingsModelFactory
    {
        public static IList<AvailableUserModel> PrepareAvailableUserModels(string strProjectGuid) 
        {
            IList<AvailableUserModel> lstUsers = new List<AvailableUserModel>();
            var lstAvailableUsers = SettingsModule.GetUsersForProject(strProjectGuid);
            foreach (var user in lstAvailableUsers)
            {
                lstUsers.Add(new AvailableUserModel()
                {
                    DisplayValue = "-user id: " + user.UserId + " -group: " + user.UserGroup,
                    Guid = user.UserGuid
                });
            }
            return lstUsers;
        }

        public static CommonSettingsModel GetCommonSettings()
        {
            CommonSettingsModel model = new CommonSettingsModel();
            var objCMSRoot = SettingsModule.GetLMICMSRoot();
            if (objCMSRoot != null)
            {
                model.LMICMSExeLoc = objCMSRoot.LMICMSExeLoc;
                model.LMIConfigFileLoc = objCMSRoot.LMIConfigFileLoc;
                model.LMITempDownloadPath = objCMSRoot.LMITempDownloadPath;
            }
            return model;
        }

        public static CreateOrUpdateRepo GetRepoDetailsModel(string strProjectGuid, string strRepoGuid)
        {
            CreateOrUpdateRepo repoDetailsModel = null;
            var repo = SettingsModule.GetRepo(strProjectGuid, strRepoGuid);
            if (repo != null)
            {
                repoDetailsModel = new CreateOrUpdateRepo();
                repoDetailsModel.RepoGuid = repo.RepoGuid;
                repoDetailsModel.RepoName = repo.RepoName;
                repoDetailsModel.RepoPath = repo.RepoPath;
                repoDetailsModel.ACLPath = repo.AppDataLocs.FirstOrDefault(x => String.Compare(x.AppDataName, "ACL") == 0)?.AppDataPath;
                repoDetailsModel.AuthorizationPath = repo.AppDataLocs.FirstOrDefault(x => String.Compare(x.AppDataName, "Authorization") == 0)?.AppDataPath;
                repoDetailsModel.OrganizationPath = repo.AppDataLocs.FirstOrDefault(x => String.Compare(x.AppDataName, "Organization") == 0)?.AppDataPath;
                repoDetailsModel.PFFPath = repo.AppDataLocs.FirstOrDefault(x => String.Compare(x.AppDataName, "PFF") == 0)?.AppDataPath;
                repoDetailsModel.PreferencePath = repo.AppDataLocs.FirstOrDefault(x => String.Compare(x.AppDataName, "Preference") == 0)?.AppDataPath;
                repoDetailsModel.QueryPath = repo.AppDataLocs.FirstOrDefault(x => String.Compare(x.AppDataName, "Query") == 0)?.AppDataPath;
                repoDetailsModel.StylesheetPath = repo.AppDataLocs.FirstOrDefault(x => String.Compare(x.AppDataName, "Stylesheet") == 0)?.AppDataPath;
                repoDetailsModel.TransferModePath = repo.AppDataLocs.FirstOrDefault(x => String.Compare(x.AppDataName, "TransferMode") == 0)?.AppDataPath;
                repoDetailsModel.WorkflowPath = repo.AppDataLocs.FirstOrDefault(x => String.Compare(x.AppDataName, "Workflow") == 0)?.AppDataPath;
            }
            return repoDetailsModel;
        }

        public static IList<TCConfigurationModel> GetConfigurationModels(string strProjectGuid)
        {
            IList<TCConfigurationModel> configModels = new List<TCConfigurationModel>();
            var lstConfigs = SettingsModule.GetConfigurations(strProjectGuid);

            TCConfigurationModel objConfigModel = null;
            foreach (var config in lstConfigs)
            {
                objConfigModel = new TCConfigurationModel();
                objConfigModel.ConfigGuid = config.ConfigGuid;
                objConfigModel.ConfigName = "Name: " + config.ConfigName;
                objConfigModel.TCRoot = "TC Root: " + (!String.IsNullOrEmpty(config.TCRoot) ? config.TCRoot : "Not set yet");
                objConfigModel.TCData = "TC Data: " + (!String.IsNullOrEmpty(config.TCData) ? config.TCData : "Not set yet");
                objConfigModel.LMIActiveUserGuid = config.LMIActiveUserGuid;

                var objActiveUser = SettingsModule.GetUserByGuid(config.LMIActiveUserGuid);
                if (objActiveUser != null)
                {
                    objConfigModel.ActiveUser = "ActiveUser: " + String.Format("{0}_{1}_{2}", objActiveUser.UserId, objActiveUser.UserGroup, objActiveUser.UserRole);
                }

                if (String.Compare(config.ConfigGuid, ConfigConstants.ActiveConfigGuid) == 0)
                {
                    objConfigModel.Active = true;
                }
                configModels.Add(objConfigModel);
            }
            return configModels;
        }

        public static IList<RepoModel> GetRepoModels(string strProjectGuid)
        {
            IList<RepoModel> repoModels = new List<RepoModel>();
            var lstRepos = SettingsModule.GetRepos(strProjectGuid);

            RepoModel repoModel = null;
            foreach (var repo in lstRepos)
            {
                repoModel = new RepoModel();
                repoModel.RepoGuid = repo.RepoGuid;
                repoModel.RepoName = repo.RepoName;
                repoModel.RepoPath = repo.RepoPath;

                repoModel.AppDataLocs = ResourceHelper.FindResource("COUR.RepoName") + " : " + repoModel.RepoName + Environment.NewLine;
                repoModel.AppDataLocs += ResourceHelper.FindResource("COUR.RepoPath") + " : " + repoModel.RepoPath + Environment.NewLine;

                foreach (var appDataLoc in repo.AppDataLocs)
                {
                    repoModel.AppDataLocs += appDataLoc.AppDataName + " : " + appDataLoc.AppDataPath + Environment.NewLine;
                }

                if (String.Compare(repo.RepoGuid, ConfigConstants.ActiveRepoPathGuid) == 0)
                {
                    repoModel.Active = true;
                }

                repoModels.Add(repoModel);
            }

            return repoModels;
        }
    }
}
