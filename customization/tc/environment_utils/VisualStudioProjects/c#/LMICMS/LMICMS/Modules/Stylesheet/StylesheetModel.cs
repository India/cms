﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: StylesheetModel.cs

Description: This file contains operations related to Stylesheet Names.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;

namespace LMICMS.Modules.Configuration
{
  public  class StylesheetModel
    {
      //  public bool BooleanFlag { get; set; }
        public Boolean bIsActive { get; set; } = false;
        public string StylesheetName { get; set; }
        public string FileName { get; set; }
    }
}
