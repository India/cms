﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Query Model.cs

Description: This file contains operations related to Query Names.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Modules.Pff
{
   public class PffModel
    {
        public Boolean bIsActive { get; set; } = false;
        public string PffName { get; set; }
        public string FileName { get; set; }
        public string UID { get; set; }
    }
}
