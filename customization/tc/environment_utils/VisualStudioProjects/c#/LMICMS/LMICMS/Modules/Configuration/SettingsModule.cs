﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: SettingsModule.cs

Description: This file contains various operation related to SettingsModule.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Settings;
using LMICMS.Services;

namespace LMICMS.Modules.Configuration
{
    public class SettingsModule
    {
        #region Project

        public static IList<ProjectModel> GetAllProjects()
        {
            IList<ProjectModel> listProjects = new List<ProjectModel>();

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strProjectTagPath = String.Format(@"//{0}/{1}/{2}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject);

                var projectsList = xmlDoc.SelectNodes(strProjectTagPath);
                foreach (XmlNode project in projectsList)
                {
                    if (project.Attributes != null && project.Attributes[ConfigConstants.AttributeProjectGuid] != null)
                    {
                        ProjectModel objProject = new ProjectModel();
                        objProject.ProjectGuid = project.Attributes[ConfigConstants.AttributeProjectGuid].InnerText.Trim();
                        objProject.ProjectName = project.Attributes[ConfigConstants.AttributeProjectName].InnerText.Trim();
                       
                        if (String.Compare(objProject.ProjectGuid, ConfigConstants.ActiveProjectGuid) == 0)
                        {
                            objProject.Active = true;
                        }

                        listProjects.Add(objProject);
                    }
                }
            }
            catch (Exception exc)
            {
              
            }

            return listProjects;
        }

        public static void GetProjectElementByGuid(string strProjectGuid, XmlDocument xmlDoc, ref XmlElement projectElement)
        {
            try
            {
                string strProjectPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid);

                //find existing
                projectElement = (XmlElement)xmlDoc.SelectSingleNode(strProjectPath);

            }
            catch (Exception exc)
            {
                
            }
        }

        public static ProjectModel GetProjectByGuid(string strProjectGuid)
        {
            ProjectModel objProject = null;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                XmlElement projectElement = null;
                GetProjectElementByGuid(strProjectGuid, xmlDoc, ref projectElement);

                if (projectElement != null)
                {
                    objProject = new ProjectModel();
                    foreach (XmlNode node in projectElement.ChildNodes)
                    {
                        
                    }

                    if (String.Compare(objProject.ProjectGuid, ConfigConstants.ActiveProjectGuid) == 0)
                    {
                        objProject.Active = true;
                    }
                }
            }
            catch (Exception exc)
            {
                
            }

            return objProject;
        }

        public static bool SaveProject(ProjectModel objProject)
        {
            bool flag = false;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strProjectsDetailsPath = String.Format(@"//{0}/{1}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails);
                var projectDetailsTag = (XmlElement)xmlDoc.SelectSingleNode(strProjectsDetailsPath);

                string strProjectTagPath = String.Format(@"{0}/{1}[@{2}='{3}']", strProjectsDetailsPath, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid, objProject.ProjectGuid);

                var project = xmlDoc.SelectSingleNode(strProjectTagPath);

                if (project == null)
                {
                    string strConfigName = ConfigurationManager.AppSettings[ConfigConstants.DefaultConfigName];
                    string strRepoName = ConfigurationManager.AppSettings[ConfigConstants.DefaultRepoName];
                    ConfigService.AddProjectConfig(objProject.ProjectName, objProject.ProjectGuid, strConfigName, strRepoName, ref projectDetailsTag, ref xmlDoc);
                }
                else
                {
                    project.Attributes[ConfigConstants.AttributeProjectName].InnerText = objProject.ProjectName;
                }

                if (objProject.Active == true)
                {
                    string strActiveProjectGuidPath = String.Format(@"//{0}/{1}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.ElementActiveProjectGuid);

                    var activeProjectGuidElement = (XmlElement)xmlDoc.SelectSingleNode(strActiveProjectGuidPath);

                    if (activeProjectGuidElement != null)
                    {
                        activeProjectGuidElement.InnerText = objProject.ProjectGuid;
                    }
                }

                flag = ConfigService.SaveConfigFile(xmlDoc);
            }
            catch (Exception ex)
            {

                throw;
            }
            return flag;
        }

        public static bool SetProjectActive(string strProjectGuid)
        {
            bool flag = false;
            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strActiveProjectGuidPath = String.Format(@"//{0}/{1}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.ElementActiveProjectGuid);

                var activeProjectGuidElement = (XmlElement)xmlDoc.SelectSingleNode(strActiveProjectGuidPath);

                if (activeProjectGuidElement != null)
                {
                    activeProjectGuidElement.InnerText = strProjectGuid;
                    flag = ConfigService.SaveConfigFile(xmlDoc);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return flag;
        }

        public static bool DeleteProject(string strProjectGuid)
        {
            bool flag = false;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strProjectsDetailsTagPath = String.Format(@"//{0}/{1}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails);
                var projectsDetailsElement = (XmlElement)xmlDoc.SelectSingleNode(strProjectsDetailsTagPath);

                //find existing
                XmlElement projectElement = null;
                GetProjectElementByGuid(strProjectGuid, xmlDoc, ref projectElement);

                //found
                if (projectElement != null)
                {
                    projectsDetailsElement.RemoveChild(projectElement);
                }

                flag = ConfigService.SaveConfigFile(xmlDoc);
            }
            catch (Exception exc)
            {
                
            }

            return flag;
        }

        #endregion

        #region users

        public static void GetUserElementByGuid(string strUserGuid, XmlDocument xmlDoc, ref XmlElement userElement)
        {
            try
            {
                string strUserListPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}/{6}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    ConfigConstants.ActiveProjectGuid, ConfigConstants.TagUsersDetails, ConfigConstants.TagUser);

                //find existing
                var usersNodeList = xmlDoc.SelectNodes(strUserListPath);
                foreach (XmlNode userNode in usersNodeList)
                {
                    if (userNode.Attributes != null && userNode.Attributes[ConfigConstants.AttributeUserGuid] != null)
                    {
                        var str1 = userNode.Attributes[ConfigConstants.AttributeUserGuid].InnerText.Trim();
                        if (string.Compare(str1, strUserGuid) == 0)
                        {
                            userElement = (XmlElement)userNode;
                            break;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                
            }
        }

        public static TCUser GetUserByGuid(string strUserGuid, XmlDocument xmlDoc = null)
        {
            TCUser objTCUser = null;

            try
            {
                if (xmlDoc == null)
                {
                    string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                    xmlDoc = new XmlDocument();
                    xmlDoc.Load(strConfigFilePath);
                }
                
                //find existing
                XmlElement userElement = null;
                GetUserElementByGuid(strUserGuid, xmlDoc, ref userElement);

                //found
                if (userElement != null)
                {
                    objTCUser = new TCUser();
                    objTCUser.UserGuid = userElement.Attributes[ConfigConstants.AttributeUserGuid].InnerText.Trim();
                    objTCUser.UserId = userElement.Attributes[ConfigConstants.AttributeUserId].InnerText.Trim();
                    objTCUser.UserRole = userElement.Attributes[ConfigConstants.AttributeUserRole].InnerText.Trim();
                    objTCUser.UserGroup = userElement.Attributes[ConfigConstants.AttributeUserGroup].InnerText.Trim();
                    objTCUser.UserPassword = userElement.Attributes[ConfigConstants.AttributeUserPassword].InnerText.Trim();
                }
            }
            catch (Exception exc)
            {
               
            }

            return objTCUser;
        }

        public static IList<TCUser> GetAllUsers()
        {
            IList<TCUser> lstUsers = new List<TCUser>();
            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strUsersTagPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}/{6}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    ConfigConstants.ActiveProjectGuid, ConfigConstants.TagUsersDetails, ConfigConstants.TagUser);

                var nodeList = xmlDoc.SelectNodes(strUsersTagPath);
                foreach (XmlNode node in nodeList)
                {
                    if (node.Attributes != null && node.Attributes[ConfigConstants.AttributeUserGuid] != null)
                    {
                        TCUser objUser = new TCUser();
                        objUser.UserGuid = node.Attributes[ConfigConstants.AttributeUserGuid].InnerText.Trim();
                        objUser.UserId = node.Attributes[ConfigConstants.AttributeUserId].InnerText.Trim();
                        objUser.UserPassword = node.Attributes[ConfigConstants.AttributeUserPassword].InnerText.Trim();
                        objUser.UserGroup = node.Attributes[ConfigConstants.AttributeUserGroup].InnerText.Trim();
                        objUser.UserRole = node.Attributes[ConfigConstants.AttributeUserRole].InnerText.Trim();
                        lstUsers.Add(objUser);
                    }
                }
            }
            catch (Exception exc)
            {
              
            }

            return lstUsers;
        }

        public static IList<TCUser> GetUsersForProject(string strProjectGuid)
        {
            IList<TCUser> lstUsers = new List<TCUser>();
            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strUsersTagPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}/{6}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.TagUsersDetails, ConfigConstants.TagUser);

                var nodeList = xmlDoc.SelectNodes(strUsersTagPath);
                foreach (XmlNode node in nodeList)
                {
                    if (node.Attributes != null && node.Attributes[ConfigConstants.AttributeUserGuid] != null)
                    {
                        TCUser objUser = new TCUser();
                        objUser.UserGuid = node.Attributes[ConfigConstants.AttributeUserGuid].InnerText.Trim();
                        objUser.UserId = node.Attributes[ConfigConstants.AttributeUserId].InnerText.Trim();
                        objUser.UserPassword = node.Attributes[ConfigConstants.AttributeUserPassword].InnerText.Trim();
                        objUser.UserGroup = node.Attributes[ConfigConstants.AttributeUserGroup].InnerText.Trim();
                        objUser.UserRole = node.Attributes[ConfigConstants.AttributeUserRole].InnerText.Trim();
                        lstUsers.Add(objUser);
                    }
                }
            }
            catch (Exception exc)
            {
             
            }

            return lstUsers;
        }

        public static bool SaveUser(TCUser user)
        {
            bool flag = false;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strUsersDetailsTagPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    ConfigConstants.ActiveProjectGuid, ConfigConstants.TagUsersDetails);
                var usersDetailsTag = (XmlElement)xmlDoc.SelectSingleNode(strUsersDetailsTagPath);

                XmlElement userTag = null;
                string strUserTagPath = strUsersDetailsTagPath + "/" + ConfigConstants.TagUser;

                //find existing
                var usersNodeList = xmlDoc.SelectNodes(strUserTagPath);
                foreach (XmlNode userNode in usersNodeList)
                {
                    if (userNode.Attributes != null && userNode.Attributes[ConfigConstants.AttributeUserGuid] != null)
                    {
                        var strGuid = userNode.Attributes[ConfigConstants.AttributeUserGuid].InnerText.Trim();

                        if (string.Compare(strGuid, user.UserGuid) == 0)
                        {
                            userTag = (XmlElement)userNode;
                            break;
                        }
                    }
                }

                //create new one if not found
                if (userTag == null)
                {
                    userTag = xmlDoc.CreateElement(ConfigConstants.TagUser);
                    userTag.SetAttribute(ConfigConstants.AttributeUserGuid, user.UserGuid);
                    usersDetailsTag.AppendChild(userTag);
                }

                userTag.SetAttribute(ConfigConstants.AttributeUserId, user.UserId);
                userTag.SetAttribute(ConfigConstants.AttributeUserPassword, user.UserPassword);
                userTag.SetAttribute(ConfigConstants.AttributeUserGroup, user.UserGroup);
                userTag.SetAttribute(ConfigConstants.AttributeUserRole, user.UserRole);

                flag = ConfigService.SaveConfigFile(xmlDoc);
            }
            catch (Exception exc)
            {
              
            }

            return flag;
        }

        public static bool IsUserAlreadyExist(TCUser objUser)
        {
            return false;
        }

        public static bool DeleteUser(string userGuid)
        {
            bool flag = false;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strUsersDetailsTagPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    ConfigConstants.ActiveProjectGuid, ConfigConstants.TagUsersDetails);
                var rootElement = (XmlElement)xmlDoc.SelectSingleNode(strUsersDetailsTagPath);

                //find existing
                XmlElement userElement = null;
                GetUserElementByGuid(userGuid, xmlDoc, ref userElement);

                //found
                if (userElement != null)
                {
                    rootElement.RemoveChild(userElement);
                }

                flag = ConfigService.SaveConfigFile(xmlDoc);
            }
            catch (Exception exc)
            {
                
            }

            return flag;
        }

        #endregion

        #region configuration

        public static void GetConfigNodeByGuid(string strConfigGuid, string strProjectGuid, XmlDocument xmlDoc, ref XmlNode configNode)
        {
            try
            {
                string strConfigPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}/{6}[@{7}='{8}']", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.TagTCConfigurations, ConfigConstants.TagTCConfiguration, ConfigConstants.AttributeConfigGuid, strConfigGuid);

                //find existing
                configNode = xmlDoc.SelectSingleNode(strConfigPath);

            }
            catch (Exception exc)
            {
                //Debug.Write(exc.ToString());
            }
        }

        public static TCConfiguration GetConfiguration(string strProjectGuid, string strConfigGuid)
        {
            TCConfiguration objConfiguration = null;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                XmlNode configNode = null;
                GetConfigNodeByGuid(strConfigGuid, strProjectGuid, xmlDoc, ref configNode);

                if (configNode != null)
                {
                    objConfiguration = XmlHelper.DeserializeXMLNodeToObject<TCConfiguration>(configNode);
                }
            }
            catch (Exception exc)
            {
                //Debug.Write(exc.ToString());
            }

            return objConfiguration;
        }

        public static IList<TCConfiguration> GetConfigurations(string strProjectGuid)
        {
            IList<TCConfiguration> listConfigs = new List<TCConfiguration>();

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strConfigsPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}/{6}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.TagTCConfigurations, ConfigConstants.TagTCConfiguration);

                var configsList = xmlDoc.SelectNodes(strConfigsPath);
                foreach (XmlNode config in configsList)
                {
                    TCConfiguration tCConfiguration = XmlHelper.DeserializeXMLNodeToObject<TCConfiguration>(config);
                    if (tCConfiguration != null)
                    {
                        listConfigs.Add(tCConfiguration);
                    }
                }
            }
            catch (Exception exc)
            {
                //Debug.Write(exc.ToString());
            }

            return listConfigs;
        }

        public static bool SetConfigActive(string strProjectGuid, string strConfigGuid)
        {
            bool flag = false;
            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strActiveConfigGuidPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.ElementActiveConfigGuid);

                var activeConfigGuidElement = (XmlElement)xmlDoc.SelectSingleNode(strActiveConfigGuidPath);

                if (activeConfigGuidElement != null)
                {
                    activeConfigGuidElement.InnerText = strConfigGuid;
                    flag = ConfigService.SaveConfigFile(xmlDoc);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return flag;
        }

        public static bool SaveConfiguration(string strProjectGuid, TCConfiguration objConfig)
        {
            bool flag = false;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strTCConfigsTagPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.TagTCConfigurations);

                string strConfigPath = String.Format(@"{0}/{1}[@{2}='{3}']", strTCConfigsTagPath, ConfigConstants.TagTCConfiguration, ConfigConstants.AttributeConfigGuid, objConfig.ConfigGuid);

                var tcConfigurationTag = (XmlElement)xmlDoc.SelectSingleNode(strConfigPath);

                if (tcConfigurationTag != null)
                {
                    tcConfigurationTag.InnerXml = string.Empty;
                }
                else
                {
                    var configsTag = (XmlElement)xmlDoc.SelectSingleNode(strTCConfigsTagPath);
                    tcConfigurationTag = xmlDoc.CreateElement(ConfigConstants.TagTCConfiguration);
                    tcConfigurationTag.SetAttribute(ConfigConstants.AttributeConfigGuid, objConfig.ConfigGuid);
                    configsTag.AppendChild(tcConfigurationTag);
                }

                tcConfigurationTag.SetAttribute(ConfigConstants.AttributeConfigName, objConfig.ConfigName);

                var tcRootElement = xmlDoc.CreateElement(ConfigConstants.ElementTCRoot);
                tcRootElement.InnerText = objConfig.TCRoot;
                tcConfigurationTag.AppendChild(tcRootElement);

                var tcDataElement = xmlDoc.CreateElement(ConfigConstants.ElementTCData);
                tcDataElement.InnerText = objConfig.TCData;
                tcConfigurationTag.AppendChild(tcDataElement);

                var activeUserGuid = xmlDoc.CreateElement(ConfigConstants.ElementActiveUserGuid);
                activeUserGuid.InnerText = objConfig.LMIActiveUserGuid;
                tcConfigurationTag.AppendChild(activeUserGuid);

                flag = ConfigService.SaveConfigFile(xmlDoc);
            }
            catch (Exception exc)
            {
                //Debug.Write(exc.ToString());
            }

            return flag;
        }

        public static bool DeleteConfig(string strProjectGuid, string strConfigGuid)
        {
            bool flag = false;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strTCConfigsTagPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.TagTCConfigurations);
                var configsNode = xmlDoc.SelectSingleNode(strTCConfigsTagPath);

                //find existing
                XmlNode configNode = null;
                GetConfigNodeByGuid(strConfigGuid, strProjectGuid, xmlDoc, ref configNode);

                //found
                if (configNode != null)
                {
                    configsNode.RemoveChild(configNode);
                }

                flag = ConfigService.SaveConfigFile(xmlDoc);
            }
            catch (Exception exc)
            {
                //Debug.Write(exc.ToString());
            }

            return flag;
        }

        #endregion

        #region common settings

        public static bool UpdateConfigFilePath(string strConfigFilePath)
        {
            bool flag = false;
            try
            {
                string strDefaultConfigFilePath = LMIConfigHelper.GetDefaultConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strDefaultConfigFilePath);

                string strPath = String.Format(@"//{0}/{1}", ConfigConstants.TagConfigurationRoot, ConfigConstants.ElementConfigFileLoc);
                var repo = xmlDoc.SelectSingleNode(strPath);
                if (repo != null)
                {
                    repo.InnerText = strConfigFilePath;
                }

                flag = ConfigService.SaveConfigFile(xmlDoc, refresh: false, strConfigFilePath: strDefaultConfigFilePath);
            }
            catch (Exception)
            {

                throw;
            }
            return flag;
        }

        public static LMICMSRoot GetLMICMSRoot()
        {
            LMICMSRoot objRoot = null;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                var root = xmlDoc.SelectSingleNode(@"//" + ConfigConstants.TagConfigurationRoot);

                if (root != null)
                {
                    objRoot = new LMICMSRoot();
                    foreach (XmlNode node in root.ChildNodes)
                    {
                        switch (node.Name)
                        {
                            case ConfigConstants.ElementCMSExeLoc:
                                objRoot.LMICMSExeLoc = node.InnerText;
                                break;
                            case ConfigConstants.ElementTempDownloadPath:
                                objRoot.LMITempDownloadPath = node.InnerText;
                                break;
                            case ConfigConstants.ElementConfigFileLoc:
                                objRoot.LMIConfigFileLoc = node.InnerText;
                                break;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                //Debug.Write(exc.ToString());
            }

            return objRoot;
        }

        public static bool SaveCommonSettings(LMICMSRoot objRoot)
        {
            bool flag = false;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strRootPath = String.Format(@"//{0}", ConfigConstants.TagConfigurationRoot);

                string strTempDownPath = String.Format(@"//{0}/{1}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.ElementTempDownloadPath);
                string strCMSExePath = String.Format(@"//{0}/{1}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.ElementCMSExeLoc);
                var tempDownPathNode = xmlDoc.SelectSingleNode(strTempDownPath);
                var cmsExePathNode = xmlDoc.SelectSingleNode(strCMSExePath);

                var rootNode = xmlDoc.SelectSingleNode(strRootPath);
                if (rootNode != null)
                {
                    if (tempDownPathNode != null)
                    {
                        tempDownPathNode.InnerText = objRoot.LMITempDownloadPath;
                    }
                    if (cmsExePathNode != null)
                    {
                        cmsExePathNode.InnerText = objRoot.LMICMSExeLoc;
                    }

                    flag = ConfigService.SaveConfigFile(xmlDoc);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return flag;
        }

        #endregion

        #region repo

        public static void GetRepoNodeByGuid(string strRepoGuid, string strProjectGuid, XmlDocument xmlDoc, ref XmlNode repoNode)
        {
            try
            {
                string strRepoPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}/{6}[@{7}='{8}']", ConfigConstants.TagConfigurationRoot,
                     ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                     strProjectGuid, ConfigConstants.TagSrcControlLoc, ConfigConstants.ElementRepo, ConfigConstants.AttributeRepoGuid, strRepoGuid);

                //find existing
                repoNode = xmlDoc.SelectSingleNode(strRepoPath);

            }
            catch (Exception exc)
            {
              
            }
        }

        public static Repo GetRepo(string strProjectGuid, string strRepoGuid)
        {
            Repo objRepo = null;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                XmlNode repoNode = null;
                GetRepoNodeByGuid(strRepoGuid, strProjectGuid, xmlDoc, ref repoNode);

                if (repoNode != null)
                {
                    objRepo = XmlHelper.DeserializeXMLNodeToObject<Repo>(repoNode);
                }
            }
            catch (Exception exc)
            {
            
            }

            return objRepo;
        }

        public static IList<Repo> GetRepos(string strProjectGuid)
        {
            IList<Repo> listRepos = new List<Repo>();

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strReposPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}/{6}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.TagSrcControlLoc, ConfigConstants.ElementRepo);

                var reposList = xmlDoc.SelectNodes(strReposPath);
                foreach (XmlNode config in reposList)
                {
                    Repo objRepo = XmlHelper.DeserializeXMLNodeToObject<Repo>(config);
                    if (objRepo != null)
                    {
                        listRepos.Add(objRepo);
                    }
                }
            }
            catch (Exception exc)
            {
             
            }

            return listRepos;
        }

        public static bool SaveRepo(String strProjectGuid, Repo repo)
        {
            bool flag = false;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strReposTagPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.TagSrcControlLoc);

                string strRepoPath = String.Format(@"{0}/{1}[@{2}='{3}']", strReposTagPath, ConfigConstants.ElementRepo, ConfigConstants.AttributeRepoGuid, repo.RepoGuid);

                var repoTag = (XmlElement)xmlDoc.SelectSingleNode(strRepoPath);

                if (repoTag != null)
                {
                    repoTag.InnerXml = string.Empty;
                }
                else
                {
                    var reposTag = (XmlElement)xmlDoc.SelectSingleNode(strReposTagPath);
                    repoTag = xmlDoc.CreateElement(ConfigConstants.ElementRepo);
                    repoTag.SetAttribute(ConfigConstants.AttributeRepoGuid, repo.RepoGuid);
                    reposTag.AppendChild(repoTag);
                }

                repoTag.SetAttribute(ConfigConstants.AttributeRepoName, repo.RepoName);
                repoTag.SetAttribute(ConfigConstants.AttributeRepoPath, repo.RepoPath);

                foreach (var appDataLoc in repo.AppDataLocs)
                {
                    var appDataEle = xmlDoc.CreateElement(ConfigConstants.ElementAppData);
                    appDataEle.SetAttribute(ConfigConstants.AttributeAppDataName, appDataLoc.AppDataName);
                    appDataEle.SetAttribute(ConfigConstants.AttributeAppDataPath, appDataLoc.AppDataPath);
                    repoTag.AppendChild(appDataEle);
                }

                flag = ConfigService.SaveConfigFile(xmlDoc);
            }
            catch (Exception exc)
            {
               
            }

            return flag;
        }

        public static bool DeleteRepo(string strProjectGuid, string strRepoGuid)
        {
            bool flag = false;

            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strReposPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.TagSrcControlLoc);
                var reposElement = (XmlElement)xmlDoc.SelectSingleNode(strReposPath);

                //find existing
                XmlNode repoNode = null;
                GetRepoNodeByGuid(strRepoGuid,strProjectGuid, xmlDoc, ref repoNode);

                //found
                if (repoNode != null)
                {
                    reposElement.RemoveChild(repoNode);
                }

                flag = ConfigService.SaveConfigFile(xmlDoc);
            }
            catch (Exception exc)
            {
               
            }

            return flag;
        }

        public static bool SetRepoActive(string strProjectGuid, string strRepoGuid)
        {
            bool flag = false;
            try
            {
                string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strConfigFilePath);

                string strActiveRepoGuidPath = String.Format(@"//{0}/{1}/{2}[@{3}='{4}']/{5}", ConfigConstants.TagConfigurationRoot,
                    ConfigConstants.TagProjectsDetails, ConfigConstants.TagProject, ConfigConstants.AttributeProjectGuid,
                    strProjectGuid, ConfigConstants.ElementActiveRepoGuid);

                var activeRepoGuidElement = (XmlElement)xmlDoc.SelectSingleNode(strActiveRepoGuidPath);

                if (activeRepoGuidElement != null)
                {
                    activeRepoGuidElement.InnerText = strRepoGuid;
                    flag = ConfigService.SaveConfigFile(xmlDoc);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return flag;
        }

        #endregion
    }
}
