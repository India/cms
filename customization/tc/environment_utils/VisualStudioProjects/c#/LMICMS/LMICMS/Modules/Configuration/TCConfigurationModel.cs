﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: TCConfigurationModel.cs

Description: This file contains operations related to TCConfigurationModel

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/

namespace LMICMS.Modules.Configuration
{
    public class TCConfigurationModel
    {
        public string ConfigGuid { get; set; }
        public string ConfigName { get; set; }
        public string TCRoot { get; set; }
        public string TCData { get; set; }
        public string LMIActiveUserGuid { get; set; }
        public string ActiveUser { get; set; }
        public bool Active { get; set; }
    }
}
