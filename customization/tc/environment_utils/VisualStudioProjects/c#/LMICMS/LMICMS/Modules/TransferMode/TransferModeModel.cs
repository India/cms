﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: TransferModeModel.cs

Description: This file contains operations related to TransferMode Names.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;

namespace LMICMS.Modules.Configuration
{
   public class TransferModeModel
    {
        public Boolean bIsActive { get; set; } = false;
        public string TransferModeName { get; set; }

        public string FileName { get; set; }

        public string UID { get; set; }
    }
}
