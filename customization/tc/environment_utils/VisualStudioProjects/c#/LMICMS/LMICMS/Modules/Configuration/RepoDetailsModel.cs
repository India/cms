﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: RepoDetailsModel.cs

Description: This file contains operations related to RepoDetailsModel

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
namespace LMICMS.Modules.Configuration
{
    public class CreateOrUpdateRepo
    {
        public bool Active { get; set; }
        public string RepoGuid { get; set; }
        public string RepoName { get; set; }
        public string RepoPath { get; set; }
        public string WorkflowPath { get; set; }
        public string AuthorizationPath { get; set; }
        public string OrganizationPath { get; set; }
        public string ACLPath { get; set; }
        public string PFFPath { get; set; }
        public string StylesheetPath { get; set; }
        public string PreferencePath { get; set; }
        public string QueryPath { get; set; }
        public string TransferModePath { get; set; }
    }
}
