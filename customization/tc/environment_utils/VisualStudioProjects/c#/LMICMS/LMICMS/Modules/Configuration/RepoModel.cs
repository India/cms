﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: RepoModel.cs

Description: This file contains operations related to RepoModel

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/

namespace LMICMS.Modules.Configuration
{
    public class RepoModel
    {
        public RepoModel()
        {
            
        }
        public string RepoGuid { get; set; }
        public string RepoName { get; set; }
        public string RepoPath { get; set; }
        public bool Active { get; set; }
        
        public string AppDataLocs { get; set; }
    }
}
