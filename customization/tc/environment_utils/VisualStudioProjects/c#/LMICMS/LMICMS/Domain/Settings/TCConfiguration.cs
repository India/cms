﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LMICMS.Domain.Settings
{
    [XmlRoot(ElementName = "TC_CONFIGURATIONS")]
    public class TCConfigurations
    {
        [XmlElement(ElementName = "TC_CONFIGURATION")]
        public List<TCConfiguration> TCConfigs { get; set; }
    }

    [XmlRoot(ElementName = "TC_CONFIGURATION")]
    public class TCConfiguration
    {
        [XmlAttribute(AttributeName = "CONFIG_GUID")]
        public string ConfigGuid { get; set; }
        [XmlAttribute(AttributeName = "CONFIG_NAME")]
        public string ConfigName { get; set; }
        [XmlElement(ElementName = "TC_ROOT")]
        public string TCRoot { get; set; }
        [XmlElement(ElementName = "TC_DATA")]
        public string TCData { get; set; }
        [XmlElement(ElementName = "LMI_ACTIVE_USER_GUID")]
        public string LMIActiveUserGuid { get; set; }
    }
}
