﻿using System.Xml.Serialization;

namespace LMICMS.Domain.Settings
{
    [XmlRoot(ElementName = "LMI_CMS")]
    public class LMICMSRoot
    {
        [XmlElement(ElementName = "ACTIVE_PROJECT_GUID")]
        public string ActiveProjectGuid { get; set; }
        [XmlElement(ElementName = "LMI_TEMP_DOWNLOAD_PATH")]
        public string LMITempDownloadPath { get; set; }
        [XmlElement(ElementName = "LMI_CMS_EXE_LOC")]
        public string LMICMSExeLoc { get; set; }
        [XmlElement(ElementName = "LMI_CONFIG_FILE_LOC")]
        public string LMIConfigFileLoc { get; set; }
        [XmlElement(ElementName = "PROJECTS_DETAILS")]
        public ProjectsDetails ProjectsDetails { get; set; }
    }
}
