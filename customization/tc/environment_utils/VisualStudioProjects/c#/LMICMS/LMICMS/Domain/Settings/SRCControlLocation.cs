﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LMICMS.Domain.Settings
{
    [XmlRoot(ElementName = "SRC_CONTROL_LOC")]
    public class SRCControlLocation
    {
        [XmlElement(ElementName = "REPO")]
        public List<Repo> RepoPaths { get; set; }
    }

    [XmlRoot(ElementName = "REPO")]
    public class Repo
    {
        public Repo()
        {
            AppDataLocs = new List<AppData>();
        }
        [XmlAttribute(AttributeName = "REPO_GUID")]
        public string RepoGuid { get; set; }
        [XmlAttribute(AttributeName = "REPO_NAME")]
        public string RepoName { get; set; }
        [XmlAttribute(AttributeName = "REPO_PATH")]
        public string RepoPath { get; set; }
        [XmlElement(ElementName = "APP_DATA")]
        public List<AppData> AppDataLocs { get; set; }

    }

    [XmlRoot(ElementName = "APP_DATA")]
    public class AppData
    {
        [XmlAttribute(AttributeName = "APP_DATA_NAME")]
        public string AppDataName { get; set; }
        [XmlAttribute(AttributeName = "APP_DATA_PATH")]
        public string AppDataPath { get; set; }
    }
}
