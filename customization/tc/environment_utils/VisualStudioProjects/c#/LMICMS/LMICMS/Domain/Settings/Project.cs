﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LMICMS.Domain.Settings
{
    [XmlRoot(ElementName = "PROJECTS_DETAILS")]
    public class ProjectsDetails
    {
        [XmlElement(ElementName = "PROJECT")]
        public List<Project> Projects { get; set; }
    }

    [XmlRoot(ElementName = "PROJECT")]
    public class Project
    {
        [XmlAttribute(AttributeName = "PROJECT_GUID")]
        public string ProjectGuid { get; set; }
        [XmlAttribute(AttributeName = "PROJECT_NAME")]
        public string ProjectName { get; set; }
        [XmlElement(ElementName = "ACTIVE_CONFIG_GUID")]
        public string ActiveConfigGuid { get; set; }
        [XmlElement(ElementName = "ACTIVE_REPO_GUID")]
        public string ActiveRepoGuid { get; set; }
        [XmlElement(ElementName = "TC_CONFIGURATIONS")]
        public TCConfigurations TCConfigurations { get; set; }
        [XmlElement(ElementName = "TC_USERS_DETAILS")]
        public TCUsersDetails TCUsersDetails { get; set; }
        [XmlElement(ElementName = "SRC_CONTROL_LOC")]
        public SRCControlLocation SRCControlLocation { get; set; }
    }
}
