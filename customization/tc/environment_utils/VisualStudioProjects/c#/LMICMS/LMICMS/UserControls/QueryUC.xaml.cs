﻿/*======================================================================================================================================================================
                Copyright 2020  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: QueryUC.xaml.cs

Description: This file is the entry point for QueryUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    public partial class QueryUC : UserControl
    {
        private Logger logger = Logger.getInstance();
        public QueryUC()
        {
            InitializeComponent();
        }

        /* This Function gets invoked when user clicks on Export/Import tab */
        private void TabQuerySelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                try
                {
                    string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                    switch (tabItem)
                    {
                        case Constants.Export:
                            ccExport.Content = new QueryExportUC();
                            break;
                        case Constants.Import:
                            ccImport.Content = new QueryImportUC();
                            break;
                    }
                }
                catch(Exception ex)
                {
                    logger.error(ex.Message);
                }
            }
        }
    }
}

    

