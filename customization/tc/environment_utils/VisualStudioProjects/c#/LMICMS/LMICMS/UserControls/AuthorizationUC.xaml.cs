﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Authorization.xaml.cs

Description: This file contains implementation for AuthorizationUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;

namespace LMICMS.UserControls
{
    public partial class AuthorizationUC : UserControl
    {
        public AuthorizationUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Export_Location.Text = ConfigConstants.AuthorizationPath;
            tb_Import_Location.Text = ConfigConstants.AuthorizationPath;
        }

        private void Exp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Imp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Import_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
