﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: OrganizationExportImportUC.xaml.cs

Description: This file contains implementation for OrganizationExportImportUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Dialogs;
using LMICMS.Services;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    public partial class OrganizationExportImportUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage = string.Empty;

        private Logger logger = Logger.getInstance();

        public OrganizationExportImportUC()
        {
            InitializeComponent();
        }

        /* This function is the call back function which gets invoked when StylesheetExportUC is loaded */
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            /*Assigning Organization Path in Export Text box Loaction */
            tb_Export_Location.Text = ConfigConstants.OrganizationPath;

            /*Assigning Organization Path in  Import Text box Loaction */
            tb_Import_Location.Text = ConfigConstants.OrganizationPath;
        }

        /* This function gets invoked when user clicks on BrowseFolder Button which  is used to Browse for requisite  Folder Location */
        private void Exp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                    /* Setting the trimmed text box location text in dialog path */
                    dialog.SelectedPath = tb_Export_Location.Text.Trim();

                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        /* set the selected location in text box */
                        tb_Export_Location.Text = dialog.SelectedPath;
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user clicks on BrowseFiles Button which  is used to Browse and select  for requisite  File */
        private void Imp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Import_Location.Text))
                {
                    /* creating object of Open FileDialog Class */
                    OpenFileDialog openFileDialog = new OpenFileDialog();

                    /* Enabling multiple file selection */
                    openFileDialog.Multiselect = true;

                    /* Setting Import text box location as initial directory to search */
                    openFileDialog.InitialDirectory = tb_Import_Location.Text;

                    /* Applying File type filter */
                    openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                    DirectoryInfo dir = new DirectoryInfo(tb_Import_Location.Text);

                    if (openFileDialog.ShowDialog() == true)

                        foreach (string filename in openFileDialog.FileNames)
                        {
                            String strFileName = System.IO.Path.GetFileName(filename);

                            /* setting the directory path along with file name selected */
                            string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                            /* set the selected file in text box */
                            tb_Import_Location.Text = openFileDialog.FileName;
                        }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(Constants.ERR_INVALIDPATH);
            }
        }

        /*This function gets invoked when user clicks on Export Button to export set of Organization to a set location */
        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strOutMsg = string.Empty;

                /* Creating object of class Progressbar and passing organization name, export request and export location to it */
                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Export_Location.Text, Constants.REQ_ORGANISATIONEXPORT, tb_organization_name.Text, 0);

                /* Displaying the progress bar */
                progressBar.ShowDialog();

                /* Assigning the final message output of the export operation to string Disp */
                string strDisp = OrganisationExportService.strOutput;

                if (strDisp.Contains(Constants.ERROR) == false)
                {
                    strHeaderMessage = Constants.HEADER_INFO;
                    strOutMsg = Constants.OPERATION_COMPLETED;
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_INFO;
                    strOutMsg = Constants.OPERATION_FAILED;
                }

                /* Creating an object of Diplay class and passing values to it */
                Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                if (display.ShowDialog() == true)
                {
                    /*Display the Message dialog box */
                    display.ShowDialog();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(Constants.ERR_INVALIDPATH);
            }
        }

        /*This function gets invoked when user clicks on Import Button to import  Organization to a set location */
        private void Import_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strOutFileName = string.Empty;
                string strOutMsg = string.Empty;
                FileNameUtils objFnameUtil = new FileNameUtils();

                if (Path.HasExtension(tb_Import_Location.Text) == true)
                {
                    /* if checkbox is selected then file needs to be cleaned */
                    if (CB.Content.Equals(Constants.CHECKED))
                    {
                        /* Construct filename for file to be cleaned */
                        strOutFileName = objFnameUtil.constructGenericFileNames(Constants.OUT_FILE_NAME, Constants.FEX_XML);
                    }

                    /* Creating object of class Progressbar and passing importLocation, import request and filename to it */
                    LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Import_Location.Text, Constants.REQ_ORGANISATIONIMPORT, strOutFileName, 0);

                    /* Displaying the progress bar */
                    progressBar.ShowDialog();

                    /* Assigning the final message output of the export operation to string Disp */
                    string strDisp = OrganisationImportService.strOutput;

                    if (strDisp.Contains(Constants.ERROR) == false)
                    {
                        strHeaderMessage = Constants.HEADER_INFO;
                        strOutMsg = Constants.OPERATION_COMPLETED;
                    }
                    else
                    {
                        strHeaderMessage = Constants.HEADER_INFO;
                        strOutMsg = Constants.OPERATION_FAILED;
                    }

                    /* Creating an object of Diplay class and passing values to it */
                    Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                    if (display.ShowDialog() == true)
                    {
                        /*Display the Message dialog box */
                        display.ShowDialog();
                    }
                }
                else
                {

                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_FILENAMEEMPTY;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENAMEEMPTY);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(Constants.ERR_INVALIDPATH);
            }
        }

        /* This function gets invoked when user selects the  Is Clean Required Checkbox */
        private void CBCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            /* Fill the Content Property of checkbox with value "Checked" */
            CB.Content = Constants.CHECKED;
        }

        /* This function gets invoked when user does not selects the Is clean Required Checkbox */ 
        private void CBCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            /* Fill the Content Property of checkbox with value "UnChecked" */
            CB.Content = Constants.UNCHECKED;
        }
    }
}
