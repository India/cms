﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: PffExportUC.xaml.cs

Description: This file contains implementation for PffExportUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.Modules.Pff;
using LMICMS.ReqResp;
using LMICMS.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for PFFExportUC.xaml
    /// </summary>
    public partial class PFFExportUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage = string.Empty;

        private Logger logger = Logger.getInstance();

        List<PffModel> lPffNames = null;

        List<PffModel> lSearchPffName = null;

        public PFFExportUC()
        {
            InitializeComponent();
        }

        /* This function is the call back function which gets invoked when PFF UserControl is loaded */
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            /*Assigning Pff Path in Text box Loaction */
            tb_Export_Location.Text = ConfigConstants.PFFPath;
        }

        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                /* Assigning the value of textfilter text box to string */
                string strSearchPattern = txtFilter.Text;

                if (strSearchPattern.Length == 0)
                {
                    /* Assigning asterisk to string */
                    strSearchPattern = Constants.ASTERISK;
                }

                /* Creating object of List<StylesheetModel> class */
                lSearchPffName = new List<PffModel>();

                /*Using Regex.Escape to frame Search pattern to execute live search */
                strSearchPattern = Constants.SEP_CARET + Regex.Escape(strSearchPattern).Replace(Constants.SEP_ASTERISK, Constants.SEP_DOT_ASTERISK) + Constants.SEP_DOLLAR;

                foreach (var item in lPffNames)
                {
                    /* Matching the Transfermodename with Search Pattern entered in Search Transfermodename text box */
                    if (Regex.IsMatch(item.PffName, strSearchPattern, RegexOptions.IgnoreCase))
                    {
                        /* When match is found then add Stylesheetname and filename to list */
                        lSearchPffName.Add(new PffModel() { PffName = item.PffName, FileName = item.FileName, UID = item.UID });
                    }
                }

                /*Filling the datagrid with SearchStylesheetnames*/
                dgPFFName.ItemsSource = lSearchPffName;

                /*Refreshing the items contained in datagrid */
                dgPFFName.Items.Refresh();
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void SelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgPFFName.ItemsSource == lPffNames)
                {
                    foreach (var item in lPffNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgPFFName.Items.Refresh();
                }
                else if (dgPFFName.ItemsSource == lSearchPffName)
                {
                    foreach (var item in lSearchPffName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgPFFName.Items.Refresh();
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void UnSelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgPFFName.ItemsSource == lPffNames)
                {
                    foreach (var item in lPffNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgPFFName.Items.Refresh();
                }
                else if (dgPFFName.ItemsSource == lSearchPffName)
                {
                    foreach (var item in lSearchPffName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgPFFName.Items.Refresh();
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user clicks on Refresh button to Fetch all PffNames from Teamcenter */
        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strText = string.Empty;
                dgPFFName.ItemsSource = null;
                FileHelper fileHelper = new FileHelper();
                Boolean bIsFileAvailable = false;
                lPffNames = new List<PffModel>();

                /* Refreshes the datagrid items */
                dgPFFName.Items.Refresh();

                /* Accessing the location of Temp Folder */
                string strTempDir = ConfigService.strTempFolder;

                /* Constructing the Filename for Pffnames */
                string strFileName = (Constants.PFF_NAME + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                /* Starting the Progress bar considering the lengthy operation */
                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(strFileName, Constants.REQ_PFFNAME, 0, tb_UserReq_Location.Text);

                /* Shows the progressbar dialog box */
                progressBar.ShowDialog();

                /* Checking whether PffName File is available or not */
                bIsFileAvailable = fileHelper.Check_File_Exists(strTempDir + Constants.FILE_PATH + strFileName);

                if (bIsFileAvailable == true)
                {
                    /* Read the whole TransferMode Name file and assign it to string Text */
                    using (var streamReader = new StreamReader(strTempDir + Constants.FILE_PATH + strFileName, Encoding.UTF8))
                    {
                        strText = streamReader.ReadToEnd();
                    }

                    /* Tokenise the string Text with New Line and store it in array */
                    string[] parts = strText.Split(new[] { Constants.NEW_LINE_ONE, Constants.NEW_LINE_TWO }, StringSplitOptions.RemoveEmptyEntries);

                    int iDx = 0;
                    int jDx = 0;

                    /* Loop through parts array as they contain Transfermode names and Uid's but  UID's are not to be read and placed in data grid */
                    for (iDx = 0, jDx = 1; iDx < parts.Length && jDx < parts.Length; iDx += 2, jDx += 2)
                    {
                        {
                            string strValueOne = parts[iDx];
                            string strValueTwo = parts[jDx];

                            /* Add strValue1 which contains Transfermodenames to Transfermode name and File name of TransfermodeModel and strValue2 to UID of TransfermodeModel */
                            lPffNames.Add(new PffModel() { PffName = strValueOne, FileName = strValueOne, UID = strValueTwo });
                        }

                        /*Filling the datagrid with Pffnames*/
                        dgPFFName.ItemsSource = lPffNames;

                        /*Refreshing the items contained in datagrid */
                        dgPFFName.Items.Refresh();
                    }
                }

                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_FILENOTEXISTS;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning dialog Box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENOTEXISTS);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(Constants.ERR_FILENOTEXISTS);
            }
        }

        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strOutMsg = string.Empty;
                string strFileName = string.Empty;
                string strPffName = string.Empty;
                string strUID = string.Empty;
                PffExportModel pffExportModel = new PffExportModel();

                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    if (dgPFFName.ItemsSource == lPffNames)
                    {
                        foreach (var item in lPffNames)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row Stylesheetname item to string Stylesheetname seperated by a comma */
                                strPffName = strPffName + ((PffModel)item).PffName + Constants.SEP_COMMA;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((PffModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((PffModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a comma */
                                strFileName = strFileName + ((PffModel)item).FileName + Constants.SEP_COMMA;

                                /* Add  selected row correponding UID to string UID seperated by a comma  */
                                strUID = strUID + ((PffModel)item).UID + Constants.SEP_COMMA;
                            }
                        }
                    }
                    else if (dgPFFName.ItemsSource == lSearchPffName)
                    {
                        foreach (var item in lSearchPffName)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row Stylesheetname item to string Stylesheetname seperated by a comma */
                                strPffName = strPffName + ((PffModel)item).PffName + Constants.SEP_COMMA;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((PffModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((PffModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a comma */
                                strFileName = strFileName + ((PffModel)item).FileName + Constants.SEP_COMMA;

                                /* Add  selected row correponding UID to string UID seperated by a comma  */
                                strUID = strUID + ((PffModel)item).UID + Constants.SEP_COMMA;
                            }

                        }
                    }
                    /*    To add Stylesheetnames as string in PfftExportModel */
                    pffExportModel.pff = strPffName;

                    /* To add FileName as string in PffExportModel */
                    pffExportModel.exportedpff = strFileName;

                    /* To add UIDs as string in Pffexportmodel */
                    pffExportModel.pffUID = strUID;

                    /* Remove Comma from end of the string UID */
                    strUID = strUID.TrimEnd(Constants.COMMA_SEP);


                    if (pffExportModel.exportedpff != null && pffExportModel.exportedpff.Length > 0)
                    {
                        /* Creating object of class Progressbar and passing stylesheetexportmodel, export request and export location to it */
                        LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(pffExportModel, Constants.REQ_PFFEXPORT, tb_Export_Location.Text, 0);

                        /* Displaying the progress bar */
                        progressBar.ShowDialog();

                        UnSelectAll_Button_Click(sender, e);

                        /* Assigning the final message output of the export operation to string Disp */
                        string strDisp = PffExportService.strAppendedOutput;

                        if (strDisp.Contains(Constants.ERROR) == false)
                        {
                            strHeaderMessage = Constants.HEADER_INFO;
                            strOutMsg = Constants.OPERATION_COMPLETED;
                        }
                        else
                        {
                            strHeaderMessage = Constants.HEADER_INFO;
                            strOutMsg = Constants.OPERATION_FAILED;
                        }

                        /* Creating an object of Diplay class and passing values to it */
                        Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                        if (display.ShowDialog() == true)
                        {
                            /*Display the Message dialog box */
                            display.ShowDialog();
                        }
                    }
                    else
                    {
                        strHeaderMessage = Constants.HEADER_WARNING;

                        strOutputMessage = Constants.ERR_PFFNAMESELECTED;

                        /* Creating object of Warning Display class and passing values to it */
                        WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                        /* Display the Warning Dialog box */
                        display.ShowDialog();

                        /* Calling object logger and passing exception message to write it in Error log file */
                        logger.error(Constants.ERR_PFFNAMEEMPTY);
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /* Creating object of Warning Display class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Display the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    
        /* This function gets invoked when user clicks on BrowseFolder Button which  is used to Browse for requisite  Folder Location */
        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                    /* Setting the trimmed text box location text in dialog path */
                    dialog.SelectedPath = tb_Export_Location.Text.Trim();

                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        /* set the selected location in text box */
                        tb_Export_Location.Text = dialog.SelectedPath;
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }

        }

        /* This function gets invoked when user moves up and down in datagrid */
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /* It indicates that the event handler has already processed the event */
            e.Handled = true;
        }
    }
}
