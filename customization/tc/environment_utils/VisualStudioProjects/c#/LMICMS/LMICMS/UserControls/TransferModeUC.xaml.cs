﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: TransferModeUC.xaml.cs

Description: This file is the entry point for TransferModeUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    public partial class TransferModeUC : UserControl
    {
        private Logger logger = Logger.getInstance();
        public TransferModeUC()
        {
            InitializeComponent();
        }

        /* This Function gets invoked when user clicks on Export/Import tab */
        private void TabTransferModeSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                try
                {
                    string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                    switch (tabItem)
                    {
                        case Constants.Export:
                            ccExport.Content = new TMExportUC();
                            break;
                        case Constants.Import:
                            ccImport.Content = new TMImportUC();
                            break;
                    }
                }
                catch(Exception ex)
                {
                    logger.error(ex.Message);
                }    
            }
        }
    }
}
