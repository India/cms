﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Configuration.xaml.cs

Description: This file contains implementation for ConfigurationUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Dialogs;
using LMICMS.Factories;
using LMICMS.Modules.Configuration;

namespace LMICMS.UserControls
{
    public partial class ConfigurationUC : UserControl
    {
        public ConfigurationUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            GridViewRefresh();
        }

        private void GridViewRefresh()
        {
            dgConfiguration.ItemsSource = SettingsModelFactory.GetConfigurationModels(ConfigConstants.ActiveProjectGuid);
            dgConfiguration.Items.Refresh();
        }

        private void BtnAddClick(object sender, RoutedEventArgs e)
        {
            CreateOrUpdateConfiguration createOrUpdateDialog = new CreateOrUpdateConfiguration();
            if (createOrUpdateDialog.ShowDialog() == true)
            {
                GridViewRefresh();
            }
            e.Handled = true;
        }

        private void BtnEditClick(object sender, RoutedEventArgs e)
        {
            Button btnEdit = (Button)e.OriginalSource;

            CreateOrUpdateConfiguration createOrUpdateDialog = new CreateOrUpdateConfiguration(Convert.ToString(btnEdit.CommandParameter));
            if (createOrUpdateDialog.ShowDialog() == true)
            {
                GridViewRefresh();
            }

            e.Handled = true;
        }

        private void BtnDeleteClick(object sender, RoutedEventArgs e)
        {
            Button btnDelete = (Button)e.OriginalSource;
            String strConfigGuid = Convert.ToString(btnDelete.CommandParameter);

            if (!String.IsNullOrEmpty(strConfigGuid))
            {
                bool deleted = SettingsModule.DeleteConfig(ConfigConstants.ActiveProjectGuid, strConfigGuid);
                if (deleted == true)
                {
                    GridViewRefresh();
                }
            }
            e.Handled = true;
        }

        private void BtnSetActiveClick(object sender, RoutedEventArgs e)
        {
            Button btnSetProActive = (Button)e.OriginalSource;
            String strConfigGuid = Convert.ToString(btnSetProActive.CommandParameter);

            if (!String.IsNullOrEmpty(strConfigGuid))
            {
                bool active = SettingsModule.SetConfigActive(ConfigConstants.ActiveProjectGuid, strConfigGuid);
                if (active == true)
                {
                    GridViewRefresh();
                }
            }
            e.Handled = true;
        }

        private void DataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
