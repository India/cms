﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: TMExportUC.xaml.cs

Description: This file contains implementation for TransferModeExportUC Operation.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Modules.Configuration;
using LMICMS.ReqResp;
using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.Services;

namespace LMICMS.UserControls
{
    public partial class TMExportUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage = string.Empty;

        List<TransferModeModel> lTransferModeNames = null;

        List<TransferModeModel> lSearchTransferModeName = null;

        private Logger logger = Logger.getInstance();
        public TMExportUC()
        {
            InitializeComponent();
        }

        /* This function is the call back function which gets invoked when TMExportUC is loaded */
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            /*Assigning Workflow Path in Text box Loaction */
            tb_Export_Location.Text = ConfigConstants.TransferModePath;
        }

        /* This function gets invoked when user clicks on BrowseFolder Button which  is used to Browse for requisite  Folder Location */
        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                    /* Setting the trimmed text box location text in dialog path */
                    dialog.SelectedPath = tb_Export_Location.Text.Trim();

                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        /* set the selected location in text box */
                        tb_Export_Location.Text = dialog.SelectedPath;
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning dialog Box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user clicks on Refresh button to Fetch all TransferModeNames from Teamcenter */
        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strText = string.Empty;
                FileHelper fileHelper = new FileHelper();
                Boolean bIsFileAvailable = false;
                lTransferModeNames = new List<TransferModeModel>();
                dgTransferModeName.ItemsSource = null;

                /* Refreshes the datagrid items */
                dgTransferModeName.Items.Refresh();

                /* Accessing the location of Temp Folder */
                string strTempDir = ConfigService.strTempFolder;

                /* Constructing the Filename for TransferModenames */
                string strFileName = (Constants.TRANSFER_MODE_NAME + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                /* Starting the Progress bar considering the lengthy operation */
                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(strFileName, Constants.REQ_TRANSFERMODENAME, 0, 0);

                /* Shows the progressbar dialog box */
                progressBar.ShowDialog();

                /* Checking whether TransferModeName File is available or not */
                bIsFileAvailable = fileHelper.Check_File_Exists(strTempDir + Constants.FILE_PATH + strFileName);

                if (bIsFileAvailable == true)
                {
                    /* Read the whole TransferMode Name file and assign it to string Text */
                    using (var streamReader = new StreamReader(strTempDir + Constants.FILE_PATH + strFileName, Encoding.UTF8))
                    {
                        strText = streamReader.ReadToEnd();
                    }

                    /* Tokenise the string Text with New Line and store it in array */
                    string[] parts = strText.Split(new[] { Constants.NEW_LINE_ONE, Constants.NEW_LINE_TWO }, StringSplitOptions.RemoveEmptyEntries);

                    int iDx = 0;
                    int jDx = 0;

                    /* Loop through parts array as they contain Transfermode names and Uid's but  UID's are not to be read and placed in data grid */
                    for (iDx = 0, jDx = 1; iDx < parts.Length && jDx < parts.Length; iDx += 2, jDx += 2)
                    {
                        {
                            string strValueOne = parts[iDx];
                            string strValueTwo = parts[jDx];

                            /* Add strValue1 which contains Transfermodenames to Transfermode name and File name of TransfermodeModel and strValue2 to UID of TransfermodeModel */
                            lTransferModeNames.Add(new TransferModeModel() { TransferModeName = strValueOne, FileName = strValueOne, UID = strValueTwo });
                        }

                        /*Filling the datagrid with Transfermodenames*/
                        dgTransferModeName.ItemsSource = lTransferModeNames;

                        /*Refreshing the items contained in datagrid */
                        dgTransferModeName.Items.Refresh();
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_FILENOTEXISTS;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Display the Warning dialog Box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENOTEXISTS);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on SelectAll button which Selects the overall datagrid items */
        private void SelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgTransferModeName.ItemsSource == lTransferModeNames)
                {
                    foreach (var item in lTransferModeNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgTransferModeName.Items.Refresh();
                }
                else if (dgTransferModeName.ItemsSource == lSearchTransferModeName)
                {
                    foreach (var item in lSearchTransferModeName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgTransferModeName.Items.Refresh();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on UnSelectAll button which UnSelects the overall datagrid items */
        private void UnSelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgTransferModeName.ItemsSource == lTransferModeNames)
                {
                    foreach (var item in lTransferModeNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgTransferModeName.Items.Refresh();
                }
                else if (dgTransferModeName.ItemsSource == lSearchTransferModeName)
                {
                    foreach (var item in lSearchTransferModeName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgTransferModeName.Items.Refresh();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user moves up and down in datagrid */
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /* It indicates that the event handler has already processed the event */
            e.Handled = true;
        }

        /*This function gets invoked when user enters text in Search textbox which is Used for performing Live search within datagrid items */
        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                /* Assigning the value of textfilter text box to string */
                string strSearchPattern = txtFilter.Text;

                if (strSearchPattern.Length == 0)
                {
                    /* Assigning asterisk to string */
                    strSearchPattern = Constants.ASTERISK;
                }
                /* Creating object of List<WorkflowModel> class */
                lSearchTransferModeName = new List<TransferModeModel>();

                /*Using Regex.Escape to frame Search pattern to execute live search */
                strSearchPattern = Constants.SEP_CARET + Regex.Escape(strSearchPattern).Replace(Constants.SEP_ASTERISK, Constants.SEP_DOT_ASTERISK) + Constants.SEP_DOLLAR;

                foreach (var item in lTransferModeNames)
                {
                    /* Matching the Transfermodename with Search Pattern entered in Search Transfermodename text box */
                    if (Regex.IsMatch(item.TransferModeName, strSearchPattern, RegexOptions.IgnoreCase))
                    {
                        /* When match is found then add Transfermodename and Filename to list */
                        lSearchTransferModeName.Add(new TransferModeModel() { TransferModeName = item.TransferModeName, FileName = item.FileName, UID = item.UID});
                    }
                }
                /*Filling the datagrid with SearchTransfermodenames*/
                dgTransferModeName.ItemsSource = lSearchTransferModeName;

                /*Refreshing the items contained in datagrid */
                dgTransferModeName.Items.Refresh();
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on Export Button to export set of Transfermodes to a set location */
        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strOutMsg = string.Empty;
                string strFileName = string.Empty;
                string strTransferModeName = string.Empty;
                string strUID = string.Empty;
                TransferModeExportModel transferModeExportModel = new TransferModeExportModel();

                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    if (dgTransferModeName.ItemsSource == lTransferModeNames)
                    {
                        foreach (var item in lTransferModeNames)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row  TransferModeName item to string TransferModeName seperated by a comma */
                                strTransferModeName = strTransferModeName + ((TransferModeModel)item).TransferModeName + Constants.SEP_COMMA;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((TransferModeModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((TransferModeModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a comma  */
                                strFileName = strFileName + ((TransferModeModel)item).FileName + Constants.SEP_COMMA;

                                /* Add  selected row correponding UID to string UID seperated by a comma  */
                                strUID = strUID + ((TransferModeModel)item).UID + Constants.SEP_COMMA;
                            }
                        }
                          
                    }
                    else if (dgTransferModeName.ItemsSource == lSearchTransferModeName)
                    {
                        foreach (var item in lSearchTransferModeName)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row  TransferModeName item to string TransferModeName seperated by a comma */
                                strTransferModeName = strTransferModeName + ((TransferModeModel)item).TransferModeName + Constants.SEP_COMMA;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((TransferModeModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((TransferModeModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a comma  */
                                strFileName = strFileName + ((TransferModeModel)item).FileName + Constants.SEP_COMMA;

                                /* Add  selected row correponding UID to string UID seperated by a comma  */
                                strUID = strUID + ((TransferModeModel)item).UID + Constants.SEP_COMMA;
                            }
                        }
                    }
                    /* Remove Comma from end of the string Filename */
                    strFileName = strFileName.TrimEnd(Constants.COMMA_SEP);

                    /* Remove Comma from end of the string UID */
                    strUID = strUID.TrimEnd(Constants.COMMA_SEP);

                    /* To add Transfermodenames as string in transfermodeexportmodel */
                    transferModeExportModel.transfermode = strTransferModeName;

                    /* To add Filenames as string in transfermodeexportmodel */
                    transferModeExportModel.exportedtransfermode = strFileName;

                    /* To add UIDs as string in transfermodeexportmodel */
                    transferModeExportModel.transfermodeUID = strUID;

                    if (transferModeExportModel.exportedtransfermode != null && transferModeExportModel.exportedtransfermode.Length > 0)
                    {
                        /* Creating object of class Progressbar and passing transferModeExportModel, export request and export location to it */
                        LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(transferModeExportModel, Constants.REQ_TRANSFERMODEEXPORT, tb_Export_Location.Text, 0);

                        /* Displaying the progress bar */
                        progressBar.ShowDialog();

                        UnSelectAll_Button_Click(sender, e);

                        /* Assigning the final message output of the export operation to string Disp */
                        string strDisp = TransferModeExportService.strAppendedOutput;

                        if (strDisp.Contains(Constants.ERROR) == false)
                        {
                            strHeaderMessage = Constants.HEADER_INFO;
                            strOutMsg = Constants.OPERATION_COMPLETED;
                        }
                        else
                        {
                            strHeaderMessage = Constants.HEADER_INFO;
                            strOutMsg = Constants.OPERATION_FAILED;
                        }

                        /* Creating an object of Diplay class and passing values to it */
                        Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                        if (display.ShowDialog() == true)
                        {
                            /* Display the Message Dialog box */
                            display.ShowDialog();
                        }
                    }
                    else
                    {
                        strHeaderMessage = Constants.HEADER_WARNING;

                        strOutputMessage = Constants.ERR_TRANSFMODENAMESELECTED;

                        /* Creating object of Warning Display class and passing values to it */
                        WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                        /* Display the Warning Dialog box */
                        display.ShowDialog();

                        /* Calling object logger and passing exception message to write it in Error log file */
                        logger.error(Constants.ERR_TRANSFMODENAMESELECTED);
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /* Creating object of Warning Display class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Display the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}

       

