﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: ImportUC.xaml.cs

Description: This file contains implementation for QueryImportUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Services;
using LMICMS.Common.Constants;
using LMICMS.ReqResp;
using LMICMS.Dialogs;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    public partial class QueryImportUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage   = string.Empty;

        private Logger logger = Logger.getInstance();


        public QueryImportUC()
        {
            InitializeComponent();
        }

        /* This function is the call back function which gets invoked when QueryImportUC is loaded */
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            /*Assigning Query Path in Text box Location */
            tb_Query_Location.Text = ConfigConstants.QueryPath;
        }

        /* This function gets invoked when user clicks on BrowseFiles Button which  is used to Browse for requisite  File  */
        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Query_Location.Text))
                {
                    /* creating object of Open FileDialog Class */
                    OpenFileDialog openFileDialog = new OpenFileDialog();

                    /* Enabling multiple file selection */
                    openFileDialog.Multiselect = true;

                    /* Setting text box location as initial directory to search */
                    openFileDialog.InitialDirectory = tb_Query_Location.Text;

                    /* Applying File type filter */
                    openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                    DirectoryInfo dir = new DirectoryInfo(tb_Query_Location.Text);

                    if (openFileDialog.ShowDialog() == true)

                        foreach (string filename in openFileDialog.FileNames)
                        {
                            String strFileName = System.IO.Path.GetFileName(filename);

                            /* setting the directory path along with file name selected */
                            string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                            if (!dgQueryName.Items.Contains(strFileName))
                            {
                                dgQueryName.Items.Add(strFileName);
                            }
                            /* set the selected file path in text box */
                            tb_Query_Location.Text = directoryPath;
                        }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user moves up and down in datagrid */
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /* It indicates that the event handler has already processed the event */
            e.Handled = true;
        }

        /* This function gets invoked when user clicks on Delete button which deletes the selected item from data grid row */
        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            for (int counter = dgQueryName.Items.Count - 1; counter >= 0; counter--)
            {
                /* Remove selected item from datagrid */
                dgQueryName.Items.Remove(dgQueryName.SelectedItem);
            }
        }

        /*This function gets invoked when user clicks on Load Files Button which provides all the file names in datagrid present in set Location */
        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Query_Location.Text))
                {
                    /* Get all xml type  file paths from the location set in text box */
                    string[] filePaths = Directory.GetFiles(tb_Query_Location.Text, Constants.FILE_EXT_ALL_XML);

                    foreach (string file in filePaths)
                    {
                        if (dgQueryName.Items.Contains(System.IO.Path.GetFileName(file)))
                        {

                        }
                        else
                        {
                            /* Add files to Datagrid */
                            dgQueryName.Items.Add(System.IO.Path.GetFileName(file));
                        }
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning dialog Box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on Import Button to import Querynames to a set location */
        private void Import_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strOutMsg = string.Empty;
                QueryImportModel queryimportModel = new QueryImportModel();

                if (dgQueryName.Items.Count > 0 )
                {
                    foreach (var item in dgQueryName.Items)
                    {
                        /* Add item as string to StylesheetImportModel */
                        queryimportModel.queries.Add(Convert.ToString(item));
                    }

                    /* Assigning Query Location to Query ImportModel */
                    queryimportModel.strFilePath = tb_Query_Location.Text;

                    /* Creating object of class Progressbar and passing stylesheetImportModel, import request  to it */
                    LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(queryimportModel, Constants.REQ_IMPORT, 0, 0);

                    /* Displaying the progress bar */
                    progressBar.ShowDialog();

                    /* Assigning the final message output of the import operation to string Disp */
                    string strDisp = QueryImportService.strAppendedOutput;

                    if (strDisp.Contains(Constants.ERROR) == false)
                    {
                        strHeaderMessage = Constants.HEADER_INFO;
                        strOutMsg = Constants.OPERATION_COMPLETED;
                    }
                    else
                    {
                        strHeaderMessage = Constants.HEADER_INFO;
                        strOutMsg = Constants.OPERATION_FAILED;
                    }

                    /* Creating an object of Diplay class and passing values to it */
                    Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                    if (display.ShowDialog() == true)
                    {
                        /*Display the Message dialog box */
                        display.ShowDialog();
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_INFO;

                    strOutputMessage = Constants.ERR_FILENAMEEMPTY;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /*Display the Warning Message dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENAMEEMPTY);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void OnDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Check if the user double-clicked a grid row and not something else
            DataGridRow row = ItemsControl.ContainerFromElement((DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;

            // If so, go ahead and do my thing
            if (row != null)
                dgQueryName.Items.Refresh();
        }
    }
}
         
           
            
           





