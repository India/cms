﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: OrganizationUC.xaml.cs

Description: This file is the entry point for OrganizationUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    public partial class OrganizationUC : UserControl
    {
        private Logger logger = Logger.getInstance();
        public OrganizationUC()
        {
            InitializeComponent();
        }
        /* This Function gets invoked when user clicks on Export/Import/Clean tab */
        private void TabOrganizationUCSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                try
                {
                    string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                    switch (tabItem)
                    {
                        case Constants.ExpImp:
                            ccExportImport.Content = new OrganizationExportImportUC();
                            break;
                        case Constants.Clean:
                            ccClean.Content = new OrganizationCleanUC();
                            break;
                    }
                }
                catch(Exception ex)
                {
                    logger.error(ex.Message);
                }  
            }
        }
    }
}
