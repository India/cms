﻿/*======================================================================================================================================================================
                Copyright 2020  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: SettingsUC.xaml.cs

    Description: This file contains various operations related to Settings.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Windows.Controls;
using LMICMS.Common.Constants;

namespace LMICMS.UserControls
{
    public partial class SettingsUC : UserControl
    {
        public SettingsUC()
        {
            InitializeComponent();
        }

        private void TabSettingsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

            switch (tabItem)
            {
                case Constants.Common:
                    ccCommon.Content = new CommonSettingsUC();
                    break;
                case Constants.Project:
                    ccProject.Content = new ProjectUC();
                    break;
                case Constants.Users:
                    ccUsers.Content = new UsersUC();
                    break;
                case Constants.TCConfiguration:
                    ccTCConfiguration.Content = new ConfigurationUC();
                    break;
                case Constants.SVNLocation:
                    ccSVNLocation.Content = new SVNLocationUC();
                    break;
                
            }
            e.Handled = true;
        }
    }
}
