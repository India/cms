﻿/*======================================================================================================================================================================
                Copyright 2020  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: UsersUC.xaml.cs

    Description: This file contains various operations related to UsersUC.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Settings;
using LMICMS.Modules.Configuration;

namespace LMICMS.UserControls
{
    public partial class UsersUC : UserControl
    {

        private bool editEndHandle = true;

        #region ctor

        public UsersUC()
        {
            InitializeComponent();      
        }

        #endregion

        #region methods

        private void GridViewRefresh()
        {
            dataGrid.ItemsSource = SettingsModule.GetAllUsers();
            dataGrid.Items.Refresh();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            GridViewRefresh();
        }

        #endregion

        private void BtnDeleteClick(object sender, RoutedEventArgs e)
        {
            Button btnDelete = (Button)e.OriginalSource;
            String strUserGuid = Convert.ToString(btnDelete.CommandParameter);

            if (!String.IsNullOrEmpty(strUserGuid))
            {
                bool deleted = SettingsModule.DeleteUser(strUserGuid);
                if (deleted == true)
                {
                   
                    GridViewRefresh();
                }
            }
        }

        #region events
        /*

    private void TextboxTextChanged(object sender, TextChangedEventArgs e)
    {
        TCUser objUser = dgUsers.SelectedItem as TCUser;
        if (objUser == null && !String.IsNullOrEmpty(txtUserId.Text) && 
                               !string.IsNullOrEmpty(txtUserGroup.Text) && 
                               !string.IsNullOrEmpty(txtUserPassword.Text) &&
                               !string.IsNullOrEmpty(txtUserRole.Text))
        {
            btnAdd.IsEnabled = true;
        }
        else
        {
            btnAdd.IsEnabled = false;
        }
    }
    */
        #endregion


        private void DataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void DataGridRowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (editEndHandle == true)
            {
                editEndHandle = false;

                var objUser = e.Row.Item as TCUser;
                dataGrid.CommitEdit();

                if (!String.IsNullOrEmpty(objUser.UserId) &&
                    !String.IsNullOrEmpty(objUser.UserPassword) &&
                    !String.IsNullOrEmpty(objUser.UserGroup) &&
                    !String.IsNullOrEmpty(objUser.UserRole))
                {
                    if (String.IsNullOrEmpty(objUser.UserGuid))
                    {
                        objUser.UserGuid = Guid.NewGuid().ToString();
                        SettingsModule.SaveUser(objUser);
                        
                    }
                    else
                    {
                        SettingsModule.SaveUser(objUser);
                 
                    }
                }

                GridViewRefresh();
                editEndHandle = true;
            }
        }
    }
}

