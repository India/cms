﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: WorkflowUC.xaml.cs

Description: This file is the entry point for WorkflowUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    public partial class WorkflowUC : UserControl
    {
        private Logger logger = Logger.getInstance();
        public WorkflowUC()
        {
            InitializeComponent();
        }

        /* This Function gets invoked when user clicks on Export/Import tab */
        private void TabWorkflowSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                try
                {
                    string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                    switch (tabItem)
                    {
                        case Constants.Export:
                            ccExport.Content = new WFExportUC();
                            break;
                        case Constants.Import:
                            ccImport.Content = new WFImportUC();
                            break;
                    }
                }
                catch(Exception ex)
                {
                    logger.error(ex.Message);
                }    
            }
        }
    }
}
