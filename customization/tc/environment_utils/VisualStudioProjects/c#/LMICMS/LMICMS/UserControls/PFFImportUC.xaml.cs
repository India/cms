﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: PffImportUC.xaml.cs

Description: This file contains implementation for PffImportUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.ReqResp;
using LMICMS.Services;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for PFFImportUC.xaml
    /// </summary>
    public partial class PFFImportUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage = string.Empty;

        private Logger logger = Logger.getInstance();
        public PFFImportUC()
        {
            InitializeComponent();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_PFF_Location.Text = ConfigConstants.PFFPath;
        }

        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_PFF_Location.Text))
                {
                    /* creating object of Open FileDialog Class */
                    OpenFileDialog openFileDialog = new OpenFileDialog();

                    /* Enabling multiple file selection */
                    openFileDialog.Multiselect = true;

                    /* Setting text box location as initial directory to search */
                    openFileDialog.InitialDirectory = tb_PFF_Location.Text;

                    /* Applying File type filter */
                    openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                    DirectoryInfo dir = new DirectoryInfo(tb_PFF_Location.Text);

                    if (openFileDialog.ShowDialog() == true)

                        foreach (string filename in openFileDialog.FileNames)
                        {
                            String strFileName = System.IO.Path.GetFileName(filename);

                            /* setting the directory path along with file name selected */
                            string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                            if (!dgPFFName.Items.Contains(strFileName))
                            {
                                dgPFFName.Items.Add(strFileName);
                            }
                            /* set the selected file path in text box */
                            tb_PFF_Location.Text = directoryPath;
                        }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_PFF_Location.Text))
                {
                    /* Get all xml type  file paths from the location set in text box */
                    string[] filePaths = Directory.GetFiles(tb_PFF_Location.Text, Constants.FILE_EXT_ALL_XML);

                    foreach (string file in filePaths)
                    {
                        if (dgPFFName.Items.Contains(System.IO.Path.GetFileName(file)))
                        {

                        }
                        else
                        {
                            /* Add files to Datagrid */
                            dgPFFName.Items.Add(System.IO.Path.GetFileName(file));
                        }
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning dialog Box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void Import_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strOutMsg = string.Empty;
                PffImportModel pffImportModel = new PffImportModel();

                if (dgPFFName.Items.Count > 0)
                {
                    foreach (var item in dgPFFName.Items)
                    {
                        /* Add item as string to workflowimportModel */
                        pffImportModel.pffnames.Add(Convert.ToString(item));
                    }

                    /* Assigning Workflow Location to workflow ImportModel */
                    pffImportModel.strFilePath = tb_PFF_Location.Text;

                    /* Creating object of class Progressbar and passing workflowImportModel, import request  to it */
                    LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(pffImportModel, Constants.REQ_PFFIMPORT, 0, 0);

                    /* Displaying the progress bar */
                    progressBar.ShowDialog();

                    /* Assigning the final message output of the import operation to string Disp */
                    string strDisp = PffImportService.strAppendedOutput;

                    if (strDisp.Contains(Constants.ERROR) == false)
                    {
                        strHeaderMessage = Constants.HEADER_INFO;
                        strOutMsg = Constants.OPERATION_COMPLETED;
                    }
                    else
                    {
                        strHeaderMessage = Constants.HEADER_INFO;
                        strOutMsg = Constants.OPERATION_FAILED;
                    }

                    /* Creating an object of Diplay class and passing values to it */
                    Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                    if (display.ShowDialog() == true)
                    {
                        /*Display the Message dialog box */
                        display.ShowDialog();
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_FILENAMEEMPTY;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENAMEEMPTY);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            for (int counter = dgPFFName.Items.Count - 1; counter >= 0; counter--)
            {
                /* Remove selected item from datagrid */
                dgPFFName.Items.Remove(dgPFFName.SelectedItem);
            }
        }

        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void OnDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Check if the user double-clicked a grid row and not something else
            DataGridRow row = ItemsControl.ContainerFromElement((DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;

            // If so, go ahead and do my thing
            if (row != null)
                dgPFFName.Items.Refresh();
        }
    }
}
