﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: StylesheetExportUC.xaml.cs

Description: This file contains implementation for StylesheetExportUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;
using LMICMS.Common.Constants;
using LMICMS.Modules.Configuration;
using LMICMS.ReqResp;
using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.Services;

namespace LMICMS.UserControls
{
    public partial class StylesheetExportUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage = string.Empty;

        private Logger logger = Logger.getInstance();

        List<StylesheetModel> lSearchStylesheetName = null;

        public StylesheetExportUC()
        {  
            InitializeComponent();
            StylesheetExportServiceFinal.strUnusedFilenames = null;
        }

        /* This function is the call back function which gets invoked when StylesheetExportUC is loaded */
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            /*Assigning Query Path in Text box Loaction */
            tb_Export_Location.Text = ConfigConstants.StylesheetPath;
        }

        /* This function gets invoked when user clicks on BrowseFolder Button which  is used to Browse for requisite  Folder Location */
        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                    /* Setting the trimmed text box location text in dialog path */
                    dialog.SelectedPath = tb_Export_Location.Text.Trim();

                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        /* set the selected location in text box */
                        tb_Export_Location.Text = dialog.SelectedPath;
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user clicks on Refresh button to Fetch all StylesheetNames from Teamcenter */
        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dgStylesheetName.ItemsSource = null;
                FileHelper fileHelper = new FileHelper();
                Boolean bIsFileAvailable = false;

                /* Refreshes the datagrid items */
                dgStylesheetName.Items.Refresh();

                /* Accessing the location of Temp Folder */
                string strTempDir = ConfigService.strTempFolder;

                /* Constructing the Filename for Stylesheetnames */
                string strFileName = (Constants.STYLESHEET_NAME + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                /* Starting the Progress bar considering the lengthy operation */
                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(strFileName, Constants.REQ_STYLESHEETNAME, 0, tb_UserReq_Location.Text);

                /* Shows the progressbar dialog box */
                progressBar.ShowDialog();

                /* Checking whether StylesheetName File is available or not */
                bIsFileAvailable = fileHelper.Check_File_Exists(strTempDir + Constants.FILE_PATH + strFileName);

                if (bIsFileAvailable == true)
                {
                    /*Filling the datagrid with Stylesheetnames*/
                    dgStylesheetName.ItemsSource = StylesheetName.lStylesheetNames;

                    /*Refreshing the items contained in datagrid */
                    dgStylesheetName.Items.Refresh();
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_FILENOTEXISTS;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning dialog Box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENOTEXISTS);
                }
            }
            catch(Exception  ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(Constants.ERR_FILENOTEXISTS);
            }
        }

        /*This function gets invoked when user clicks on SelectAll button which Selects the overall datagrid items */
        private void SelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgStylesheetName.ItemsSource == StylesheetName.lStylesheetNames)
                {
                    foreach (var item in StylesheetName.lStylesheetNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgStylesheetName.Items.Refresh();
                }
                else if (dgStylesheetName.ItemsSource == lSearchStylesheetName)
                {
                    foreach (var item in lSearchStylesheetName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgStylesheetName.Items.Refresh();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on UnSelectAll button which UnSelects the overall datagrid items */
        private void UnSelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgStylesheetName.ItemsSource == StylesheetName.lStylesheetNames)
                {
                    foreach (var item in StylesheetName.lStylesheetNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgStylesheetName.Items.Refresh();
                }
                else if (dgStylesheetName.ItemsSource == lSearchStylesheetName)
                {
                    foreach (var item in lSearchStylesheetName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgStylesheetName.Items.Refresh();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user moves up and down in datagrid */
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /* It indicates that the event handler has already processed the event */
            e.Handled = true;
        }

        /*This function gets invoked when user enters text in Search textbox which is Used for performing Live search within datagrid items */
        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                /* Assigning the value of textfilter text box to string */
                string strSearchPattern = txtFilter.Text;

                if (strSearchPattern.Length == 0)
                {
                    /* Assigning asterisk to string */
                    strSearchPattern = Constants.ASTERISK;
                }

                /* Creating object of List<StylesheetModel> class */
                lSearchStylesheetName = new List<StylesheetModel>();

                /*Using Regex.Escape to frame Search pattern to execute live search */
                strSearchPattern = Constants.SEP_CARET + Regex.Escape(strSearchPattern).Replace(Constants.SEP_ASTERISK, Constants.SEP_DOT_ASTERISK) + Constants.SEP_DOLLAR;

                foreach (var item in StylesheetName.lStylesheetNames)
                {
                    /* Matching the Transfermodename with Search Pattern entered in Search Transfermodename text box */
                    if (Regex.IsMatch(item.StylesheetName, strSearchPattern, RegexOptions.IgnoreCase))
                    {
                        /* When match is found then add Stylesheetname and filename to list */
                        lSearchStylesheetName.Add(new StylesheetModel() { StylesheetName = item.StylesheetName, FileName = item.FileName });
                    }
                }

                /*Filling the datagrid with SearchStylesheetnames*/
                dgStylesheetName.ItemsSource = lSearchStylesheetName;

                /*Refreshing the items contained in datagrid */
                dgStylesheetName.Items.Refresh();
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on Export Button to export set of Stylesheetnames to a set location */
        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strOutMsg = string.Empty;
                string strFileName = string.Empty;
                string strStylesheetName = string.Empty;
                StylesheetExportModel stylesheetExportModel = new StylesheetExportModel();

                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    if (dgStylesheetName.ItemsSource == StylesheetName.lStylesheetNames)
                    {
                        foreach (var item in StylesheetName.lStylesheetNames)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row Stylesheetname item to string Stylesheetname seperated by a comma */
                                strStylesheetName = strStylesheetName + ((StylesheetModel)item).StylesheetName + Constants.SEP_COMMA;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((StylesheetModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((StylesheetModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a comma */
                                strFileName = strFileName + ((StylesheetModel)item).FileName + Constants.SEP_COMMA;
                            }
                        }
                    }
                    else if (dgStylesheetName.ItemsSource == lSearchStylesheetName)
                    {
                        foreach (var item in lSearchStylesheetName)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row Stylesheetname item to string Stylesheetname seperated by a comma */
                                strStylesheetName = strStylesheetName + ((StylesheetModel)item).StylesheetName + Constants.SEP_COMMA;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((StylesheetModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((StylesheetModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a comma */
                                strFileName = strFileName + ((StylesheetModel)item).FileName + Constants.SEP_COMMA;
                            }
                        }
                    }
                    /*To add Stylesheetnames as string in stylesheetExportModel */
                    stylesheetExportModel.stylesheet = strStylesheetName;

                    /* To add FileName as string in stylesheetExportModel */
                    stylesheetExportModel.exportedstylesheet = strFileName;

                    if (stylesheetExportModel.exportedstylesheet != null && stylesheetExportModel.exportedstylesheet.Length > 0)
                    {
                        /* Creating object of class Progressbar and passing stylesheetexportmodel, export request and export location to it */
                        LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(stylesheetExportModel, Constants.REQ_STYLESHEETEXPORT, tb_Export_Location.Text, 0);

                        /* Displaying the progress bar */
                        progressBar.ShowDialog();

                        UnSelectAll_Button_Click(sender, e);

                        /* Assigning the final message output of the export operation to string Disp */
                        string strDisp = StylesheetExportServiceFinal.strAppendedOutput;

                        if (strDisp.Contains(Constants.ERROR) == false)
                        {
                            strHeaderMessage = Constants.HEADER_INFO;

                            /* Check whether there are unused stylesheet files in Repository location */
                            if (string.IsNullOrEmpty(StylesheetExportServiceFinal.strUnusedFilenames))
                            {
                                strOutMsg = Constants.OPERATION_COMPLETED + Environment.NewLine + Constants.UNUSED_STYLESHEET_NAME + Environment.NewLine + Constants.NO_UNUSED_STYLESHEET;
                            }
                            else
                            {
                                strOutMsg = Constants.OPERATION_COMPLETED + Environment.NewLine + Constants.UNUSED_STYLESHEET_NAME + Environment.NewLine + StylesheetExportServiceFinal.strUnusedFilenames;
                            }
                        }
                        else
                        {
                            strHeaderMessage = Constants.HEADER_INFO;
                            strOutMsg = Constants.OPERATION_FAILED + Environment.NewLine;
                        }

                        /* Creating an object of Diplay class and passing values to it */
                        Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                        if (display.ShowDialog() == true)
                        {
                            /*Display the Message dialog box */
                            display.ShowDialog();
                        }
                    }
                    else
                    {
                        strHeaderMessage = Constants.HEADER_WARNING;

                        strOutputMessage = Constants.ERR_STYLESHEETNAMEEMPTY;

                        /* Creating object of Warning Display class and passing values to it */
                        WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                        /* Display the Warning Dialog box */
                        display.ShowDialog();

                        /* Calling object logger and passing exception message to write it in Error log file */
                        logger.error(Constants.ERR_STYLESHEETNAMEEMPTY);
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /* Creating object of Warning Display class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Display the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}
