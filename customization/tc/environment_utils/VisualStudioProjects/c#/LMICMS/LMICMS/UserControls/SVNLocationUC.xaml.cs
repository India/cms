﻿/*======================================================================================================================================================================
                Copyright 2020  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: SVNLocationUC.xaml.cs

    Description: This file contains implementation for SVNLocation Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Factories;
using LMICMS.Modules.Configuration;
using CreateOrUpdateRepo = LMICMS.Dialogs.CreateOrUpdateRepo;

namespace LMICMS.UserControls
{
    public partial class SVNLocationUC : UserControl
    {
        public SVNLocationUC()
        {
            InitializeComponent();
        }

        private void GridViewRefresh()
        {
            dgRepos.ItemsSource = SettingsModelFactory.GetRepoModels(ConfigConstants.ActiveProjectGuid);
            dgRepos.Items.Refresh();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            GridViewRefresh();
        }

        private void BtnAddClick(object sender, RoutedEventArgs e)
        {
           LMICMS.Dialogs.CreateOrUpdateRepo createOrUpdateDialog = new CreateOrUpdateRepo();
            if (createOrUpdateDialog.ShowDialog() == true)
            {
                GridViewRefresh();
            }
            e.Handled = true;
        }

        private void BtnEditClick(object sender, RoutedEventArgs e)
        {
            Button btnEdit = (Button)e.OriginalSource;

            CreateOrUpdateRepo createOrUpdateDialog = new CreateOrUpdateRepo(Convert.ToString(btnEdit.CommandParameter));
            if (createOrUpdateDialog.ShowDialog() == true)
            {
                GridViewRefresh();
            }

            e.Handled = true;
        }

        private void BtnDeleteClick(object sender, RoutedEventArgs e)
        {
            Button btnDelete = (Button)e.OriginalSource;
            String strRepoGuid = Convert.ToString(btnDelete.CommandParameter);

            if (!String.IsNullOrEmpty(strRepoGuid))
            {
                bool deleted = SettingsModule.DeleteRepo(ConfigConstants.ActiveProjectGuid, strRepoGuid);
                if (deleted == true)
                {
                    GridViewRefresh();
                }
            }
            e.Handled = true;
        }

        private void BtnSetActiveClick(object sender, RoutedEventArgs e)
        {
            Button btnSetRepoActive = (Button)e.OriginalSource;
            String strRepoGuid = Convert.ToString(btnSetRepoActive.CommandParameter);

            if (!String.IsNullOrEmpty(strRepoGuid))
            {
                bool active = SettingsModule.SetRepoActive(ConfigConstants.ActiveProjectGuid, strRepoGuid);
                if (active == true)
                {
                    GridViewRefresh();
                }
            }
            e.Handled = true;
        }

        private void DataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
