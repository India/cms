﻿using System;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    /// <summary>
    /// Interaction logic for PFFUC.xaml
    /// </summary>
    public partial class PFFUC : UserControl
    {
        private Logger logger = Logger.getInstance();
        public PFFUC()
        {
            InitializeComponent();
        }

        /* This Function gets invoked when user clicks on Export/Import tab */
        private void TabPFFSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                try
                {
                    string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                    switch (tabItem)
                    {
                        case Constants.Export:
                            ccExport.Content = new PFFExportUC();
                            break;
                        case Constants.Import:
                            ccImport.Content = new PFFImportUC();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    logger.error(ex.Message);
                }
            }
        }
    }
}

