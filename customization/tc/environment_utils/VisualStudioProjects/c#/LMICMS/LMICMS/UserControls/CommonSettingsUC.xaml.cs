﻿/*======================================================================================================================================================================
                                Copyright 2020  LMtec India
                                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: CommonSettingsUC.xaml.cs

Description: This file contains implementation for CommonSettingsUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.Settings;
using LMICMS.Factories;
using LMICMS.Modules.Configuration;
using LMICMS.Services;

namespace LMICMS.UserControls
{
    public partial class CommonSettingsUC : UserControl
    {
        public CommonSettingsUC()
        {
            InitializeComponent();
            var commonSettings = SettingsModelFactory.GetCommonSettings();
            if (commonSettings != null)
            {
                txtCMSExeLoc.Text = commonSettings.LMICMSExeLoc;
                txtConfigFilePath.Text = commonSettings.LMIConfigFileLoc;
                txtTempDownPath.Text = commonSettings.LMITempDownloadPath;
            }
        }
        
        private void BtnConfigFilePathClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialogCustom dialog = new OpenFileDialogCustom();
            dialog.ValidateNames = true;
            dialog.CheckFileExists = false;
            dialog.CheckPathExists = true;
            dialog.Filter = "XML Files (*.xml)|*.xml";
            dialog.ValidExtension = ".xml";

            if (dialog.ShowDialog() == true)
            {
                txtConfigFilePath.Text = dialog.Dialog.FileName;
            }
            e.Handled = true;
        }

        private void BtnSaveConfigFilePathClick(object sender, RoutedEventArgs e)
        {
            string message = string.Empty;
            string strConfigFilePath = txtConfigFilePath.Text.Trim();
            if (!string.IsNullOrEmpty(strConfigFilePath))
            {
                if (FileHelper.IsTextFileEmpty(strConfigFilePath) == true)
                {
                    SettingsModule.UpdateConfigFilePath(strConfigFilePath);
                    ConfigService.CreateConfigurationFile(strConfigFilePath);
                    MessageBox.Show(ResourceHelper.FindResource("CSUC.ConfigFileSaved"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    bool isValid = ConfigService.ValidateConfigFile(strConfigFilePath);
                    if (isValid == true)
                    {
                        SettingsModule.UpdateConfigFilePath(strConfigFilePath);
                        ConfigService.RefreshConfiguration();
                        MessageBox.Show(ResourceHelper.FindResource("CSUC.ConfigFileSaved"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show(ResourceHelper.FindResource("CSUC.InvalidConfigFile"), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show(ResourceHelper.FindResource("CSUC.ConfigFilePathEmpty"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            e.Handled = true;
        }

        private void BtnTempDownPathClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = txtTempDownPath.Text.Trim();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtTempDownPath.Text = dialog.SelectedPath;
            }
            e.Handled = true;
        }

        private void BtnCMSExeLocClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = txtCMSExeLoc.Text.Trim();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtCMSExeLoc.Text = dialog.SelectedPath;
            }
            e.Handled = true;
        }

        private void BtnSaveClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtTempDownPath.Text) &&
                !string.IsNullOrEmpty(txtCMSExeLoc.Text))
            {
                LMICMSRoot objRoot = new LMICMSRoot();
                objRoot.LMITempDownloadPath = txtTempDownPath.Text;
                objRoot.LMICMSExeLoc = txtCMSExeLoc.Text;
                bool flag = SettingsModule.SaveCommonSettings(objRoot);
                if (flag == true)
                {
                    MessageBox.Show(ResourceHelper.FindResource("CSUC.GeneralSettingsSaved"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {  
                MessageBox.Show(ResourceHelper.FindResource("CSUC.AllFieldsMand"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            e.Handled = true;
        }

        private void TxtCMSExeLoc_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
