﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: OrganizationCleanUC.xaml.cs

Description: This file contains implementation for OrganizationCleanUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Dialogs;
using LMICMS.Services;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    public partial class OrganizationCleanUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage = string.Empty;

        private Logger logger = Logger.getInstance();

        public OrganizationCleanUC()
        {
            InitializeComponent();
        }

        /* This function is the call back function which gets invoked when QueryImportUC is loaded */
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            tb_Input_Location.Text = ConfigConstants.OrganizationPath;
            tb_Output_Location.Text = ConfigConstants.OrganizationPath;
        }


        /* This function gets invoked when user clicks on BrowseFiles Button which  is used to Browse for requisite  File  */
        private void Input_Location_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Input_Location.Text))
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();

                    openFileDialog.Multiselect = true;

                    openFileDialog.InitialDirectory = tb_Input_Location.Text;

                    openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                    DirectoryInfo dir = new DirectoryInfo(tb_Input_Location.Text);

                    if (openFileDialog.ShowDialog() == true)

                        foreach (string filename in openFileDialog.FileNames)
                        {
                            String strFileName = System.IO.Path.GetFileName(filename);

                            string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                            /* set the selected file path with file name in text box */
                            tb_Input_Location.Text = openFileDialog.FileName;
                        }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

         /* This function gets invoked when user clicks on browse folder button to set path for Output Location */
        private void Output_Location_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (System.IO.Directory.Exists(tb_Output_Location.Text))
                {
                    System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                    dialog.SelectedPath = tb_Output_Location.Text.Trim();

                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        tb_Output_Location.Text = dialog.SelectedPath;
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

         /* This function gets invoked when user clicks on submit button */
        private void Org_Name_Submit_Button_Click(object sender, RoutedEventArgs e)
        {
            string strOutFileName = string.Empty;
            FileNameUtils objFnameUtil = new FileNameUtils();
            try
            {
                if (Path.HasExtension(tb_Input_Location.Text) == true)
                {
                    if (tb_Organization_Name.Text == string.Empty)
                    {
                        /* Construct filename for file to be cleaned */
                        strOutFileName = objFnameUtil.constructGenericFileNames(Constants.OUT_FILE_NAME, Constants.FEX_XML);

                        LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Output_Location.Text, Constants.REQ_ORGANISATIONCLEAN, strOutFileName, tb_Input_Location.Text);

                        progressBar.ShowDialog();
                    }
                    else
                    {
                        LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Output_Location.Text, Constants.REQ_ORGANISATIONCLEAN, tb_Organization_Name.Text, tb_Input_Location.Text);

                        progressBar.ShowDialog();
                    }

                    string strTempDir = ConfigService.strTempFolder;

                    foreach (var file in Directory.GetFiles(ConfigConstants.OrganizationPath, Constants.ALL_EXT_LOG))
                    {
                        string filename = Path.GetFileName(file);

                        File.Move(file, strTempDir + Constants.FILE_PATH + filename);
                    }

                    strHeaderMessage = Constants.HEADER_INFO;

                    strOutputMessage = Constants.OPERATION_COMPLETED;

                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    display.ShowDialog();
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_FILENAMEEMPTY;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENAMEEMPTY);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}
