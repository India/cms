﻿/*======================================================================================================================================================================
                Copyright 2020  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: ProjectUC.xaml.cs

    Description: This file contains various operations related to ProjectUC .

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Modules.Configuration;

namespace LMICMS.UserControls
{
    public partial class ProjectUC : UserControl
    {
        private bool editEndHandle = true;

        public ProjectUC()
        {
            InitializeComponent();
        }

        private void GridViewRefresh()
        {
            dgProject.ItemsSource = SettingsModule.GetAllProjects();
            dgProject.Items.Refresh();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            GridViewRefresh();
        }

        private void BtnDeleteClick(object sender, RoutedEventArgs e)
        {
            Button btnDelete = (Button)e.OriginalSource;
            String strProjectGuid = Convert.ToString(btnDelete.CommandParameter);

            if (!String.IsNullOrEmpty(strProjectGuid))
            {
                bool deleted = SettingsModule.DeleteProject(strProjectGuid);
                if (deleted == true)
                {
                    //lblMessage.Content = ConfigurationManager.AppSettings[AppSettingKeys.ProjectUC_ProjectDeleted];
                    GridViewRefresh();
                }
            }
            e.Handled = true;
        }

        private void BtnSetActiveClick(object sender, RoutedEventArgs e)
        {
            Button btnSetProActive = (Button)e.OriginalSource;
            String strProjectGuid = Convert.ToString(btnSetProActive.CommandParameter);

            if (!String.IsNullOrEmpty(strProjectGuid))
            {
                bool active = SettingsModule.SetProjectActive(strProjectGuid);
                if (active == true)
                {
                    //lblMessage.Content = ConfigurationManager.AppSettings[AppSettingKeys.ProjectUC_ProjectActivated];
                    GridViewRefresh();
                    MainWindow._mainWindow.lblActiveProject.Content = ConfigConstants.ActiveProjectName;
                }
            }
            e.Handled = true;
        }

        private void DataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void DataGridRowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (editEndHandle == true)
            {
                editEndHandle = false;

                var objProject = e.Row.Item as ProjectModel;
                dgProject.CommitEdit();

                if (!String.IsNullOrEmpty(objProject.ProjectName))
                {
                    if (String.IsNullOrEmpty(objProject.ProjectGuid))
                    {
                        objProject.ProjectGuid = Guid.NewGuid().ToString();
                        SettingsModule.SaveProject(objProject);
                        //lblMessage.Content = ConfigurationManager.AppSettings[AppSettingKeys.ProjectUC_ProjectAdded];
                    }
                    else
                    {
                        SettingsModule                                                                                                                                     .SaveProject(objProject);
                        //lblMessage.Content = ConfigurationManager.AppSettings[AppSettingKeys.ProjectUC_ProjectUpdated];
                    }
                }

                GridViewRefresh();
                MainWindow._mainWindow.lblActiveProject.Content = ConfigConstants.ActiveProjectName;
                editEndHandle = true;
            }
        }

        private void DataGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
           
        }    
    }
}
