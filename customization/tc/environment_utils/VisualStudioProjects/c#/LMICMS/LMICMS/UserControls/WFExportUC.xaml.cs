﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: WFExportUC.xaml.cs

Description: This file contains implementation for Workflow Export Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.Modules.Configuration;
using LMICMS.ReqResp;
using LMICMS.Services;

namespace LMICMS.UserControls
{
    public partial class WFExportUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage = string.Empty;

        private Logger logger = Logger.getInstance();

        /* Creating object of List<WorkflowModel> class */
        List<WorkflowModel> lSearchWoWorkflowName = null;
            
        public WFExportUC()
        {
            InitializeComponent();
        }

        /* This function is the call back function which gets invoked when WFExportUC is loaded */
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            /*Assigning Workflow Path in Text box Loaction */
            tb_Export_Location.Text = ConfigConstants.WorkflowPath;
        }

        /*This function gets invoked when user clicks on SelectAll button which Selects the overall datagrid items */
        private void SelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgWorkflowName.ItemsSource == WorkflowName.lWorkflowNames)
                {
                    foreach (var item in WorkflowName.lWorkflowNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgWorkflowName.Items.Refresh();
                }
                else if (dgWorkflowName.ItemsSource == lSearchWoWorkflowName)
                {
                    foreach (var item in lSearchWoWorkflowName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgWorkflowName.Items.Refresh();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on UnSelectAll button which UnSelects the overall datagrid items */
        private void UnSelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgWorkflowName.ItemsSource == WorkflowName.lWorkflowNames)
                {
                    foreach (var item in WorkflowName.lWorkflowNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgWorkflowName.Items.Refresh();
                }
                else if (dgWorkflowName.ItemsSource == lSearchWoWorkflowName)
                {
                    foreach (var item in lSearchWoWorkflowName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgWorkflowName.Items.Refresh();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user clicks on Refresh button to Fetch all WorkflowNames from Teamcenter */
        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dgWorkflowName.ItemsSource = null;
                FileHelper fileHelper = new FileHelper();
                Boolean bIsFileAvailable = false;

                /* Refreshes the datagrid items */
                dgWorkflowName.Items.Refresh();

                /* Accessing the location of Temp Folder */
                string strTempDir = ConfigService.strTempFolder;

                /* Constructing the Filename for Workflownames */
                string strFileName = (Constants.WORKFLOW_NAME + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                /* Starting the Progress bar considering the lengthy operation */
                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(strFileName, Constants.REQ_WORKFLOWNAME, 0, 0);

                /* Shows the progressbar dialog box */
                progressBar.ShowDialog();

                /* Checking whether WorkflowName File is available or not */
                bIsFileAvailable = fileHelper.Check_File_Exists(strTempDir + Constants.FILE_PATH + strFileName);

                if (bIsFileAvailable == true)
                {
                    /*Filling the datagrid with Workflownames*/
                    dgWorkflowName.ItemsSource = WorkflowName.lWorkflowNames;

                    /*Refreshing the items contained in datagrid */
                    dgWorkflowName.Items.Refresh();
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_FILENOTEXISTS;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Display the Warning dialog Box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENOTEXISTS);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user clicks on BrowseFolder Button which  is used to Browse for requisite  Folder Location */
        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                    /* Setting the trimmed text box location text in dialog path */
                    dialog.SelectedPath = tb_Export_Location.Text.Trim();

                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        /* set the selected location in text box */
                        tb_Export_Location.Text = dialog.SelectedPath;
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning dialog Box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user enters text in Search textbox which is Used for performing Live search within datagrid items */
        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                /* Assigning the value of textfilter text box to string */
                string strSearchPattern = txtFilter.Text;

                if (strSearchPattern.Length == 0)
                {
                    /* Assigning asterisk to string */
                    strSearchPattern = Constants.ASTERISK;
                }

                lSearchWoWorkflowName = new List<WorkflowModel>();

                /*Using Regex.Escape to frame Search pattern to execute live search */
                strSearchPattern = Constants.SEP_CARET + Regex.Escape(strSearchPattern).Replace(Constants.SEP_ASTERISK, Constants.SEP_DOT_ASTERISK) + Constants.SEP_DOLLAR;

                foreach (var item in WorkflowName.lWorkflowNames)
                {
                    /* Matching the Workflowname with Search Pattern entered in Search Workflowname text box */
                    if (Regex.IsMatch(item.WorkflowName, strSearchPattern, RegexOptions.IgnoreCase))
                    {
                        /* When match is found then add Workflowname and filename to list */
                        lSearchWoWorkflowName.Add(new WorkflowModel() { WorkflowName = item.WorkflowName, FileName = item.FileName });
                    }
                }
                /*Filling the datagrid with SearchWorkflownames*/
                dgWorkflowName.ItemsSource = lSearchWoWorkflowName;

                /*Refreshing the items contained in datagrid */
                dgWorkflowName.Items.Refresh();
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }     
        }

        /* This function gets invoked when user moves up and down in datagrid */
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /* It indicates that the event handler has already processed the event */
            e.Handled = true;
        }

        /*This function gets invoked when user clicks on Export Button to export set of workflows to a set location */
        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strOutMsg = string.Empty;
                string strFileName = string.Empty;
                string strWFName = string.Empty;
                WorkflowExportModel workflowExpModel = new WorkflowExportModel();

                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    if (dgWorkflowName.ItemsSource == WorkflowName.lWorkflowNames)
                    {
                        foreach (var item in WorkflowName.lWorkflowNames)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row  Workflowname item to string WFname seperated by a New Line */
                                strWFName = strWFName + ((WorkflowModel)item).WorkflowName + System.Environment.NewLine;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((WorkflowModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((WorkflowModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a New line  */
                                strFileName = strFileName + ((WorkflowModel)item).FileName + System.Environment.NewLine;
                            }
                        }     
                    }
                    else if (dgWorkflowName.ItemsSource == lSearchWoWorkflowName)
                    {
                        foreach (var item in lSearchWoWorkflowName)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row  Workflowname item to string WFname seperated by a New Line */
                                strWFName = strWFName + ((WorkflowModel)item).WorkflowName + System.Environment.NewLine;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((WorkflowModel)item).FileName = System.IO.Path.GetFileNameWithoutExtension(((WorkflowModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a New line  */
                                strFileName = strFileName + ((WorkflowModel)item).FileName + System.Environment.NewLine;
                            }
                        }
                    }
                    
                    /* To add WFNames as string in workflowExpModel */
                    workflowExpModel.WFName = strWFName;

                    /* To add FileNames as string in workflowExpModel */
                    workflowExpModel.ExportWFNames = strFileName;

                    if (workflowExpModel.ExportWFNames != null && workflowExpModel.ExportWFNames.Length > 0)
                    {
                        /* Creating object of class Progressbar and passing Workflowexportmodel, export request and export location to it */
                        LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(workflowExpModel, Constants.REQ_WORKFLOWEXPORT, tb_Export_Location.Text, 0);

                        /* Displaying the progress bar */
                        progressBar.ShowDialog();

                        UnSelectAll_Button_Click(sender, e);

                        /* Assigning the final message output of the export operation to string Disp */
                        string strDisp = WorkflowExportService.strAppendedOutput;

                        if (strDisp.Contains(Constants.ERROR) == false)
                        {
                            strHeaderMessage = Constants.HEADER_INFO;
                            strOutMsg = Constants.OPERATION_COMPLETED;
                        }
                        else
                        {
                            strHeaderMessage = Constants.HEADER_INFO;
                            strOutMsg = Constants.OPERATION_FAILED;
                        }

                        /* Creating an object of Diplay class and passing values to it */
                        Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                        if (display.ShowDialog() == true)
                        {
                            /* Display the Message Dialog box */
                            display.ShowDialog();
                        }
                    }
                    else
                    {
                        strHeaderMessage = Constants.HEADER_WARNING;

                        strOutputMessage = Constants.ERR_WFNAMEEMPTY;

                        /* Creating object of Warning Display class and passing values to it */
                        WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                        /* Display the Warning Dialog box */
                        display.ShowDialog();

                        /* Calling object logger and passing exception message to write it in Error log file */
                        logger.error(Constants.ERR_WFNAMEEMPTY);
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /* Creating object of Warning Display class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Display the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}

