﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: StylesheetUC.xaml.cs

Description: This file is the entry point for  StylesheetUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    public partial class StylesheetUC : UserControl
    {
        private Logger logger = Logger.getInstance();
        public StylesheetUC()
        {
            InitializeComponent();
        }
        /* This Function gets invoked when user clicks on Export/Import tab */
        private void TabStylesheetSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                try
                {
                    string tabItem = (((sender as TabControl).SelectedItem as TabItem).Header as TextBlock).Text as string;

                    switch (tabItem)
                    {
                        case Constants.Export:
                            ccExport.Content = new StylesheetExportUC();
                            break;
                        case Constants.Import:
                            ccImport.Content = new StylesheetImportUC();
                            break;
                    }
                }
                catch(Exception ex)
                {
                    logger.error(ex.Message);
                }  
            }
        }
    }
}
