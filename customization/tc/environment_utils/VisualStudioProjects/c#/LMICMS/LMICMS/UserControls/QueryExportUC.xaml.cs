﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: QueryExportUC.xaml.cs

Description: This file contains implementation for Query Export Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using LMICMS.Dialogs;
using LMICMS.Services;
using LMICMS.Common.Utilities;
using LMICMS.Common.Constants;
using LMICMS.Modules.Configuration;
using LMICMS.ReqResp;

namespace LMICMS.UserControls
{
    public partial class QueryExportUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage = string.Empty;
       
        private Logger logger = Logger.getInstance();

        List<QueryModel> lSearchQueryName = null;

        public QueryExportUC()
        {
            InitializeComponent();
        }

       /* This function is the call back function which gets invoked when QueryExportUC is loaded */
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            /*Assigning Query Path in Text box Loaction */
            tb_Export_Location.Text = ConfigConstants.QueryPath;
        }

        /* This Function gets invoked when user clicks on Refresh button to Fetch all QueryNames from Teamcenter */
        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dgQueryName.ItemsSource = null;
                Boolean bIsQueryNameFileAvailable = false;
                FileHelper fileHelper = new FileHelper();

                /* Refreshes the datagrid items */
                dgQueryName.Items.Refresh();

                /* Accessing the location of Temp Folder and storing it in string */
                string strTempDir = ConfigService.strTempFolder;

                /* Constructing the Filename for Querynames */
                string strFileName = (Constants.QUERY_NAME + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                /* Starting the Progress bar considering the lengthy operation */
                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(strFileName, Constants.REQ_QUERYNAME, 0, 0);

                /* Shows the progressbar dialog box */
                progressBar.ShowDialog();

                /* Checking whether QueryName File is available or not */
                bIsQueryNameFileAvailable = fileHelper.Check_File_Exists(strTempDir + Constants.FILE_PATH + strFileName);

                if (bIsQueryNameFileAvailable == true)
                {
                    /*Filling the datagrid with Querynames*/
                    dgQueryName.ItemsSource = QueryName.lQueryNames;

                    /*Refreshing the items contained in datagrid */
                    dgQueryName.Items.Refresh();
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_FILENOTEXISTS;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning dialog Box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENOTEXISTS);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user clicks on BrowseFolder Button which  is used to Browse for requisite  Folder Location */
        private void Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                    /* Setting the trimmed text box location text in dialog path */
                    dialog.SelectedPath = tb_Export_Location.Text.Trim();

                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        /* set the selected location in text box */
                        tb_Export_Location.Text = dialog.SelectedPath;
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
            
        }

        /*This function gets invoked when user clicks on SelectAll button which Selects the overall datagrid items */
        private void SelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgQueryName.ItemsSource == QueryName.lQueryNames)
                {
                    foreach (var item in QueryName.lQueryNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgQueryName.Items.Refresh();
                }
                else if (dgQueryName.ItemsSource == lSearchQueryName)
                {
                    foreach (var item in lSearchQueryName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = true;
                        }
                        else if (item.bIsActive == false)
                        {
                            item.bIsActive = true;
                        }
                        else
                        {
                            item.bIsActive = true;
                        }
                    }
                    dgQueryName.Items.Refresh();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        /*This function gets invoked when user clicks on UnSelectAll button which UnSelects the overall datagrid items */
        private void UnSelectAll_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgQueryName.ItemsSource == QueryName.lQueryNames)
                {
                    foreach (var item in QueryName.lQueryNames)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgQueryName.Items.Refresh();
                }
                else if (dgQueryName.ItemsSource == lSearchQueryName)
                {
                    foreach (var item in lSearchQueryName)
                    {
                        if (item.bIsActive == true)
                        {
                            item.bIsActive = false;
                        }
                    }
                    dgQueryName.Items.Refresh();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }


        /*This function gets invoked when user enters text in Search textbox which is Used for performing Live search within datagrid items */
        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                /* Assigning the value of textfilter text box to string */
                string strSearchPattern = txtFilter.Text;

                if (strSearchPattern.Length == 0)
                {
                    /* Assigning asterisk to string */
                    strSearchPattern = Constants.ASTERISK;
                }

                /* Creating object of List<QueryModel> class */
                lSearchQueryName = new List<QueryModel>();

                /*Using Regex.Escape to frame Search pattern to execute live search */
                strSearchPattern = Constants.SEP_CARET + Regex.Escape(strSearchPattern).Replace(Constants.SEP_ASTERISK, Constants.SEP_DOT_ASTERISK) + Constants.SEP_DOLLAR;

                foreach (var item in QueryName.lQueryNames)
                {
                    /* Matching the Queryname with Search Pattern entered in Search Queryname text box */
                    if (Regex.IsMatch(item.QueryName, strSearchPattern, RegexOptions.IgnoreCase))
                    {
                        /* When match is found then add Queryname and filename to list */
                        lSearchQueryName.Add(new QueryModel() { QueryName = item.QueryName, FileName = item.FileName });
                    }
                }

                /*Filling the datagrid with SearchQuerynames*/
                dgQueryName.ItemsSource = lSearchQueryName;

                /*Refreshing the items contained in datagrid */
                dgQueryName.Items.Refresh();
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user moves up and down in datagrid */
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /* It indicates that the event handler has already processed the event */
            e.Handled = true;
        }

        /*This function gets invoked when user clicks on Export Button to export set of Queries to a set location */
        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strFileName = string.Empty;
                string strQueryName = string.Empty;
                string strOutMsg = string.Empty;
                QueryExportModel exportModel = new QueryExportModel();

                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    if (dgQueryName.ItemsSource == QueryName.lQueryNames)
                    {
                        foreach (var item in QueryName.lQueryNames)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row  Queryname item to string Queryname seperated by a comma */
                                strQueryName = strQueryName + ((QueryModel)item).QueryName + Constants.SEP_COMMA;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((QueryModel)item).FileName = Path.GetFileNameWithoutExtension(((QueryModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a comma */
                                strFileName = strFileName + ((QueryModel)item).FileName + Constants.SEP_COMMA;
                            }
                        }
                         
                    }
                    else if (dgQueryName.ItemsSource == lSearchQueryName)
                    {
                        foreach (var item in lSearchQueryName)
                        {
                            /* To check a particular item in a row is set as active */
                            if (item.bIsActive == true)
                            {
                                /* Add selected row  Queryname item to string Queryname seperated by a comma */
                                strQueryName = strQueryName + ((QueryModel)item).QueryName + Constants.SEP_COMMA;

                                /* Remove any extension name if found in  Selected row  Filename */
                                ((QueryModel)item).FileName = Path.GetFileNameWithoutExtension(((QueryModel)item).FileName);

                                /* Add  selected row Filename item to string Filename seperated by a comma */
                                strFileName = strFileName + ((QueryModel)item).FileName + Constants.SEP_COMMA;
                            }
                        }   
                    }
                    /* To add Querynames as string in exportModel */
                    exportModel.queries = strQueryName;

                    /* To add File names as string in exportModel */
                    exportModel.exportedqueries = strFileName;

                    if (exportModel.exportedqueries != null && exportModel.exportedqueries.Length > 0)
                    {
                        /* Creating object of class Progressbar and passing exportmodel, export request and export location to it */
                        LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(exportModel, Constants.REQ_EXPORT, tb_Export_Location.Text, 0);

                        /* Displaying the progress bar */
                        progressBar.ShowDialog();

                        UnSelectAll_Button_Click(sender, e);

                        /* Assigning the final message output of the export operation to string Disp */
                        string strDisp = QueryExportServiceFinal.strAppendedOutput;

                        if ((strDisp.Contains(Constants.ERROR) == false))
                        {
                            strHeaderMessage = Constants.HEADER_INFO;
                            strOutMsg = Constants.OPERATION_COMPLETED;
                        }
                        else
                        {
                            strHeaderMessage = Constants.HEADER_INFO;
                            strOutMsg = Constants.OPERATION_FAILED;
                        }

                        /* Creating an object of Diplay class and passing values to it */
                        Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                        if (display.ShowDialog() == true)
                        {
                            /* Display the Message Dialog box */
                            display.ShowDialog();
                        }
                    }
                    else
                    {
                        strHeaderMessage = Constants.HEADER_WARNING;

                        strOutputMessage = Constants.ERR_QRYNAMEEMPTY;

                        /* Creating object of Warning Display class and passing values to it */
                        WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                        /* Display the Warning Dialog box */
                        display.ShowDialog();

                        /* Calling object logger and passing exception message to write it in Error log file */
                        logger.error(Constants.ERR_QRYNAMEEMPTY);
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /* Creating object of Warning Display class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Display the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            } 
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}

        

















