﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: ACLUC.xaml.cs

Description: This file contains implementation for ACLUC Operation

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Dialogs;
using LMICMS.Services;
using LMICMS.Common.Utilities;

namespace LMICMS.UserControls
{
    public partial class ACLUC : UserControl
    {
        public string strHeaderMessage = string.Empty;

        public string strOutputMessage = string.Empty;

        private Logger logger = Logger.getInstance();
        public ACLUC()
        {
            InitializeComponent();
        }

        /* This function is the call back function which gets invoked when ACLUC is loaded */
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            /*Assigning ACL Path in Export Text box Loaction */
            tb_Export_Location.Text = ConfigConstants.ACLPath;

            /*Assigning ACL Path in  Import Text box Loaction */
            tb_Import_Location.Text = ConfigConstants.ACLPath;
        }

        /* This function gets invoked when user clicks on BrowseFolder Button which  is used to Browse for requisite  Folder Location */
        private void Exp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Export_Location.Text))
                {
                    System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                    /* Setting the trimmed text box location text in dialog path */
                    dialog.SelectedPath = tb_Export_Location.Text.Trim();

                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        /* set the selected location in text box */
                        tb_Export_Location.Text = dialog.SelectedPath;
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /* This function gets invoked when user clicks on BrowseFiles Button which  is used to Browse and select  for requisite  File */
        private void Imp_Browse_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Check whether Location in text box exists or not */
                if (System.IO.Directory.Exists(tb_Import_Location.Text))
                {
                    /* creating object of Open FileDialog Class */
                    OpenFileDialog openFileDialog = new OpenFileDialog();

                    /* Enabling multiple file selection */
                    openFileDialog.Multiselect = true;

                    /* Setting Import text box location as initial directory to search */
                    openFileDialog.InitialDirectory = tb_Import_Location.Text;

                    /* Applying File type filter */
                    openFileDialog.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                    DirectoryInfo dir = new DirectoryInfo(tb_Import_Location.Text);

                    if (openFileDialog.ShowDialog() == true)

                        foreach (string filename in openFileDialog.FileNames)
                        {
                            String strFileName = System.IO.Path.GetFileName(filename);

                            /* setting the directory path along with file name selected */
                            string directoryPath = System.IO.Path.GetDirectoryName(openFileDialog.FileName);

                            /* set the selected file in text box */
                            tb_Import_Location.Text = openFileDialog.FileName;
                        }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_INVALIDPATH;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_INVALIDPATH);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on Import Button to import  ACL to a set location */
        private void Import_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Path.HasExtension(tb_Import_Location.Text) == true)
                {
                    string strOutMsg = string.Empty;

                    /* Creating object of class Progressbar and passing importLocation, import request  to it */
                    LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Import_Location.Text, Constants.REQ_ACLIMPORT, 0, 0);

                    /* Displaying the progress bar */
                    progressBar.ShowDialog();

                    /* Assigning the final message output of the import operation to string Disp */
                    string strDisp = ACLImportService.strOutput;

                    if (strDisp.Contains(Constants.ERROR) == false)
                    {
                        strHeaderMessage = Constants.HEADER_INFO;
                        strOutMsg = Constants.OPERATION_COMPLETED;
                    }
                    else
                    {
                        strHeaderMessage = Constants.HEADER_INFO;
                        strOutMsg = Constants.OPERATION_FAILED;
                    }

                    /* Creating an object of Diplay class and passing values to it */
                    Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                    if (display.ShowDialog() == true)
                    {
                        /*Display the Message dialog box */
                        display.ShowDialog();
                    }
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_WARNING;

                    strOutputMessage = Constants.ERR_FILENAMEEMPTY;

                    /*Creating object of Warningdisplay class and passing values to it */
                    WarningDisplay display = new WarningDisplay(strOutputMessage, strHeaderMessage);

                    /* Displaying the Warning Dialog box */
                    display.ShowDialog();

                    /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(Constants.ERR_FILENAMEEMPTY);

                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on Export Button to export ACL to a set location */
        private void Export_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strOutMsg = string.Empty;

                /* Creating object of class Progressbar and passing Acl name, export request and export location to it */
                LMICMS.Dialogs.ProgressBar progressBar = new LMICMS.Dialogs.ProgressBar(tb_Export_Location.Text, Constants.REQ_ACLEXPORT, tb_acl_name.Text, 0);

                /* Displaying the progress bar */
                progressBar.ShowDialog();

                /* Assigning the final message output of the export operation to string Disp */
                string strDisp = ACLExportService.strOutput;

                if (strDisp.Contains(Constants.ERROR) == false)
                {
                    strHeaderMessage = Constants.HEADER_INFO;
                    strOutMsg = Constants.OPERATION_COMPLETED;
                }
                else
                {
                    strHeaderMessage = Constants.HEADER_INFO;
                    strOutMsg = Constants.OPERATION_FAILED;
                }

                /* Creating an object of Diplay class and passing values to it */
                Display display = new Display(strDisp, strHeaderMessage, strOutMsg);

                if (display.ShowDialog() == true)
                {
                    /*Display the Message dialog box */
                    display.ShowDialog();
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }   
    }
}
