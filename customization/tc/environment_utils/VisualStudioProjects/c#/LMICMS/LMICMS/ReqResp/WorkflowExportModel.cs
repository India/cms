﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: WorkflowExportModel.cs

Description: This file contains operations related to Workflow Export.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;

namespace LMICMS.ReqResp
{
   public class WorkflowExportModel
    {
        public WorkflowExportModel()
        {
            ExportWFNames = string.Empty;

            WFName = string.Empty;
        }

        public string WFName { get; set; }
        public string strFilePath { get; set; }
        public string ExportWFNames { get; set; }
    }
}
