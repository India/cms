﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: PffExportModel.cs

Description: This file contains operations related to PFF Export.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.ReqResp
{
    public class PffExportModel
    {
        public PffExportModel()
        {
            pff = string.Empty;

            exportedpff = string.Empty;

            pffUID = string.Empty;
        }

        public string pff { get; set; }
        public string exportedpff { get; set; }
        public string pffUID { get; set; }
    }
}
