﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: ImportModel.cs

Description: This file contains operations related to Query Import.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;

namespace LMICMS.ReqResp
{
    public class QueryImportModel
    {
        public QueryImportModel()
        {
            queries = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> queries { get; set; }
    }
}
