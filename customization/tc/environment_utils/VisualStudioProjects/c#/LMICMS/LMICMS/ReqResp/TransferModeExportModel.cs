﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: TransferModeExportModel.cs

Description: This file contains operations related to TransferMode Export.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;

namespace LMICMS.ReqResp
{
   public class TransferModeExportModel
    {
        public TransferModeExportModel()
        {
            transfermode = string.Empty;

            exportedtransfermode = string.Empty;

            transfermodeUID = string.Empty;
        }

        public string transfermode { get; set; }
        public string exportedtransfermode { get; set; }
        public string transfermodeUID { get; set; }
    }
}
