﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: StylesheetImportModel.cs

Description: This file contains operations related to Stylesheet Import.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;

namespace LMICMS.ReqResp
{
   public class StylesheetImportModel
    {
        public StylesheetImportModel()
        {
            stylesheet = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> stylesheet { get; set; }
    }
}
