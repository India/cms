﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: TransferModeImportModel.cs

Description: This file contains operations related to TransferMode Import.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;

namespace LMICMS.ReqResp
{
  public  class TransferModeImportModel
    {
        public TransferModeImportModel()
        {
            transfermode = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> transfermode { get; set; }
    }
}
