﻿/*======================================================================================================================================================================
                                                Copyright 2020  LMtec India
                                                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: WorkflowImportModel.cs

Description: This file contains operations related to Workflow Import.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;

namespace LMICMS.ReqResp
{
    class WorkflowImportModel
    {
        public WorkflowImportModel()
        {
            workflownames = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> workflownames { get; set; }
    }
}
