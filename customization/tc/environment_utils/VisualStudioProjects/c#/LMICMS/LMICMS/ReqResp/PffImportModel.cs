﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.ReqResp
{
   public class PffImportModel
    {
        public PffImportModel()
        {
            pffnames = new List<string>();
        }
        public String strFilePath { get; set; }
        public List<String> pffnames { get; set; }
    }
}
