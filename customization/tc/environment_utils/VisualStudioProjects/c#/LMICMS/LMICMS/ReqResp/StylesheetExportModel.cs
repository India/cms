﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: StylesheetExportModel.cs

Description: This file contains operations related to Stylesheet Export.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;

namespace LMICMS.ReqResp
{
   public  class StylesheetExportModel
    {
        public StylesheetExportModel()
        {
            stylesheet = string.Empty;

            exportedstylesheet = string.Empty;
        }

        public string stylesheet { get; set; }
        public string exportedstylesheet { get; set; }
    }
}
