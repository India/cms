﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: ReqandResp.cs

Description: This file contains operations related to ReqandResp

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Services;

namespace LMICMS.ReqResp
{
    class ReqandResp
    {
        private Logger logger = Logger.getInstance();

        public ReqandResp()
        {
        }
        public void processReqEvent(Object objInp, int iEventType, Object val, Object value)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_IMPORT:
                        req_import_operation(objInp);
                        break;
                    case Constants.REQ_WORKFLOWIMPORT:
                        req_workflow_import_operation(objInp);
                        break;
                    case Constants.REQ_TRANSFERMODEIMPORT:
                        req_transfer_mode_import_operation(objInp);
                        break;
                    case Constants.REQ_ACLIMPORT:
                        req_acl_import_operation(objInp);
                        break;
                    case Constants.REQ_STYLESHEETIMPORT:
                        req_stylesheet_import_operation(objInp);
                        break;
                    case Constants.REQ_ORGANISATIONIMPORT:
                        req_organisation_import_operation(objInp, val);
                        break;
                    case Constants.REQ_PFFIMPORT:
                        req_pff_import_operation(objInp, val);
                        break;
                    case Constants.REQ_EXPORT:
                        req_export_operation(objInp, val);
                        break;
                    case Constants.REQ_WORKFLOWEXPORT:
                        req_workflowexport_operation(objInp, val);
                        break;
                    case Constants.REQ_TRANSFERMODEEXPORT:
                        req_transfer_mode_export_operation(objInp, val);
                        break;
                    case Constants.REQ_ACLEXPORT:
                        req_acl_export_operation(objInp, val);
                        break;
                    case Constants.REQ_STYLESHEETEXPORT:
                        req_stylesheet_export_operation(objInp, val);
                        break;
                    case Constants.REQ_ORGANISATIONEXPORT:
                        req_organisation_export_operation(objInp, val);
                        break;
                    case Constants.REQ_PFFEXPORT:
                        req_pff_export_operation(objInp, val);
                        break;
                    case Constants.REQ_QUERYNAME:
                        req_query_operation(objInp);
                        break;
                    case Constants.REQ_WORKFLOWNAME:
                        req_workflow_operation(objInp);
                        break;
                    case Constants.REQ_TRANSFERMODENAME:
                        req_transfer_mode_operation(objInp);
                        break;
                    case Constants.REQ_STYLESHEETNAME:
                        req_stylesheet_operation(objInp, value);
                        break;
                    case Constants.REQ_ORGANISATIONCLEAN:
                        req_clean_operation(objInp, val, value);
                        break;
                    case Constants.REQ_PFFNAME:
                        req_pff_operation(objInp, value);
                        break;
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }

        }

        private void req_pff_export_operation(object objInp, object val)
        {
            try
            {
                PffExportService obj = new PffExportService();

                PffExportModel pffExportModel = (PffExportModel)objInp;

                obj.PffProcessExport(pffExportModel, val.ToString());
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void req_pff_import_operation(object objInp, object val)
        {

            try
            {
                PffImportService obj = new PffImportService();

                PffImportModel pffImportModel = (PffImportModel)objInp;

                obj.PffProcessImport(pffImportModel);

            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void req_query_operation(object objInp)
        {
            try
            {
                QueryName obj = new QueryName();

                obj.ProcessQueryName(objInp.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_workflow_operation(object objInp)
        {
            try
            {
                WorkflowName obj = new WorkflowName();

                obj.ProcessWorkflowName(objInp.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_transfer_mode_operation(object objInp)
        {
            try
            {
                TransferModeName obj = new TransferModeName();

                obj.ProcessTransferModeName(objInp.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_stylesheet_operation(object objInp, object value)
        {
            try
            {
                StylesheetName obj = new StylesheetName();

                obj.ProcessStylesheetName(objInp.ToString(), value.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_clean_operation(object objInp, object val, object value)
        {
            try
            {
                OrganisationCleanService obj = new OrganisationCleanService();

                obj.ProcessOrganisationClean(objInp.ToString(), val.ToString(), value.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void req_pff_operation(object objInp, object value)
        {
            try
            {
                PffName obj = new PffName();

                obj.ProcessPffName(objInp.ToString(), value.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_export_operation(object objInp, object val)
        {
            try
            {
                QueryExportService obj = new QueryExportService();

                QueryExportModel model = (QueryExportModel)objInp;

                obj.ProcessExport(model, val.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }

        }
        private void req_workflowexport_operation(object objInp, object val)
        {
            try
            {
                WorkflowExportService obj = new WorkflowExportService();

                WorkflowExportModel workflowExportModel = (WorkflowExportModel)objInp;

                obj.WorkflowProcessExport(workflowExportModel, val.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }

        }

        private void req_transfer_mode_export_operation(object objInp, object val)
        {
            try
            {
                TransferModeExportService obj = new TransferModeExportService();

                TransferModeExportModel model = (TransferModeExportModel)objInp;

                obj.TransferModeProcessExport(model, val.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_acl_export_operation(object objInp, object val)
        {
            try
            {
                ACLExportService obj = new ACLExportService();

                obj.ACLProcessExport(objInp.ToString(), val.ToString());
            }

            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_stylesheet_export_operation(object objInp, object val)
        {
            try
            {
                StylesheetExportService obj = new StylesheetExportService();

                StylesheetExportModel stylesheetExportModel = (StylesheetExportModel)objInp;

                obj.StylesheetProcessExport(stylesheetExportModel, val.ToString());
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void req_organisation_export_operation(object objInp, object val)
        {
            try
            {
                OrganisationExportService obj = new OrganisationExportService();

                obj.OrganisationProcessExport(objInp.ToString(), val.ToString());
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }

        }
        private void req_import_operation(object objInp)
        {
            try
            {
                QueryImportService obj = new QueryImportService();

                QueryImportModel model = (QueryImportModel)objInp;

                obj.ProcessImport(model);
            }
            catch (Exception ex)
            {

                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void req_workflow_import_operation(object objInp)
        {
            try
            {
                WorkflowImportService obj = new WorkflowImportService();

                WorkflowImportModel workflowImportModel = (WorkflowImportModel)objInp;

                obj.WorkflowProcessImport(workflowImportModel);

            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_transfer_mode_import_operation(object objInp)
        {
            try
            {
                TransferModeImportService obj = new TransferModeImportService();

                TransferModeImportModel transferModeImportModel = (TransferModeImportModel)objInp;

                obj.TransferModeProcessImport(transferModeImportModel);
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_acl_import_operation(object objInp)
        {
            try
            {
                ACLImportService obj = new ACLImportService();

                obj.ACLProcessImport(objInp.ToString());
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        private void req_stylesheet_import_operation(object objInp)
        {
            try
            {
                StylesheetImportService obj = new StylesheetImportService();

                StylesheetImportModel stylesheetImportModel = (StylesheetImportModel)objInp;

                obj.StylesheetProcessImport(stylesheetImportModel);
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
        private void req_organisation_import_operation(object objInp , object val)
        {
            try
            {
                OrganisationImportService obj = new OrganisationImportService();

                obj.OrganisationProcessImport(objInp.ToString(), val.ToString());
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }

        public void handleResponseEvent(Object outObj, int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case Constants.REQ_CANCELLED:
                        break;      
                }
            }
            catch (Exception ex)
            {
                /*  No need to do anything here, because this has been called by Custom Progress bar form, and upto now that form has already been disposed,
                 *  so that exception will become unhandled, leaving it the way it is and it will remain the thing as it is. 
                 */

                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}
       
