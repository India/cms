﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: StylesheetWriteFile.cs

Description: This file contains operations related to Stylesheet FileWrite.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using System.Text;
using LMICMS.Common.Constants;

namespace LMICMS.ReqResp
{
    class StylesheetWriteFile
    {
        public  void Process_Execute_Stylesheet_Write_File(string str, string str2, string strPath)
        {
            if (File.Exists(strPath + Constants.FILE_PATH + "import_stylesheet.txt"))
            {
                str = str.Replace("\"", "");

                string[] parts1 = str.Split(new[] { Constants.SEP_COMMA }, StringSplitOptions.RemoveEmptyEntries);

                string[] parts2 = str2.Split(new[] { Constants.SEP_COMMA, Constants.SEP_SPACE }, StringSplitOptions.RemoveEmptyEntries);

                string Concated = null;
             
                for (int i = 0; i < parts1.Length && i < parts2.Length; i++)
                {
                    string[] lines = File.ReadAllLines(strPath + Constants.FILE_PATH + "import_stylesheet.txt");

                    string text = File.ReadAllText(strPath + Constants.FILE_PATH + "import_stylesheet.txt", Encoding.UTF8);

                    Concated = string.Concat(parts1[i], ",", " ", parts2[i] + Constants.FILE_EXT_XML);

                    if (text.Contains(Concated))
                    {
                        
                    }
                    else if(text.Contains(parts1[i]))
                    {
                        if(text.Contains(Concated) == false)
                        {
                            foreach(var line in lines)
                            {
                                 if (line.Contains(parts1[i]) == true && (line.Contains(Concated) == false))
                               
                                 {
                                    File.WriteAllText(strPath + Constants.FILE_PATH + "import_stylesheet.txt", text.Replace(line, Concated));
                                    break;
                                 }
                            }          
                        }
                    }
                    else if(text.Contains(Concated) == false)
                    {
                        StreamWriter writer = new StreamWriter(strPath + Constants.FILE_PATH + "import_stylesheet.txt", true);
                        writer.Write(Concated + Environment.NewLine);
                        writer.Close();
                    }

                }
            }

            else
            {
                str = str.Replace("\"", "");

                string[] part1 = str.Split(new[] { Constants.SEP_COMMA }, StringSplitOptions.RemoveEmptyEntries);

                string[] part2 = str2.Split(new[] { Constants.SEP_COMMA }, StringSplitOptions.RemoveEmptyEntries);

                StreamWriter writer = new StreamWriter(strPath + Constants.FILE_PATH + "import_stylesheet.txt", true);

                for (int i = 0; i < part1.Length && i < part2.Length; i++)
                {
                    string Final = string.Concat(part1[i], ",", " ", part2[i] + Constants.FILE_EXT_XML);

                    writer.Write(Final + Environment.NewLine);

                }

                writer.Close();
            }
        }

    }
}

               
           