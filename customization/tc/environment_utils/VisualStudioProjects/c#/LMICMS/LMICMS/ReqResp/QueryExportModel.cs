﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: ExportModel.cs

Description: This file contains operations related to Query Export.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;

namespace LMICMS.ReqResp
{
    public class QueryExportModel
    {
        public QueryExportModel()
        {
            queries = string.Empty;

            exportedqueries = string.Empty;
        }

        public string  queries { get; set; }
        public string exportedqueries { get; set; }
    }
}


