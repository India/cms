﻿/*======================================================================================================================================================================
                                Copyright 2020  LMtec India
                                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: GuidToEnabledConverter.cs

Description: This file contains operations related to GuidToEnabledConverter.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Globalization;
using System.Windows.Data;

namespace LMICMS.Common.ValueConverters
{
    public class GuidToEnabledConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !String.IsNullOrEmpty(System.Convert.ToString(value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
