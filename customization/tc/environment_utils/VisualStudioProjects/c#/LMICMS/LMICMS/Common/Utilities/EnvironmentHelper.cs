﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: EnvironmentHelper.cs

Description: This file contains operations related to EnvironmentVariabes.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;


namespace LMICMS.Common.Utilities
{   
    public class EnvironmentHelper
    {   
        /* This function gets the Environment variables */
        public static string GetEnvironmentVariable(string strVarName)
        {
            string strEnvValue = string.Empty; 
            try
            {
                strEnvValue = Environment.GetEnvironmentVariable(strVarName, EnvironmentVariableTarget.User);
                if (String.IsNullOrEmpty(strEnvValue))
                {
                    strEnvValue = Environment.GetEnvironmentVariable(strVarName, EnvironmentVariableTarget.Machine);
                }
                
            }
            catch (Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
            return strEnvValue;
        }
    }
}
