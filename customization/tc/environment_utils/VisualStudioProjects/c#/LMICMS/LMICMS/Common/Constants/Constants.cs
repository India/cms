﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Constants.cs

Description: This file contains Constant Variables.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
namespace LMICMS.Common.Constants
{
    public class Constants
    {
        public const int REQ_CANCELLED                  = 999;
        public const int REQ_EMPTY                      = 1000;
        public const int REQ_IMPORT                     = 1001;
        public const int REQ_EXPORT                     = 1002;
        public const int REQ_QUERYNAME                  = 1003;
        public const int REQ_WORKFLOWNAME               = 1004;
        public const int REQ_WORKFLOWEXPORT             = 1005;
        public const int REQ_WORKFLOWIMPORT             = 1006;
        public const int REQ_TRANSFERMODENAME           = 1007;
        public const int REQ_TRANSFERMODEEXPORT         = 1008;
        public const int REQ_TRANSFERMODEIMPORT         = 1009;
        public const int REQ_ACLEXPORT                  = 1010;
        public const int REQ_ACLIMPORT                  = 1011;
        public const int REQ_STYLESHEETNAME             = 1012;
        public const int REQ_STYLESHEETEXPORT           = 1013;
        public const int REQ_STYLESHEETIMPORT           = 1014;
        public const int REQ_ORGANISATIONEXPORT         = 1015;
        public const int REQ_ORGANISATIONIMPORT         = 1016;
        public const int REQ_ORGANISATIONCLEAN          = 1017;
        public const int REQ_PFFNAME                    = 1018;
        public const int REQ_PFFEXPORT                  = 1019;
        public const int REQ_PFFIMPORT                  = 1020;
        public const string EXP_QUERY                   = "export_query";
        public const string IMP_QUERY                   = "import_query";
        public const string QUERY_NAME                  = "query_name";
        public const string EXPORT_QUERY_LOG            = "query_export_log";
        public const string IMPORT_QUERY_LOG            = "query_import_log";
        public const string EXP_WORKFLOW                = "export_workflow";
        public const string IMP_WORKFLOW                = "import_workflow";
        public const string WORKFLOW_NAME               = "workflow_name";
        public const string EXPORT_WORKFLOW_LOG         = "workflow_export_log";
        public const string IMPORT_WORKFLOW_LOG         = "workflow_import_log";
        public const string EXP_TRANSFER_MODE           = "export_transfer_mode";
        public const string IMP_TRANSFER_MODE           = "import_transfer_mode";
        public const string TRANSFER_MODE_NAME          = "transfer_mode_name";
        public const string EXPORT_TRANSFER_MODE_LOG    = "transfer_mode_export_log";
        public const string IMPORT_TRANSFER_MODE_LOG    = "transfer_mode_import_log";
        public const string EXP_ACL                     = "export_acl";
        public const string IMP_ACL                     = "import_acl";
        public const string ACL_NAME                    = "acl_name";
        public const string EXPORT_ACL_LOG              = "acl_export_log";
        public const string IMPORT_ACL_LOG              = "acl_import_log";
        public const string EXP_STYLESHEET              = "export_stylesheet";
        public const string IMP_STYLESHEET              = "import_stylesheet";
        public const string STYLESHEET_NAME             = "stylesheet_name";
        public const string PFF_NAME                    = "pff_name";
        public const string EXP_PFF                     = "export_pff";
        public const string IMP_PFF                     = "import_pff";
        public const string EXPORT_PFF_LOG              = "pff_export_log";
        public const string IMPORT_PFF_LOG              = "pff_import_log";
        public const string EXPORT_STYLESHEET_LOG       = "stylesheet_export_log";
        public const string IMPORT_STYLESHEET_LOG       = "stylesheet_import_log";
        public const string STYLESHEET_EXPORT_LOG       = "stylesheet_export_log.txt";
        public const string EXP_ORGANISATION            = "export_organisation";
        public const string IMP_ORGANISATION            = "import_organisation";
        public const string ORGANISATION_NAME           = "organisation_name";
        public const string EXPORT_ORGANISATION_LOG     = "organisation_export_log";
        public const string IMPORT_ORGANISATION_LOG     = "organisation_import_log";
        public const string TC_ORGANISATION             = "tc_organisaton.xml";
        public const string TC_RULE_TREE                = "tc_rule_tree";
        public const string TIME_STAMP                  = "_HH-mm-ss";
        public const string DATE_TIME_STAMP             = "dd-MM-yyyy--HH-mm-ss";
        public const string FILE_EXT_CMD                = ".cmd"; 
        public const string FILE_EXT_TXT                =  ".txt"; 
        public const string FILE_EXT_XML                = ".xml";
        public const string FILE_EXT_ALL_XML            = "*.xml";
        public const string CMD_PROMPT                  = "cmd.exe";
        public const string EXP_FILE_NAME               = "ExportQuery.txt";
        public const string EXP_FILE_NAME_STYLESHEET    = "ExportStylesheet.txt";
        public const string EXECUTE_LMICMSUTILS         = "cd" + " ";
        public const string FILE_PATH                   = "\\";
        public const string DOUBLE_QUOTES               = "\"";
        public const string SEP_COMMA                   = ",";
        public const string SEP_DASH                    = " -";
        public const string SEP_UNDERSCORE              = "_";
        public const string SEP_SPACE                   = " ";
        public const string SEP_CARET                   = "^";
        public const string SEP_DOT                     = "...";
        public const string SEP_SING_QUOTE              = "'";
        public const string SEP_NULL                    = "";
        public const string SEP_BRACES                  = "(s)";
        public const string SEP_DOLLAR                  = "$";
        public const string SEP_ASTERISK                = "\\*";
        public const string SEP_DOT_ASTERISK            = ".*";
        public const string ASTERISK                    = "*";
        public const string SEP_NEWLINE                 = "\n";
        public const string ERR_INVALIDCLICK            = "Double click Not Allowed..Use single click";
        public const string ERR_INVALIDPATH             = "Not a Valid Path set in Export Location";
        public const string ERR_FILENOTEXISTS           = "No File Exists Or File Not Created";
        public const string OPERATION_COMPLETED         = "Operation Completed Successfully";
        public const string OPERATION_FAILED            = "Operation failed";
        public const string HEADER_WARNING              = "Warning";
        public const string MSG_LABEL                   = "Operation Completed - Check log File";
        public const string HEADER_INFO                 = "Information";
        public const string ERR_ERROR                   = "Error";
        public const string ERR_FILENAMEEMPTY           = "No value in File name - Export File name/Import File name/Input Location";
        public const string ERR_WFNAMEEMPTY             = "No Workflow Selected";
        public const string ERR_QRYNAMEEMPTY            = "No Query Selected";
        public const string ERR_PFFNAMEEMPTY            = "No PFF Selected";
        public const string ERR_STYLESHEETNAMEEMPTY     = "No Stylesheet Selected";
        public const string ERR_TRANSFMODENAMESELECTED  = "No TransferMode Selected";
        public const string ERR_PFFNAMESELECTED         = "No PFF Selected";
        public const string NEW_LINE_SEP                = "------------------------------------------------------------------";
        public const string EXPORT                      = "Export";
        public const string IMPORT                      = "Import";
        public const char  NEW_LINE_ONE                 = '\r';
        public const char NEW_LINE_TWO                  = '\n';
        public const char COMMA_SEP                     =  ',';
        public const string EXPORTER_LOG                = "_exporter.log";
        public const string TRANSFER_MODE_EXPORT_LOG    = "_transfermode_exportlog.txt";
        public const string TRANSFER_MODE_IMPORT_LOG    = "transfermode_importlog.txt";
        public const string ERROR                       = "ERROR";
        public const string UNUSED_STYLESHEET_NAME      = "Name of unused stylesheets :";
        public const string NO_UNUSED_STYLESHEET        = "!!!! No unused stylesheets found !!!!";
        public const string FAILED_OBJECTS              = "_failed_objects.xml";
        public const string VALIDATION_OUT              = "_validation.out";
        public const string  IMPORTER_LOG               = "__importer.log";
        public const string ACTIVE_IMAGE                = "active_img";
        public const string ALL_EXT_LOG                 = "*.log";
        public const string CHECKED                     = "Checked";
        public const string UNCHECKED                   = "Unchecked";
        public const string PFF_SCHEMA                  =  "C:\\Users\\Administrator\\Documents\\Test_Pff.xml";

        /*  Operation Type */
        
        public static string OP_CLEAN                   = "Clean";

        /*  Message Box Message strings */
        public static string INP_PARAMETER              = "Please Provide all Input Parameters";
        public static string VALID_OP                   = "Please select valid Operation";
        public static string FILE_ALREADY_EXISTS        = "File with same name already exists in output File path Directory. \n Do you want to overwrite it ?";
        public static string SELECT_OPERTAION_TYPE      = "Please Select Operation Type.";

        /*  Requests Event Types */
      
        public const int REQ_CLEAN_AND_EXPORT           = 1004;

        /*  XML File Tag constants */
        public static string TAG_USER_DATA              = "/xmln:PLMXML/xmln:UserData";
        public static string TAG_PERSONS                = "/xmln:PLMXML/xmln:Person";
        public static string TAG_ORG_MEM                = "/xmln:PLMXML/xmln:OrganisationMember";
        public static string TAG_USER                   = "/xmln:PLMXML/xmln:User";
        public static string TAG_HEADER                 = "/xmln:PLMXML/xmln:Header";
        public static string TAG_PLMXML                 = "/xmln:PLMXML";

        /*  XML Attributes Constants */
        public static string ATTR_TRAVERSE_ROOT_REFS    = "traverseRootRefs";
        public static string ATTR_TRANSFER_CONTEXT      = "transferContext";
        public static string ATTR_TIME                  = "time";
        public static string ATTR_SCHEMA_VERSION        = "schemaVersion";
        public static string ATTR_AUTHOR                = "author";
        public static string ATTR_DATE                  = "date";

        /*  Output File name */
        public static string OUT_FILE_NAME              = "tc_organization_";
        public static string LOG_FILE_NAME              = "\\Log_";
        public static string OUT_IMP_CLEAN_FNAME        = "\\tc_organization_new.xml";

        /* XML Name space */
        public static string XML_NS                     = "http://www.plmxml.org/Schemas/PLMXMLSchema";
        public static string XML_NS_NAME                = "xmln";

        /*  File Extensions */
        public static string FEX_XML                    = ".xml";
        public static string FEX_LOG                    = ".log";
        public static string FEX_TXT                    = ".txt";

        /*  Logger Constants    */
        public static int LOG_VERBOSE                   = 0;
        public static int LOG_DEBUG                     = 1;
        public static int LOG_INFO                      = 2;
        public static int LOG_WARNING                   = 3;
        public static int LOG_ERROR                     = 4;

        /*  Separators */
        public static string SEP_BACK_SLASH             = "//";

        /* Apps */
        public const string Workflow                    = "Workflow";
        public const string Authorization               = "Authorization";
        public const string Organization                = "Organization";
        public const string ACL                         = "ACL";
        public const string PFF                         = "PFF";
        public const string Stylesheet                  = "Stylesheet";
        public const string Preference                  = "Preference";
        public const string Query                       = "Query";
        public const string TransferMode                = "TransferMode";

        public const string Settings                    = "Settings";
        public const string Home                        = "Home";
        public const string WorkflowViewer              = "WorkflowViewer";

        /* Operations */
        public const string Export                      = "Export";
        public const string Import                      = "Import";
        public const string ExpImp                      = "Export/Import";
        public const string Clean                       = "Clean";

        /*Settings*/
        public const string Common                      = "Common";
        public const string Project                     = "Project";
        public const string Users                       = "Users";
        public const string TCConfiguration             = "TC Configuration";
        public const string SVNLocation                 = "SVN Location";    
    }
}

