﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: ConfigConstants.cs

Description: This file contains operations related to ConfigConstants

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
namespace LMICMS.Common.Constants
{
    public static class ConfigConstants
    {
        public const string TagConfigurationRoot        = "LMI_CMS";
        public const string ElementActiveProjectGuid    = "ACTIVE_PROJECT_GUID";
        public const string ElementTempDownloadPath     = "LMI_TEMP_DOWNLOAD_PATH";
        public const string ElementCMSExeLoc            = "LMI_CMS_EXE_LOC";
        public const string ElementConfigFileLoc        = "LMI_CONFIG_FILE_LOC";

        public const string TagProjectsDetails          = "PROJECTS_DETAILS";
        public const string TagProject                  = "PROJECT";
        public const string AttributeProjectGuid        = "PROJECT_GUID";
        public const string AttributeProjectName        = "PROJECT_NAME";
        public const string ElementActiveConfigGuid     = "ACTIVE_CONFIG_GUID";
        public const string ElementActiveRepoGuid       = "ACTIVE_REPO_GUID";

        public const string TagTCConfigurations         = "TC_CONFIGURATIONS";
        public const string TagTCConfiguration          = "TC_CONFIGURATION";
        public const string AttributeConfigGuid         = "CONFIG_GUID";
        public const string AttributeConfigName         = "CONFIG_NAME";
        public const string ElementTCRoot               = "TC_ROOT";
        public const string ElementTCData               = "TC_DATA";
        public const string ElementActiveUserGuid       = "LMI_ACTIVE_USER_GUID";

        public const string TagUsersDetails             = "TC_USERS_DETAILS";
        public const string TagUser                     = "TC_USER";
        public const string AttributeUserGuid           = "USER_GUID";
        public const string AttributeUserId             = "USER_ID";
        public const string AttributeUserPassword       = "USER_PASSWORD";
        public const string AttributeUserGroup          = "USER_GROUP";
        public const string AttributeUserRole           = "USER_ROLE";

        public const string TagSrcControlLoc            = "SRC_CONTROL_LOC";     
        public const string ElementRepo                 = "REPO";
        public const string AttributeRepoGuid           = "REPO_GUID";
        public const string AttributeRepoName           = "REPO_NAME";
        public const string AttributeRepoPath           = "REPO_PATH";
        public const string ElementAppData              = "APP_DATA";
        public const string AttributeAppDataName        = "APP_DATA_NAME";
        public const string AttributeAppDataPath        = "APP_DATA_PATH";

        public const string ConfigFileName              = "ConfigFileName";
        public const string EnvTCRoot                   = "EnvTCRoot";
        public const string EnvTCData                   = "EnvTCData";
        public const string DefaultProjectName          = "DefaultProjectName";
        public const string DefaultConfigName           = "DefaultConfigName";
        public const string DefaultRepoName             = "DefaultRepoName";
        public const string DefaultFontFamily           = "DefaultFontFamily";
        public const string LMI_CMS_LOG_LEVEL           = "EnvLogLevel";

        public const string SET_TC_ROOT                 = "SET TC_ROOT=";
        public const string SET_TC_DATA                 = "SET TC_DATA=";
        public const string TC_DATA_TC_PROFILEVARS      = "%TC_DATA%\\tc_profilevars";
        public const string SET_ECHO_OFF                = "@echo off";
        public const string TEXT_DISP                   = "echo Type in password of ";
        public const string SET_DBAPASS                 = "set /P DBAPASS=";
        public const string SET_DBAPASS_TMP             = "set DBAPASS_TMP=";


        #region Messages

        public const string UserUC_UserAdded            = "Message.UserUC.UserAdded";
        public const string UserUC_UserUpdated          = "Message.UserUC.UserUpdated";
        public const string UserUC_AllFieldsMandatory   = "Message.UserUC.AllFieldsMandatory";
        public const string UserUC_UserDeleted          = "Message.UserUC.UserDeleted";

        public const string ProjectUC_ProjectAdded      = "Message.ProjectUC.ProjectAdded";
        public const string ProjectUC_ProjectUpdated    = "Message.ProjectUC.ProjectUpdated";
        public const string ProjectUC_AllFieldsMandatory = "Message.ProjectUC.AllFieldsMandatory";
        public const string ProjectUC_ProjectDeleted    = "Message.ProjectUC.ProjectDeleted";
        public const string ProjectUC_ProjectActivated  = "Message.ProjectUC.ProjectActivated";

        #endregion

        public static string ActiveProjectGuid          = string.Empty;
        public static string ActiveProjectName          = string.Empty;
        public static string ActiveConfigGuid           = string.Empty;
        public static string ActiveRepoPathGuid         = string.Empty;
        public static string ActiveRepoPath             = string.Empty;
        public static string TCRoot                     = string.Empty;
        public static string TCData                     = string.Empty;
        public static string LMITempDownloadPath        = string.Empty;
        public static string LMICMSExeLoc               = string.Empty;
        public static string UserGuid                   = string.Empty;
        public static string UserName                   = string.Empty;
        public static string UserPassword               = string.Empty;
        public static string UserGroup                  = string.Empty;
        public static string UserRole                   = string.Empty;

        public static string WorkflowPath               = string.Empty;
        public static string AuthorizationPath          = string.Empty;
        public static string OrganizationPath           = string.Empty;
        public static string ACLPath                    = string.Empty;
        public static string PFFPath                    = string.Empty;
        public static string StylesheetPath             = string.Empty;
        public static string PreferencePath             = string.Empty;
        public static string QueryPath                  = string.Empty;
        public static string TransferModePath           = string.Empty;
    }
}
