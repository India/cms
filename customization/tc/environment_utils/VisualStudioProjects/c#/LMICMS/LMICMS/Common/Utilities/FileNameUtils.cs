﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: FileNameUtils.cs

Description: This file contains operations related to file creation.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;


namespace LMICMS.Common.Utilities
{
    class FileNameUtils
    {
        DateTimeUtilities objDtimeUtil = new DateTimeUtilities();

        /*  This Function creates a file name with Unix time stamp appended at end, If Prefix is not provided then file name will be current Timestamp,
         *  This function also appends File extension at end of file name if no file extension is provided default file extension is .txt 
         */
        public string constructGenericFileNames(string strPrefix, string strFileExtension)
        {
            int iEpochTime = -1;
            string strOutFileName = string.Empty;
            try
            {

                /*  Getting current epoch time */
                iEpochTime = objDtimeUtil.getEpochTime();

                if (strPrefix != null && strPrefix.Length > 0)
                {
                    if (strFileExtension != null && strFileExtension.Length > 0)
                    {
                        strOutFileName = strPrefix + iEpochTime + strFileExtension;
                    }
                    else
                    {
                        /*Default File extension is .txt */
                        strOutFileName = strPrefix + iEpochTime + Constants.Constants.FEX_TXT;
                    }
                }
                else
                {
                    /*  If prefix is not provided then creating file name with current timestamp */
                    if (strFileExtension != null && strFileExtension.Length > 0)
                    {
                        strOutFileName = iEpochTime + strFileExtension;
                    }
                    else
                    {
                        /*Default File extension is .txt */
                        strOutFileName = iEpochTime + Constants.Constants.FEX_TXT;
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }

            return strOutFileName;
        }
    }
}
