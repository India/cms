﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: ResourceHelper.cs

Description: This file contains operations related to ResourceHelper 

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Windows;

namespace LMICMS.Common.Utilities
{
    public class ResourceHelper
    {
        public static string FindResource(string key)
        {
            string strFindResource = string.Empty;
            try
            {
                strFindResource = Convert.ToString(Application.Current.TryFindResource(key));
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }

            return strFindResource;
        }
    }
}
