﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: DateTimeUtilities.cs

Description: This file contains  operations related to DateTime.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;

namespace LMICMS.Common.Utilities
{
    class DateTimeUtilities
    {
        /*  This Function return current epoch time, Epoch time is current time in seconds from 1st January 1970 */
        public int getEpochTime()
        {
            int iEpochTime = -1;
            try
            {
                /*  Generating current epoch time */
                iEpochTime = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }

            return iEpochTime;
        }
    }
}
