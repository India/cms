﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: LMIConfigHelper.cs

Description: This file contains operations related to LMIConfigHelper .

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using LMICMS.Common.Constants;
using System;
using System.Configuration;
using System.IO;
using System.Xml;

namespace LMICMS.Common.Utilities
{
    public class LMIConfigHelper
    {
        public static string GetDefaultConfigFilePath()
        {
            string strDefaultConfigFilePath = "";
            try
            { 
                strDefaultConfigFilePath = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings[ConfigConstants.ConfigFileName];
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
            
            return strDefaultConfigFilePath;
        }

        public static string GetConfigFilePath()
        {
            string strConfigFilePath = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings[ConfigConstants.ConfigFileName];
            try
            {
                if (File.Exists(strConfigFilePath))
                {
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(strConfigFilePath);

                    string strLocElePath = String.Format(@"//{0}/{1}", ConfigConstants.TagConfigurationRoot, ConfigConstants.ElementConfigFileLoc);
                    var configFieLoc = xmlDoc.SelectSingleNode(strLocElePath);
                    if (configFieLoc != null && !String.IsNullOrEmpty(configFieLoc.InnerText))
                    {
                        strConfigFilePath = configFieLoc.InnerText;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
            return strConfigFilePath;
        }

        public static string GetValue(string key)
        {
            string strGetValue = "";
            try
            {
                strGetValue = Convert.ToString(ConfigurationManager.AppSettings[key]);
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
            return strGetValue;
        }
    }
}
