﻿/*======================================================================================================================================================================
                                           Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: Logger.cs

Description: This File is specific to logging related activities.

========================================================================================================================================================================

Date               Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-August-2020    Mohit Singh          Initial Release
========================================================================================================================================================================*/

/*  Note:- This is custom class that is can be used for write logs.
 *         Predefined thing to use this class:-
 *         Logger Init is one time process and should be done only at start of pluggin and log level should be defined in it.
 *         For Windows platform it creates Log File named as EALogfile.txt in %temp% location.
 *         No log rotation technique has been implemented in this class every time it creates a new log file.
 *         No Constraint for size of log file has been implemented.
 **/

using System;
using System.IO;
using LMICMS.Services;

namespace LMICMS.Common.Utilities
{
    class Logger
    {
        static int iDefaultLogLevel = Constants.Constants.LOG_ERROR;
        static string strLogFilePath = string.Empty;

        /*  Diffrent Log Levels */
        int VERBOSE = 0;
        int DEBUG = 1;
        int INFO = 2;
        int WARNING = 3;
        int ERROR = 4;
        static Logger log = null;

        /*  Default Constructor */
        public Logger()
        {
        }

        /*  This function gives static instance of logger   */
        public static Logger getInstance()
        {
            if (log == null)
            {
                log = new Logger();
            }
            return log;
        }

        /*  This function writes basic details of system initially at start of Log File */
        public void init(int iLogLevel, string strPath)
        {
            iDefaultLogLevel = iLogLevel;

            strLogFilePath = strPath;
        }

        /*  Function that creates log file and writes log to it */
        public void LogWrite(string logMessage)
        {
            try
            {
                using (StreamWriter w = File.AppendText(strLogFilePath))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception exception)
            {
              
            }
        }

        /*  Function writes log in log file */
        public void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("\t\t" + logMessage);
            }
            catch (Exception exception)
            {
                
            }
        }

        /*  For writing Verbose log   */
        public void verbose(String szLog)
        {
            if (szLog == null)
                return;
            if (iDefaultLogLevel <= VERBOSE)
            {
                try
                {
                    LogWrite("Verbose  : " + szLog);
                }
                catch (Exception exception)
                {
                    
                }
            }
        }

        /*  For writing debug log   */
        public void debug(String szLog)
        {
            if (szLog == null)
                return;
            if (iDefaultLogLevel <= DEBUG)
            {
                try
                {
                    LogWrite("Debug    : " + szLog);
                }
                catch (Exception exception)
                {
                   
                }
            }
        }

        /*  For writing info log   */
        public void info(String szLog)
        {
            if (szLog == null)
                return;
            if (iDefaultLogLevel <= VERBOSE)
            {
                try
                {
                    LogWrite("Info     : " + szLog);
                }
                catch (Exception exception)
                {
                 
                }
            }
        }

        /*  For writing warning log   */
        public void warning(String szLog)
        {
            if (szLog == null)
                return;
            if (iDefaultLogLevel <= VERBOSE)
            {
                try
                {
                    LogWrite("Warning  : " + szLog);

                }
                catch (Exception exception)
                {
                  
                }
            }
        }

        /*  For writing warning log   */
        public void error(String szLog)
        {
            if (szLog == null)
                return;
            if (iDefaultLogLevel >= VERBOSE)
            {
                try
                {
                    LogWrite("Error    : " + szLog);
                }
                catch (Exception exception)
                {
                   
                }
            }
        }
    }
}
