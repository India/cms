﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: FileHelper.cs

Description: This file contains various operations related to FileHelper.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Text;
using System.IO;

namespace LMICMS.Common.Utilities
{
    public class FileHelper
    {
        public static bool SaveFile(string strFilePath, string strFileData) 
        {
            bool flag = false;

            try
            {
                File.WriteAllBytes(strFilePath, Encoding.UTF8.GetBytes(strFileData));
                flag = true;
            }
            catch (Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }

            return flag;
        }

        public static bool IsTextFileEmpty(string fileName)
        {
            try
            {
                var info = new FileInfo(fileName);
                if (info.Length == 0)
                    return true;

                if (info.Length < 6)
                {
                    var content = File.ReadAllText(fileName);
                    return content.Length == 0;
                }
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
            return false;
        }

        public  bool Check_File_Exists(string strFileName)
        {
            Boolean bIsFileAvailable = false;
            try
            {
                if (string.IsNullOrEmpty(strFileName) == true)
                {
                    bIsFileAvailable = false;
                }
                else
                {
                    bIsFileAvailable = true;
                }
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
          
            return bIsFileAvailable;
        }
    }
}
