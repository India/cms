﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: XmlHelper.cs

Description: This file contains operations related to XmlHelper 

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace LMICMS.Common.Utilities
{
    public class XmlHelper
    {
        public static T DeserializeXMLFileToObject<T>(string xmlFilePath)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(xmlFilePath)) return default(T);

            try
            {
                StreamReader xmlStream = new StreamReader(xmlFilePath);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch (Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
            return returnObject;
        }

        public static T DeserializeXMLNodeToObject<T>(XmlNode xmlNode)
        {
            T returnObject = default(T);
            if (xmlNode == null) return returnObject;

            try
            {
                XmlSerializer serial = new XmlSerializer(typeof(T));
                using (XmlNodeReader xmlNodeReader = new XmlNodeReader(xmlNode))
                {
                    returnObject = (T)serial.Deserialize(xmlNodeReader);
                }
            }
            catch (Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
            return returnObject;
        }
    }
}
