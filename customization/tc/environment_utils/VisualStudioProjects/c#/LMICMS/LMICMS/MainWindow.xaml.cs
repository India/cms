﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: MainWindow.xaml.cs

Description: This file is the entry point for the whole Application

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Dialogs;
using LMICMS.UserControls;

namespace LMICMS
{ 
    public partial class MainWindow : LMIWindow
    {
        private Logger logger = Logger.getInstance();

        public static MainWindow _mainWindow;

        Dictionary<string,UserControl> userControls;

        /* This is the Starting point of Application */
        public MainWindow()
        {
            InitializeComponent();
            _mainWindow = this;
            userControls = new Dictionary<string, UserControl>();
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight; 
            this.lblActiveProject.Content = ConfigConstants.ActiveProjectName;
  
            userControls.Add(typeof(HomeUC).ToString(), new HomeUC());
            userControls.Add(typeof(SettingsUC).ToString(), new SettingsUC());
            userControls.Add(typeof(QueryUC).ToString(), new QueryUC());
            userControls.Add(typeof(WorkflowUC).ToString(), new WorkflowUC());
            userControls.Add(typeof(TransferModeUC).ToString(), new TransferModeUC());
            userControls.Add(typeof(ACLUC).ToString(), new ACLUC());
            userControls.Add(typeof(AuthorizationUC).ToString(), new AuthorizationUC());
            userControls.Add(typeof(StylesheetUC).ToString(), new StylesheetUC());
            userControls.Add(typeof(OrganizationUC).ToString(), new OrganizationUC());
            userControls.Add(typeof(PFFUC).ToString(), new PFFUC());
        }

        /* This function is the callback function which gets invoked when user clicks on shutdown button */ 
        private void ShutDownClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /*This function is the call back function which gets invoked when user clicks on maximize button */
        private void FullscreenClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.WindowState == System.Windows.WindowState.Normal)
                {
                    this.WindowState = System.Windows.WindowState.Maximized;
                }
                else if (this.WindowState == System.Windows.WindowState.Maximized)
                {
                    this.WindowState = System.Windows.WindowState.Normal;
                }
            }
            catch(Exception ex)
            {
                logger.error(ex.Message);
            }
        }

        /* This function is the call back function which gets invoked  when user clicks on minimize button */
        private void MinimizeClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.WindowState != System.Windows.WindowState.Minimized)
                {
                    this.WindowState = System.Windows.WindowState.Minimized;
                }
                else
                {
                    this.WindowState = System.Windows.WindowState.Normal;
                }
            }
            catch(Exception ex)
            {
                logger.error(ex.Message);
            }
        }

        /*This function gets invoked when user clicks on different application buttons for various cases */
        private void ChangeUserControl(object sender, RoutedEventArgs e)
        {
            try
            {
                var button = sender as Button;

                switch (button.CommandParameter.ToString())
                {
                    case Constants.Settings:
                        ccAppContainer.Content = userControls[typeof(SettingsUC).ToString()];
                        break;
                    case Constants.Home:
                        ccAppContainer.Content = userControls[typeof(HomeUC).ToString()];
                        break;
                    case Constants.Query:
                        ccAppContainer.Content = userControls[typeof(QueryUC).ToString()];
                        break;
                    case Constants.WorkflowViewer:
                        ccAppContainer.Content = userControls[typeof(WorkflowUC).ToString()];
                        break;
                    case Constants.TransferMode:
                        ccAppContainer.Content = userControls[typeof(TransferModeUC).ToString()];
                        break;
                    case Constants.ACL:
                        ccAppContainer.Content = userControls[typeof(ACLUC).ToString()];
                        break;
                    case Constants.Authorization:
                        ccAppContainer.Content = userControls[typeof(AuthorizationUC).ToString()];
                        break;
                    case Constants.Stylesheet:
                        ccAppContainer.Content = userControls[typeof(StylesheetUC).ToString()];
                        break;
                    case Constants.Organization:
                        ccAppContainer.Content = userControls[typeof(OrganizationUC).ToString()];
                        break;
                    case Constants.PFF:
                        ccAppContainer.Content = userControls[typeof(PFFUC).ToString()];
                        break;
                }
            }
            catch(Exception ex)
            {
                logger.error(ex.Message);
            }
        }
    }
}
