﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: TransferModeImportService.cs

Description: This file contains operations related to TransferModeImport.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Services
{
    class TransferModeImportService
    {
        public string strOutput = string.Empty;
        public static string strAppendedOutput = string.Empty;
        Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();
        private Logger logger = Logger.getInstance();

        /*This function gets invoked to fulfill Import Operation Request to import TransferModes to a set location */
        public void TransferModeProcessImport(TransferModeImportModel transferModeImportModel)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* To create .cmd file with Date Time stamp for TransferMode Import operation */
                string strFileName = Path.Combine(strTempDir, Constants.IMP_TRANSFER_MODE + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create logfile at Tempfolder location for the import operation */
                string strLogFile = Path.Combine(strTempDir, Constants.IMPORT_TRANSFER_MODE_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                Boolean bIsFirstQuery = true;

                foreach (var item in transferModeImportModel.transfermode)
                {
                    /* Constructing tc  cmd line Arguments for TransferMode import operation  */
                    string strArguments = "tcxml_import -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -file=" + Constants.DOUBLE_QUOTES + transferModeImportModel.strFilePath + Constants.FILE_PATH + item + Constants.DOUBLE_QUOTES + " -scope_rules_mode=overwrite";

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    strAppendedOutput = strAppendedOutput + strOutput;

                    if (bIsFirstQuery == true)
                    {
                        bIsFirstQuery = false;

                        /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Import operation */
                        Execute_File_Write.Process_Execute_File_Write(strFileName);
                    }

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Import Operation and string LogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                    /* Deletes the specified file */
                    File.Delete(ConfigConstants.TransferModePath + Constants.FILE_PATH + item + Constants.FAILED_OBJECTS);

                    /* Deletes the specified file */
                    File.Delete(ConfigConstants.TransferModePath + Constants.FILE_PATH + item + Constants.VALIDATION_OUT);

                    /* Moves the specified file to a new location with a new name */
                    File.Move(ConfigConstants.TransferModePath + Constants.FILE_PATH + item + Constants.IMPORTER_LOG, strTempDir + Constants.FILE_PATH + item + Constants.TRANSFER_MODE_IMPORT_LOG);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.ToString());
            }           
        }
    }
}
