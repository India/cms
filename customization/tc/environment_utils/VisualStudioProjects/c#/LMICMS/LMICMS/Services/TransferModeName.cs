﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: TransferModeName.cs

Description: This file contains operations related to TransferMode Names

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
    class TransferModeName
    {
        string strOutput = string.Empty;
        private Logger logger = Logger.getInstance();
        Execute_cmsutils_exe execute_Cmsutils_Exe = new Execute_cmsutils_exe();

        /* This function gets invoked to fulfill Refresh Operation Request to fetch Stylesheetnames from TC */
        public void ProcessTransferModeName(string strFileName)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* Constructing tc cmd line Arguments for LMICMSUtils exe for TransferMode refresh operation */
                string strArguments = "LMICMSUTILS -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -mode=transfer_mode -operation=export_transfer_mode_name -tcconfigfilename=" + strFileName + " -filepath=" + strTempDir;

                /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to LMICMSUtils exe */
                execute_Cmsutils_Exe.Process_Execute_cmsutils_exe(strArguments, strOutput);
            }
            catch (Exception ex)
            { 
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}
