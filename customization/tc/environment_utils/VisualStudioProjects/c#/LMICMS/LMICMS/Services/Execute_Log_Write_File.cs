﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Execute_Log_Write_File.cs

Description: This file contains operations related to file Log File write.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
    public class Execute_Log_Write_File
    {
        public static void Process_Execute_Log_Write_File(string strOutput, string strLogFile)
        {
            try
            {
                StreamWriter writer = new StreamWriter(strLogFile, true);

                writer.WriteLine(DateTime.Now.ToString(Constants.DATE_TIME_STAMP));

                writer.Write(strOutput);

                writer.WriteLine(Constants.NEW_LINE_SEP);

                writer.Close();
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message); 
            }
        }
    }
}
