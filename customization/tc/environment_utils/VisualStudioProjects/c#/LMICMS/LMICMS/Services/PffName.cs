﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: PffName.cs

Description: This file contains operations related to Pff Name.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-September-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Modules.Pff;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LMICMS.Services
{
    class PffName
    {
        string strOutput = string.Empty;
        private Logger logger = Logger.getInstance();
       // public static List<PffModel> lPffNames = null;
        Execute_cmsutils_exe execute_Cmsutils_Exe = new Execute_cmsutils_exe();

        /* This function gets invoked to fulfill Refresh Operation Request to fetch Stylesheetnames from TC */
        public void ProcessPffName(string strFileName, string strUserReq)
        {
           // lPffNames = new List<PffModel>();

            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* Constructing tc cmd line Arguments for LMICMSUtils exe for Stylesheetnames refresh operation */
                string strArguments = "LMICMSUTILS -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -mode=pff -operation=export_pff_name -tcconfigfilename=" + strFileName + " -filepath=" + strTempDir + " -search_string=" + strUserReq;

                /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to LMICMSUtils exe */
                execute_Cmsutils_Exe.Process_Execute_cmsutils_exe(strArguments, strOutput);

                ///* Read Stylesheetname file created at Temp Folder location line by line and store it as array filelines */
                //string[] filelines = File.ReadAllLines(strTempDir + Constants.FILE_PATH + strFileName);

                //for (int iDx = 0; iDx < filelines.Length; iDx++)
                //{
                //    /* Initialize a new instance of StringBuilder Class */
                //    StringBuilder stringbuilder = new StringBuilder(filelines[iDx]);

                //    /* Call function Process_Replace_String_Characters of Replace_String_Characters Class and pass stringBuilder to it */
                //    stringbuilder = Replace_String_Characters.Process_Replace_String_Characters(stringbuilder);

                //    /* Add StylesheetNames  and file names to list lStylesheetNames  to populate in datagrid */
                //    lPffNames.Add(new PffModel() { PffName = filelines[iDx], FileName = stringbuilder.ToString() });
                //}
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}

