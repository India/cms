﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: ExportService.cs

Description: This file contains operations related to Query Export.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Services
{
   public class QueryExportService
    {
        string strOutput = string.Empty;
        string strExpFileName = string.Empty;
        string strQueryName = string.Empty;
        private Logger logger = Logger.getInstance(); 
        Execute_cmsutils_exe execute_Cmsutils_Exe = new Execute_cmsutils_exe();

        /*This function gets invoked to fulfill Export Operation Request to export Query to a set location */
        public void ProcessExport(QueryExportModel exportModel, string strPath)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* To create .cmd file with Date Time stamp for Export Query operation */
                string strFileName = Path.Combine(strTempDir, Constants.EXP_QUERY + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create File for storing selected Export QueryNames and UID's */
                string strTCConfigFileName = Constants.EXP_QUERY + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT;

                /* To create logfile at Tempfolder location for the export operation */
                string strLogFile = Path.Combine(strTempDir, Constants.EXPORT_QUERY_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

               if(exportModel.queries!=null && exportModel.queries.Length > 0 && exportModel.exportedqueries!=null && exportModel.exportedqueries.Length > 0)
                {
                    /* Remove Comma from end of the string internalname */
                    strQueryName = exportModel.queries.TrimEnd(Constants.COMMA_SEP);

                    /* Enclose the string Queryname with Double Quotes */
                    strQueryName = Constants.DOUBLE_QUOTES + strQueryName + Constants.DOUBLE_QUOTES;

                    /* Remove Comma from end of the string Filename */
                    strExpFileName = exportModel.exportedqueries.TrimEnd(Constants.COMMA_SEP);

                    /* Constructing tc cmd line Arguments for LMICMSUtils exe for Organisation export operation */
                    string strArguments = "LMICMSUTILS -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -mode=query -operation=export -internalname=" + strQueryName + " -exportfilename=" + strExpFileName + " -tcconfigfilename=" + strTCConfigFileName + " -filepath=" + strTempDir;

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to LMICMSUtils exe */
                    strOutput = execute_Cmsutils_Exe.Process_Execute_cmsutils_exe(strArguments, strOutput);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string LogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                   /* Creating instance of QueryExportServiceFinal Class */
                    QueryExportServiceFinal exportServiceFinal = new QueryExportServiceFinal();

                    /* Calling object of QueryExportServiceFinal class and passing string FileName, string Path, string Logfile, exportModel and String TC ConfigFileName to its function ProcessExportFinal*/
                    exportServiceFinal.ProcessExportFinal(strFileName, strPath, strLogFile, exportModel, strTCConfigFileName);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }     
   }
}

    

