﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: OrganisationExportService.cs

Description: This file contains operations related to OrganisationExport

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
    class OrganisationExportService
    {
        public static string strOutput = string.Empty;
        private Logger logger = Logger.getInstance();
        Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

        /*This function gets invoked to fulfill Export Operation Request to export Organisation to a set location */
        public void OrganisationProcessExport(string strPath, string strOrgName)
        {
            try
            {
                /* Remove any extension name if found in  StrOrgName */
                strOrgName = System.IO.Path.GetFileNameWithoutExtension(strOrgName);

                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* To create .cmd file with Date Time stamp for Export Organisation operation */
                string strFileName = Path.Combine(strTempDir, Constants.EXP_ORGANISATION + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create logfile at Tempfolder location for the export operation */
                string strLogFile = Path.Combine(strTempDir, Constants.EXPORT_ORGANISATION_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                if (String.IsNullOrEmpty(strOrgName))
                {
                    /* File name for the operation consisting of Repository location where Organisation export file has to be exported and File extension */
                    string strOrgFile= strPath + Constants.FILE_PATH + Constants.TC_ORGANISATION;

                    /* Constructing tc  cmd line Arguments for Organisation export operation */
                    string strArguments = "dsa_util -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -class=Group -f=export  -filename=" + strOrgFile;

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Export operation */
                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string LogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                }
                else
                {
                    /* User Input File name for the operation consisting of Repository location where Organisation export file has to be exported and File extension */
                    string strInpOrgFile = strPath + Constants.FILE_PATH + strOrgName + Constants.FILE_EXT_XML;

                    /* Constructin tc  cmd line Arguments for Organisation export operation */
                    string strArguments = "dsa_util -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -class=Group -f=export  -filename=" + strInpOrgFile;

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Export operation */
                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string LogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}

        
    
