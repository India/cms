﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: PffExportService.cs

Description: This file contains operations related to PFF Export.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Dec-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Services
{
    class PffExportService
    {
        public string strOutput = string.Empty;
        public static string strAppendedOutput = string.Empty;
        private Logger logger = Logger.getInstance();  
        Execute_cmsutils_exe execute_Cmsutils_Exe = new Execute_cmsutils_exe();
        string strOldFilePath = Constants.PFF_SCHEMA;
       
        /*This function gets invoked to fulfill Export Operation Request to export Stylsheet to a set location */
        public void PffProcessExport(PffExportModel pffExportModel, string strPath)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* To create .cmd file with Date Time stamp for Export Organisation operation */
                string strFileName = Path.Combine(strTempDir, Constants.EXP_PFF + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create File for storing selected Export StylesheetNames and UID's */
                string strTCConfigFileName = Constants.EXP_PFF + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT;

                /* To create logfile at Tempfolder location for the export operation */
                string strLogFile = Path.Combine(strTempDir, Constants.EXPORT_PFF_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                Boolean bIsFirstQuery = true;

                if (pffExportModel.pff != null && pffExportModel.pff.Length > 0 && pffExportModel.exportedpff != null && pffExportModel.exportedpff.Length > 0)
                {
                    /* Tokenizing on the basis of comma and storing item values in part1 and part2 arrays */
                    string[] strPartOne = pffExportModel.exportedpff.Split(new[] { Constants.COMMA_SEP }, StringSplitOptions.RemoveEmptyEntries);
                    string[] strPartTwo = pffExportModel.pffUID.Split(new[] { Constants.COMMA_SEP }, StringSplitOptions.RemoveEmptyEntries);
                   
                    for (int iDx = 0; iDx < strPartOne.Length && iDx < strPartTwo.Length; iDx++)
                    {
                        /* Storing part2 token value in string Value1 */
                        string strExpFileName = strPartOne[iDx];

                        /* Storing part1 token value in string Value2 */
                        string strPffUid = strPartTwo[iDx];

                        /* Enclosing string strPffUid within Double quotes */
                        strPffUid = Constants.DOUBLE_QUOTES + strPffUid + Constants.DOUBLE_QUOTES;
                       
                        string strNewFilePath = strPath + Constants.FILE_PATH + strExpFileName + Constants.FILE_EXT_XML;

                        File.Copy(strOldFilePath, strNewFilePath, true);

                        strNewFilePath = Constants.DOUBLE_QUOTES + strNewFilePath + Constants.DOUBLE_QUOTES;

                        /* Constructing tc cmd line Arguments for LMICMSUtils exe for Stylesheet export operation */
                        string strArguments = "LMICMSUTILS -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -mode=pff -operation=export -internalname=" + strPffUid + " -filepath=" + strNewFilePath;

                        /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                        strOutput = execute_Cmsutils_Exe.Process_Execute_cmsutils_exe(strArguments, strOutput);

                        strAppendedOutput = strAppendedOutput + strOutput;

                        if (bIsFirstQuery == true)
                        {
                            bIsFirstQuery = false;

                            /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Export operation */
                            Execute_File_Write.Process_Execute_File_Write(strFileName);
                        }

                        /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                        Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                        /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string LogFile containing logfilename along with location */
                        Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                    }
                }
                
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}

