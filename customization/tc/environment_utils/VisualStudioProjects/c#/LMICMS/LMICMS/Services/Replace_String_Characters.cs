﻿/*======================================================================================================================================================================
                Copyright 2020  LMtec India
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: Replace_String_Characters.cs

    Description: This file contains operations related to Replacing String Characters

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Text;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
  public class Replace_String_Characters
    {
        public static StringBuilder Process_Replace_String_Characters(StringBuilder stringbuilder)
        {
            try
            {
                string strSeperatorOne = Constants.SEP_DASH;

                string strSeperatorTwo = Constants.SEP_SPACE;

                string strSeperatorThree = Constants.SEP_DOT;

                string strSeperatorFour = Constants.SEP_SING_QUOTE;

                string strSeperatorFive = Constants.SEP_BRACES;

                string strReplacedSeperatorOne = Constants.SEP_UNDERSCORE;

                string strReplacedSeperatorTwo = Constants.SEP_NULL;

                stringbuilder.Replace(strSeperatorOne, strReplacedSeperatorTwo);

                stringbuilder.Replace(strSeperatorTwo, strReplacedSeperatorOne);

                stringbuilder.Replace(strSeperatorThree, strReplacedSeperatorTwo);

                stringbuilder.Replace(strSeperatorFour, strReplacedSeperatorOne);

                stringbuilder.Replace(strSeperatorFive, strReplacedSeperatorTwo);   
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                Logger.getInstance().error(ex.Message);
            }
            return stringbuilder;
        }
    }
}
