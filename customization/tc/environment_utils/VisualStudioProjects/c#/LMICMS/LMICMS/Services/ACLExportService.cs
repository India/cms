﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: ACLExportService.cs

Description: This file contains operations for exporting  ACL.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
    class ACLExportService
    {
        public static string strOutput = string.Empty;
        private Logger logger = Logger.getInstance();
        Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

        /*This function gets invoked to fulfill Export Operation Request to export ACL to a set location */
        public void ACLProcessExport( string strPath, string strAclName)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* To create .cmd file with Date Time stamp for Export ACL operation */
                string strFileName = Path.Combine(strTempDir, Constants.EXP_ACL + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create logfile at Tempfolder location for the export operation */
                string strLogFile = Path.Combine(strTempDir, Constants.EXPORT_ACL_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                if (String.IsNullOrEmpty(strAclName))
                {
                    /* File name for the operation consisting of Repository location where ACL export file has to be exported and File extension */
                    string strAclFile = strPath + Constants.FILE_PATH + Constants.TC_RULE_TREE + Constants.FILE_EXT_XML;

                    /* Constructin tc  cmd line Arguments for ACL export operation */
                    string strArguments = "am_install_tree -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -operation=export -path=" + strAclFile + " -format=xml";

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Export operation */
                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string LogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                }
                else
                {
                    strAclName = Path.GetFileNameWithoutExtension(strAclName);

                    /* User Input File name for the operation consisting of Repository location where ACL export file has to be exported and File extension */
                    string strInpAclFile = strPath + Constants.FILE_PATH + strAclName + Constants.FILE_EXT_XML;

                    /* Constructin tc  cmd line Arguments for ACL export operation */
                    string strArguments = "am_install_tree -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -operation=export -path=" + strInpAclFile + " -format=xml";

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Export operation */
                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string LogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);   
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}
