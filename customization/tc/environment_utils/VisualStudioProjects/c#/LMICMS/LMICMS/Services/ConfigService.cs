﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: ConfigService.cs

Description: This file contains operations for Configuration.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using LMICMS.Common.Utilities;
using LMICMS.Common.Constants;
using LMICMS.Settings;

namespace LMICMS.Services
{
    public class ConfigService
    {
        public static string strFolder = string.Empty;

        public static string strTempFolder
        {
            get { return strFolder; }
            set { strFolder = value; }
        }

        
        public static string strTempLogFile = string.Empty;
     

        public static bool ValidateConfigFile(string strConfigFilePath)
        {
            bool flag = false;

            LMICMSRoot objLMICms = XmlHelper.DeserializeXMLFileToObject<LMICMSRoot>(strConfigFilePath);
            if (objLMICms != null && objLMICms.ProjectsDetails!= null && objLMICms.ProjectsDetails.Projects.Count > 0)
            {
                flag = true;
            }
            return flag;
        }

        public static void AddProjectConfig(string strProjectName, string strProjectGuid, string strConfigName, string strRepoName, ref XmlElement projectsDetailsTag, ref XmlDocument xmlDoc)
        {
            string strNewProjectGuid = strProjectGuid;

            var projectTag = xmlDoc.CreateElement(ConfigConstants.TagProject);
            projectTag.SetAttribute(ConfigConstants.AttributeProjectGuid, strNewProjectGuid);
            projectTag.SetAttribute(ConfigConstants.AttributeProjectName, strProjectName);
            projectsDetailsTag.AppendChild(projectTag);

            string strConfigGuid = Guid.NewGuid().ToString();
            string strRepoGuid = Guid.NewGuid().ToString();

            var activeConfigGuidTag = xmlDoc.CreateElement(ConfigConstants.ElementActiveConfigGuid);
            activeConfigGuidTag.InnerText = strConfigGuid;
            projectTag.AppendChild(activeConfigGuidTag);
            var activeRepoGuidTag = xmlDoc.CreateElement(ConfigConstants.ElementActiveRepoGuid);
            activeRepoGuidTag.InnerText = strRepoGuid;
            projectTag.AppendChild(activeRepoGuidTag);

            var configsTag = xmlDoc.CreateElement(ConfigConstants.TagTCConfigurations);
            projectTag.AppendChild(configsTag);

            var configTag = xmlDoc.CreateElement(ConfigConstants.TagTCConfiguration);
            configTag.SetAttribute(ConfigConstants.AttributeConfigGuid, strConfigGuid);
            configTag.SetAttribute(ConfigConstants.AttributeConfigName, strConfigName);
            configsTag.AppendChild(configTag);

            var usersTag = xmlDoc.CreateElement(ConfigConstants.TagUsersDetails);
            projectTag.AppendChild(usersTag);

            var srcControlLocTag = xmlDoc.CreateElement(ConfigConstants.TagSrcControlLoc);
            projectTag.AppendChild(srcControlLocTag);
            var repoTag = xmlDoc.CreateElement(ConfigConstants.ElementRepo);
            repoTag.SetAttribute(ConfigConstants.AttributeRepoGuid, strRepoGuid);
            repoTag.SetAttribute(ConfigConstants.AttributeRepoName, strRepoName);
            repoTag.SetAttribute(ConfigConstants.AttributeRepoPath, string.Empty);
            srcControlLocTag.AppendChild(repoTag);

            /* Check for environment variables */
            string strTcRoot = EnvironmentHelper.GetEnvironmentVariable(ConfigurationManager.AppSettings[ConfigConstants.EnvTCRoot]);
            string strTcData = EnvironmentHelper.GetEnvironmentVariable(ConfigurationManager.AppSettings[ConfigConstants.EnvTCData]);

            if (!String.IsNullOrEmpty(strTcRoot))
            {
                var tcRootElement = xmlDoc.CreateElement(ConfigConstants.ElementTCRoot);
                tcRootElement.InnerText = strTcRoot;
                configTag.AppendChild(tcRootElement);
            }
            if (!String.IsNullOrEmpty(strTcData))
            {
                var tcDataElement = xmlDoc.CreateElement(ConfigConstants.ElementTCData);
                tcDataElement.InnerText = strTcData;
                configTag.AppendChild(tcDataElement);
            }

           
        }

        public static string ToIndentedString(XmlDocument doc, bool bWriteProcessing = false)
        {
            var stringWriter = new StringWriterWithEncoding(new StringBuilder(), Encoding.UTF8);
            var xmlTextWriter = new XmlTextWriter(stringWriter) { Formatting = Formatting.Indented };
            
            if (bWriteProcessing == true)
            {
                xmlTextWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='utf-8'");
            } 
            doc.Save(xmlTextWriter);
            return stringWriter.ToString();
        }

        public static bool SaveConfigFile(XmlDocument doc, bool bWriteProcessing = false, string strConfigFilePath = null, bool refresh = true)
        {
            bool flag = false;
            string strConfigXml = ToIndentedString(doc, bWriteProcessing);

            if (String.IsNullOrEmpty(strConfigFilePath))
            {
                strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
            }
            
            flag = FileHelper.SaveFile(strConfigFilePath, strConfigXml);

            if (flag == true && refresh == true)
            {
                RefreshConfiguration();
            }
            return flag;
        }

        public static void CreateConfigurationFile(string strConfigFilePath)
        {
            /* Create XML file */
            var xmlDoc = new XmlDocument();
            var rootTag = xmlDoc.CreateElement(ConfigConstants.TagConfigurationRoot);
            xmlDoc.AppendChild(rootTag);

            string strNewProjectGuid = Convert.ToString(Guid.NewGuid());

            var configFileLoc = xmlDoc.CreateElement(ConfigConstants.ElementConfigFileLoc);
            configFileLoc.InnerText = strConfigFilePath;
            rootTag.AppendChild(configFileLoc);
            var activeProEle = xmlDoc.CreateElement(ConfigConstants.ElementActiveProjectGuid);
            activeProEle.InnerText = strNewProjectGuid;
            rootTag.AppendChild(activeProEle);
            var tempDownPath = xmlDoc.CreateElement(ConfigConstants.ElementTempDownloadPath);
            rootTag.AppendChild(tempDownPath);
            var cmsExeLoc = xmlDoc.CreateElement(ConfigConstants.ElementCMSExeLoc);
            rootTag.AppendChild(cmsExeLoc);

            var projectsTag = xmlDoc.CreateElement(ConfigConstants.TagProjectsDetails);
            rootTag.AppendChild(projectsTag);

            string strProjectName = ConfigurationManager.AppSettings[ConfigConstants.DefaultProjectName];
            string strConfigName = ConfigurationManager.AppSettings[ConfigConstants.DefaultConfigName];
            string strRepoName = ConfigurationManager.AppSettings[ConfigConstants.DefaultRepoName];
            AddProjectConfig(strProjectName, strNewProjectGuid, strConfigName, strRepoName, ref projectsTag, ref xmlDoc);
    
            SaveConfigFile(xmlDoc, true, strConfigFilePath);        
        }

        public static void LoadConfiguration()
        {
            int iLogLevel = 0;
            string strEnvValue = string.Empty;
            Logger logger = Logger.getInstance();
            
            string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();

            LMICMSRoot objLMICms = XmlHelper.DeserializeXMLFileToObject<LMICMSRoot>(strConfigFilePath);
            ConfigConstants.LMICMSExeLoc = objLMICms.LMICMSExeLoc;
            ConfigConstants.LMITempDownloadPath = objLMICms.LMITempDownloadPath;

            var activeProject = objLMICms.ProjectsDetails.Projects.FirstOrDefault(project => String.Compare(project.ProjectGuid, objLMICms.ActiveProjectGuid) == 0);

            if (activeProject != null)
            {
                ConfigConstants.ActiveProjectGuid = activeProject.ProjectGuid;
                ConfigConstants.ActiveProjectName = activeProject.ProjectName;
                ConfigConstants.ActiveRepoPathGuid = activeProject.ActiveRepoGuid;
                ConfigConstants.ActiveRepoPath = activeProject.SRCControlLocation.RepoPaths.FirstOrDefault(repo => String.Compare(repo.RepoGuid, activeProject.ActiveRepoGuid) == 0).RepoPath;
                var activeConfiguration = activeProject.TCConfigurations.TCConfigs.FirstOrDefault(config => String.Compare(config.ConfigGuid, activeProject.ActiveConfigGuid) == 0);
                if (activeConfiguration != null)
                {
                    ConfigConstants.ActiveConfigGuid = activeConfiguration.ConfigGuid;
                    ConfigConstants.TCData = activeConfiguration.TCData;
                    ConfigConstants.TCRoot = activeConfiguration.TCRoot;

                    if (activeProject.TCUsersDetails.TCUsers.Count > 0)
                    {
                        var objActiveUser = activeProject.TCUsersDetails.TCUsers.FirstOrDefault(user => String.Compare(user.UserGuid, activeConfiguration.LMIActiveUserGuid) == 0);
                        if (objActiveUser != null)
                        {
                            ConfigConstants.UserGuid = objActiveUser.UserGuid;
                            ConfigConstants.UserGroup = objActiveUser.UserGroup;
                            ConfigConstants.UserName = objActiveUser.UserId;
                            ConfigConstants.UserPassword = objActiveUser.UserPassword;
                            ConfigConstants.UserRole = objActiveUser.UserRole;
                        }
                    }
                }
                if (activeProject.SRCControlLocation.RepoPaths.Count > 0)
                {
                    var objActiveRepo = activeProject.SRCControlLocation.RepoPaths.FirstOrDefault(repo => String.Compare(repo.RepoGuid, activeProject.ActiveRepoGuid) == 0);
                    if (objActiveRepo != null)
                    {
                        ConfigConstants.WorkflowPath = objActiveRepo.AppDataLocs.FirstOrDefault(loc=>String.Compare(loc.AppDataName, Constants.Workflow) == 0)?.AppDataPath;
                        ConfigConstants.AuthorizationPath = objActiveRepo.AppDataLocs.FirstOrDefault(loc => String.Compare(loc.AppDataName, Constants.Authorization) == 0)?.AppDataPath;
                        ConfigConstants.OrganizationPath = objActiveRepo.AppDataLocs.FirstOrDefault(loc => String.Compare(loc.AppDataName, Constants.Organization) == 0)?.AppDataPath;
                        ConfigConstants.ACLPath = objActiveRepo.AppDataLocs.FirstOrDefault(loc => String.Compare(loc.AppDataName, Constants.ACL) == 0)?.AppDataPath;
                        ConfigConstants.PFFPath = objActiveRepo.AppDataLocs.FirstOrDefault(loc => String.Compare(loc.AppDataName, Constants.PFF) == 0)?.AppDataPath;
                        ConfigConstants.StylesheetPath = objActiveRepo.AppDataLocs.FirstOrDefault(loc => String.Compare(loc.AppDataName, Constants.Stylesheet) == 0)?.AppDataPath;
                        ConfigConstants.PreferencePath = objActiveRepo.AppDataLocs.FirstOrDefault(loc => String.Compare(loc.AppDataName, Constants.Preference) == 0)?.AppDataPath;
                        ConfigConstants.QueryPath = objActiveRepo.AppDataLocs.FirstOrDefault(loc => String.Compare(loc.AppDataName, Constants.Query) == 0)?.AppDataPath;
                        ConfigConstants.TransferModePath = objActiveRepo.AppDataLocs.FirstOrDefault(loc => String.Compare(loc.AppDataName, Constants.TransferMode) == 0)?.AppDataPath;
                    }
                }
            }

            if (string.IsNullOrEmpty(ConfigConstants.LMITempDownloadPath))
            {
                string Todaysdate = DateTime.Now.ToString("dd-MM-yyyy");
                strFolder = Path.Combine(Environment.CurrentDirectory, Todaysdate);
                if (!Directory.Exists(strFolder))
                    Directory.CreateDirectory(strFolder);
            }
            else
            {
                string strFolderLoc = ConfigConstants.LMITempDownloadPath;
                string Todaysdate = DateTime.Now.ToString("dd-MM-yyyy");
                strFolder = Path.Combine(strFolderLoc, Todaysdate);
                if (!Directory.Exists(strFolder))
                    Directory.CreateDirectory(strFolder);
                string LMICMSFile = "LMICMS_Log" + DateTime.Now.ToString("_dd-MM-yyyy_hh-mm-ss");
                strTempLogFile = Path.Combine (strFolder + Constants.FILE_PATH + LMICMSFile) ;
                StreamWriter sw = File.CreateText(strTempLogFile);
            }

            strEnvValue = Environment.GetEnvironmentVariable(ConfigConstants.LMI_CMS_LOG_LEVEL);

            if (!string.IsNullOrEmpty(strEnvValue))
            {
                iLogLevel = Convert.ToInt32(strEnvValue);

                logger.init(iLogLevel, strTempLogFile);
            }
            else
            {
                logger.init(Constants.LOG_ERROR, strTempLogFile);
            }
        }

        public static void InitializeConfiguration()
        {
            string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
            if (!File.Exists(strConfigFilePath))
            {
                CreateConfigurationFile(strConfigFilePath);
            }
            LoadConfiguration();
        }

        public static void RefreshConfiguration()
        {
            string strConfigFilePath = LMIConfigHelper.GetConfigFilePath();
            if (!File.Exists(strConfigFilePath))
            {
                CreateConfigurationFile(strConfigFilePath);
            }
            LoadConfiguration();
        }
    }
}
