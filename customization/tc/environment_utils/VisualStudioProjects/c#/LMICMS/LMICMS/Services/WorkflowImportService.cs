﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: WorkflowImportService.cs

Description: This file contains operations related to Workflow Import.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Services
{
    class WorkflowImportService
    {
        public string strOutput = string.Empty;
        public static string strAppendedOutput = string.Empty;
        Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();
        private Logger logger = Logger.getInstance();

        /*This function gets invoked to fulfill Import Operation Request to import Workflows to a set location */
        public void WorkflowProcessImport(WorkflowImportModel WorkflowImportModel)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* To create .cmd file with Date Time stamp for Workflow Import operation */
                string strFileName = Path.Combine(strTempDir, Constants.IMP_WORKFLOW + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create logfile at Tempfolder location for the import operation */
                string strLogFile = Path.Combine(strTempDir, Constants.IMPORT_WORKFLOW_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                Boolean bIsFirstQuery = true;

                foreach (var item in WorkflowImportModel.workflownames)
                {
                    /* Constructing tc  cmd line Arguments for Workflow import operation  */
                    string strArguments = "plmxml_import -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -xml_file=" + Constants.DOUBLE_QUOTES + WorkflowImportModel.strFilePath + Constants.FILE_PATH + item + Constants.DOUBLE_QUOTES + " -transfermode=workflow_template_overwrite";

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    strAppendedOutput = strAppendedOutput + strOutput;

                    if (bIsFirstQuery == true)
                    {
                        bIsFirstQuery = false;

                        /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Import operation */
                        Execute_File_Write.Process_Execute_File_Write(strFileName);
                    }

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Import Operation and string LogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.ToString());
            }
        }
    }
}

