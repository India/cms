﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: TransferModeExportService.cs

Description: This file contains operations related to TransferMode Export

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using System.Linq;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Services
{
    class TransferModeExportService
    {
        public string strOutput = string.Empty;
        public static string strAppendedOutput = string.Empty;
        private Logger logger = Logger.getInstance();
        Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();

        /*This function gets invoked to fulfill Export Operation Request to export TransferMode to a set location */
        public void TransferModeProcessExport(TransferModeExportModel transferModeExportModel, string strPath)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* To create .cmd file with Date Time stamp for Export TransferMode operation */
                string strFileName = Path.Combine(strTempDir, Constants.EXP_TRANSFER_MODE + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create logfile at Tempfolder location for the export operation */
                string strLogFile = Path.Combine(strTempDir, Constants.EXPORT_TRANSFER_MODE_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                Boolean bIsFirstQuery = true;
                
                if(transferModeExportModel.exportedtransfermode!=null && transferModeExportModel.exportedtransfermode.Length > 0  && transferModeExportModel.transfermodeUID.Length > 0)
                { 
                    /* Tokenizing on the basis of comma and storing item values in part1 and part2 arrays */
                    string[] strPartOne = transferModeExportModel.exportedtransfermode.Split(new[] { Constants.COMMA_SEP }, StringSplitOptions.RemoveEmptyEntries);
                    string[] strPartTwo = transferModeExportModel.transfermodeUID.Split(new[] { Constants.COMMA_SEP }, StringSplitOptions.RemoveEmptyEntries);

                      for (int iDx = 0; iDx < strPartOne.Length && iDx< strPartTwo.Length; iDx++)
                      {
                        /* Storing part2 token value in string Value1 */
                        string strValUID = strPartTwo[iDx];

                        /* Storing part1 token value in string Value2 */
                        string strVal = strPartOne[iDx];

                        /* Enclosing string strValue2 within Double quotes */
                        string strValFile = Constants.DOUBLE_QUOTES + strPath + Constants.FILE_PATH + strVal + Constants.DOUBLE_QUOTES;

                        /* Constructin tc  cmd line Arguments for TransferMode export operation */
                        string strArguments = "tcxml_export -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -file=" + strValFile + Constants.FILE_EXT_XML +  " -uid=" + strValUID;

                        /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                        strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                        strAppendedOutput = strAppendedOutput + strOutput;

                        if (bIsFirstQuery == true)
                        {
                            bIsFirstQuery = false;

                            /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Export operation */
                            Execute_File_Write.Process_Execute_File_Write(strFileName);
                        }

                        /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                        Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                        /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string LogFile containing logfilename along with location */
                        Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                        /* Moves the specified file to new location with new file name */
                        File.Move(ConfigConstants.TransferModePath + Constants.FILE_PATH + strVal + Constants.EXPORTER_LOG, strTempDir + Constants.FILE_PATH  + strVal + Constants.TRANSFER_MODE_EXPORT_LOG);
                      }
                }  
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.ToString());
            }
        }
    }
}
