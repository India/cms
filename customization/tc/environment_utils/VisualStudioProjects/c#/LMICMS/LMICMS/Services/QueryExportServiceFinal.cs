﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: ExportServiceFinal.cs

Description: This file contains operations related to QueryExport.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using System.Text;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Services
{
    class QueryExportServiceFinal
    {
        string strText = string.Empty;
        public string strOutput = string.Empty;
        public static string strAppendedOutput = string.Empty;
        Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();
        private Logger logger = Logger.getInstance();
   
        /*This function gets invoked to fulfill Export Operation Request to export Queries to a set location */
        public void ProcessExportFinal(string strFinalFileName, string strQueryPath, string strFinalLog, QueryExportModel exportModel, string strTCConfigFileName)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* Storing String TCConfigFileName along with its full path in string File */
                string strFile = strTempDir + Constants.FILE_PATH + strTCConfigFileName;

                /* Initialize new instance of Filestream class with specified path, creation mode and read/write permission */
                var fileStream = new FileStream(strFile, FileMode.Open, FileAccess.Read);

                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    /* Read full file and store it in string Text */
                    strText = streamReader.ReadToEnd();
                }

                /* Tokenizing entire text on the basis of new line and storing it in array parts */
                string[] parts = strText.Split(new[] { Constants.NEW_LINE_ONE, Constants.NEW_LINE_TWO }, StringSplitOptions.RemoveEmptyEntries);

                int iDx = 0;
                int jDx = 0;

                Boolean bIsFirstQuery = true;

                for (iDx = 0, jDx = 1; iDx < parts.Length && jDx < parts.Length; iDx += 2, jDx += 2)
                {
                    /* Storing Token value in string ValueOne */
                    string strValueOne = parts[iDx];

                    /* Storing token in String ValueThree with full filepath and enclosed within Double Quotes */
                    string strValueThree = Constants.DOUBLE_QUOTES + strQueryPath + Constants.FILE_PATH + strValueOne + Constants.DOUBLE_QUOTES;

                    /* Storing Token Value in string ValueTwo */
                    string strValueTwo = parts[jDx];

                    /* Constructing tc  cmd line Arguments for Query export operation */
                    string strArguments = "plmxml_export -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -xml_file=" + strValueThree + Constants.FILE_EXT_XML + " -uid=" + strValueTwo;

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    strAppendedOutput = strAppendedOutput + strOutput;

                    if (bIsFirstQuery == true)
                    {
                        bIsFirstQuery = false;

                        /*Calling Class Execute_File_Write and passing string FinalFileName to write .cmd file for the Export operation */
                        Execute_File_Write.Process_Execute_File_Write(strFinalFileName);
                    }

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FinalFileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFinalFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string FinalLogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strFinalLog);
                }
            }
            catch(Exception ex)
            {
               /*Calling object logger and passing exception message to write it in Error log file */
               logger.error(ex.Message);
            }                
        }
    }
}


   

    
