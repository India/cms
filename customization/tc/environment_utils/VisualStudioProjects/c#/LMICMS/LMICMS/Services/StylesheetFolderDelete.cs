﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: StylesheetFolderDelete.cs

Description: This file contains operations related to Stylesheet Folder Deletion.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
    class StylesheetFolderDelete
    {
        public static void ProcessFolderDelete(string stylesheetPath, string strValue1)
        {
            string sourceDir = stylesheetPath + Constants.FILE_PATH + strValue1;
            string backupDir = ConfigConstants.StylesheetPath;
                
            try
            {
                string[] picList = Directory.GetFiles(sourceDir, Constants.FILE_EXT_ALL_XML);
                string[] txtList = Directory.GetFiles(sourceDir, Constants.FILE_EXT_ALL_XML);

                // Copy picture files.
                foreach (string f in picList)
                {
                    
                    // Remove path from the file name.
                    string fName = f.Substring(sourceDir.Length + 1);
                   
                    // Use the Path.Combine method to safely append the file name to the path.
                    // Will overwrite if the destination file already exists.
                    File.Copy(Path.Combine(sourceDir, fName), Path.Combine(backupDir, strValue1 + ".xml"), true);
                }

                // Copy text files.
                foreach (string f in txtList)
                {

                    // Remove path from the file name.
                    string fName = f.Substring(sourceDir.Length + 1);

                    try
                    {
                        // Will not overwrite if the destination file already exists.
                        File.Copy(Path.Combine(sourceDir, fName), Path.Combine(backupDir, strValue1 + ".xml"));
                    }

                    // Catch exception if the file was already copied.
                    catch (Exception ex)
                    {
                        Logger.getInstance().error(ex.Message);
                    }
                }

                // Delete source files that were copied.
                foreach (string f in txtList)
                {
                    File.Delete(f);
                }
                foreach (string f in picList)
                {
                    File.Delete(f);
                }
            }

            catch (Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
        }
    }
}
    


