﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: QueryName.cs

Description: This file contains operations related to QueryNames.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Text;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Modules.Configuration;

namespace LMICMS.Services
{
    class QueryName
    {
        string strOutput = string.Empty;
        public static List<QueryModel> lQueryNames = null;
        private Logger logger = Logger.getInstance(); 
        Execute_cmsutils_exe execute_Cmsutils_Exe = new Execute_cmsutils_exe();

       /* This function gets invoked to fulfill Refresh Operation Request to fetch Querynames from TC */  
        public void ProcessQueryName(string strFileName)
        {
            lQueryNames = new List<QueryModel>();

            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* Constructing tc cmd line Arguments for LMICMSUtils exe for Querynames refresh operation */
                string strArguments = "LMICMSUTILS -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -mode=query -operation=export_query_name -tcconfigfilename=" + strFileName + " -filepath=" + strTempDir;

                /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to LMICMSUtils exe */
                 execute_Cmsutils_Exe.Process_Execute_cmsutils_exe(strArguments, strOutput);

                /* Read Queryname file created at Temp Folder location line by line and store it as array filelines */
                string[] filelines = System.IO.File.ReadAllLines(strTempDir + Constants.FILE_PATH + strFileName);

                for (int iDx = 0; iDx < filelines.Length; iDx++)
                {
                    /* Initialize a new instance of StringBuilder Class using the specified string */
                    StringBuilder stringBuilder = new StringBuilder(filelines[iDx]);

                    /* Call function Process_Replace_String_Characters of Replace_String_Characters Class and pass stringBuilder to it */
                    stringBuilder = Replace_String_Characters.Process_Replace_String_Characters(stringBuilder);

                    /* Add QueryNames  and file names to List lQuerynames to populate in datagrid */
                    lQueryNames.Add(new QueryModel() { QueryName = filelines[iDx], FileName = stringBuilder.ToString() });
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }        
        }
    }
}

