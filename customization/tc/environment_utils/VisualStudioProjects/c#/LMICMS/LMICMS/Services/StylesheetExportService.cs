﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: StylesheetExportService.cs

Description: This file contains operations related to Stylesheet Export.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Services
{
    class StylesheetExportService
    {
        string strOutput = string.Empty;
        string strExpFileName = string.Empty;
        string strStylesheetName = string.Empty;
        Execute_cmsutils_exe execute_Cmsutils_Exe = new Execute_cmsutils_exe();
        StylesheetWriteFile stylesheetWriteFile = new StylesheetWriteFile();
        private Logger logger = Logger.getInstance();

        /*This function gets invoked to fulfill Export Operation Request to export Stylsheet to a set location */
        public void StylesheetProcessExport(StylesheetExportModel stylesheetExportModel, string strPath)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* To create .cmd file with Date Time stamp for Export Organisation operation */
                string strFileName = Path.Combine(strTempDir, Constants.EXP_STYLESHEET + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create File for storing selected Export StylesheetNames and UID's */
                string strTCConfigFileName = Constants.EXP_STYLESHEET + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT;

                /* To create logfile at Tempfolder location for the export operation */
                string strLogFile = Path.Combine(strTempDir, Constants.EXPORT_STYLESHEET_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                if(stylesheetExportModel.stylesheet!=null && stylesheetExportModel.stylesheet.Length > 0 && stylesheetExportModel.exportedstylesheet!=null && stylesheetExportModel.exportedstylesheet.Length > 0)
                    {
                        /* Remove Comma from end of the string Stylesheetname */
                        strStylesheetName = stylesheetExportModel.stylesheet.TrimEnd(Constants.COMMA_SEP);

                        /* Enclose the string Stylesheetname with Double Quotes */
                        strStylesheetName = Constants.DOUBLE_QUOTES + strStylesheetName + Constants.DOUBLE_QUOTES;

                        /* Remove Comma from end of the string Filename */
                        strExpFileName = stylesheetExportModel.exportedstylesheet.TrimEnd(Constants.COMMA_SEP);

                        /* Constructing tc cmd line Arguments for LMICMSUtils exe for Stylesheet export operation */
                        string strArguments = "LMICMSUTILS -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -mode=stylesheet -operation=export -internalname=" + strStylesheetName + " -exportfilename=" + strExpFileName + " -tcconfigfilename=" + strTCConfigFileName + " -filepath=" + strTempDir;

                        /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                        strOutput = execute_Cmsutils_Exe.Process_Execute_cmsutils_exe(strArguments, strOutput);

                        /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Import Operation and string LogFile containing logfilename along with location */
                        Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                        /* Calling Process_Execute_Stylesheet_Write_File function of class stylesheetWriteFile and passing internalname, exportfilename and str Path to it */
                        stylesheetWriteFile.Process_Execute_Stylesheet_Write_File(stylesheetExportModel.stylesheet, stylesheetExportModel.exportedstylesheet, strPath);

                        /* Initializing new instance of StylesheetExportServiceFinal */
                        StylesheetExportServiceFinal stylesheetExportServiceFinal = new StylesheetExportServiceFinal();

                        /* Calling StylesheetProcessExportFinal function of class stylesheetExportServiceFinal and passing strFileName, strPath, strLogFile, stylesheetExportModel, strTCConfigFileName to it */
                        stylesheetExportServiceFinal.StylesheetProcessExportFinal(strFileName, strPath, strLogFile, stylesheetExportModel, strTCConfigFileName);
                    }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}
