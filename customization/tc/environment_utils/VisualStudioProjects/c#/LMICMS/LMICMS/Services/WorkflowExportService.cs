﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: WorkflowExportService.cs

Description: This file contains operations related to Workflow Export.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Services
{
    class WorkflowExportService
    {
        private Logger logger = Logger.getInstance();
        public string strOutput = string.Empty;
        public static string strAppendedOutput = string.Empty;
        Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();
        
        /*This function gets invoked to fulfill Export Operation Request to export Workflows to a set location */
        public void WorkflowProcessExport(WorkflowExportModel workflowExportModel, string strPath)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* To create .cmd file with Date Time stamp for Export TransferMode operation */
                string strFileName = Path.Combine(strTempDir, Constants.EXP_WORKFLOW + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create logfile at Tempfolder location for the export operation */
                string strLogFile = Path.Combine(strTempDir, Constants.EXPORT_WORKFLOW_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                Boolean bIsFirstQuery = true;

                if(workflowExportModel.ExportWFNames != null && workflowExportModel.ExportWFNames.Length > 0  && workflowExportModel.WFName.Length > 0 )
                {
                     /* Tokenising strExpWFName on the basis of new line and storing it in array ExpWFNamepart */  
                    string[] ExpWFNamepart = workflowExportModel.ExportWFNames.Split(new[] { Constants.NEW_LINE_ONE, Constants.NEW_LINE_TWO }, StringSplitOptions.RemoveEmptyEntries);

                    /* Tokenising strWFName on the basis of new line and storing it in array WFNamepart */
                    string[] WFNamepart = workflowExportModel.WFName.Split(new[] { Constants.NEW_LINE_ONE, Constants.NEW_LINE_TWO }, StringSplitOptions.RemoveEmptyEntries);

                    for (int iDx = 0; iDx < ExpWFNamepart.Length && iDx < WFNamepart.Length; iDx++)
                    {
                        /* Storing token value ExpWFNamepart in string strValueOne */
                        string strValueOne = ExpWFNamepart[iDx];

                        /* Storing token value WFNamepart in string strValueTwo */
                        string strValueTwo = WFNamepart[iDx];
 
                        /* Storing string strValueOne in string strValueThree enclosed within double quotes */
                        string strValueThree = Constants.DOUBLE_QUOTES + strPath + Constants.FILE_PATH + strValueOne + Constants.DOUBLE_QUOTES;

                        /* Constructin tc  cmd line Arguments for TransferMode export operation */
                        string strArguments = "plmxml_export -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -template=" + Constants.DOUBLE_QUOTES +  strValueTwo + Constants.DOUBLE_QUOTES + " -xml_file=" + strValueThree + Constants.FILE_EXT_XML;

                        /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                        strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                        strAppendedOutput = strAppendedOutput + strOutput;

                        if (bIsFirstQuery == true)
                        {
                            bIsFirstQuery = false;

                            /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Export operation */
                            Execute_File_Write.Process_Execute_File_Write(strFileName);
                        }
                        /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                        Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                        /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string LogFile containing logfilename along with location */
                        Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);
                    }
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}
