﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Execute_cmsutils_exe.cs

Description: This file contains operations related to LMICMSUTILS.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System.Diagnostics;
using LMICMS.Common.Constants;

namespace LMICMS.Services
{
    class Execute_cmsutils_exe
    {
        public static string strError = string.Empty;

        /*  This function is entry point to process Refresh opertaion,This function gets triggered when user selects Refresh/Load operation at start */
        public string Process_Execute_cmsutils_exe(string strArguments, string strOutput)
        {
            /*  Creating instance of new process, This is done because the export/import operations are run on command line , for this we have to launch command promt on shell
             *  For launchinf command promt on shell we have to first create a process and then we assign cmd.exe to that process  
            */
            Process process = new Process();

            /*  Assigning cmd.exe to process */
            process.StartInfo.FileName = Constants.CMD_PROMPT;

            /*  Process Constant to run process in background i.e BLACK window is not shown to user */
            process.StartInfo.CreateNoWindow = true;

            /*  Process Constant for provding inputs to shell */
            process.StartInfo.RedirectStandardInput = true;

            /*  Process Constant for collecting standard Output */
            process.StartInfo.RedirectStandardOutput = true;

            /*  Process Constant for collecting standard Error */
            process.StartInfo.RedirectStandardError = true;

            process.StartInfo.UseShellExecute = false;

            /*  Starting Process */
            process.Start();

            process.StandardInput.WriteLine(ConfigConstants.SET_ECHO_OFF);

            /*  TC_ROOT Environement variable */
            process.StandardInput.WriteLine(ConfigConstants.SET_TC_ROOT + ConfigConstants.TCRoot);

            /*  TC_Data Environment variable */
            process.StandardInput.WriteLine(ConfigConstants.SET_TC_DATA + ConfigConstants.TCData);

            /*  TC_Profile vars */
            process.StandardInput.WriteLine(ConfigConstants.TC_DATA_TC_PROFILEVARS);

            process.StandardInput.WriteLine(Constants.EXECUTE_LMICMSUTILS + ConfigConstants.LMICMSExeLoc);

            /* Executing Command */
            process.StandardInput.WriteLine(strArguments);

            /*  Flushing process Buffer */
            process.StandardInput.Flush();

            process.StandardInput.Close();

            /*  Waiting for process to exit */
            process.WaitForExit();

            /* Read the Standard Output and store it in String Output */
            strOutput = process.StandardOutput.ReadToEnd();

            /* Read the Standard Error and store it in String Error */
            strError = process.StandardError.ReadToEnd();

            /* Return string Output along with String error */
            return strOutput + strError;
        }
    }
}

