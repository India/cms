﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: OrganisationImportService.cs

Description: This file contains operations related to Organisation Import.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
    class OrganisationImportService
    {
        string strFilePath = string.Empty;
        public static string strOutput = string.Empty;
        Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();
        private Logger logger = Logger.getInstance();
        
        /*This function gets invoked to fulfill Import Operation Request to import Organisation to a set location */
        public void OrganisationProcessImport(string strLoc, string strNewFileName)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                if (strNewFileName.Length > 0)
                {
                    /* Creating object of OrganisationCleanService */
                    OrganisationCleanService organisationCleanService = new OrganisationCleanService();

                    /* Getting location of string strLoc and storing in strFilepath */ 
                   // strFilePath = Path.GetDirectoryName(strLoc);

                    /* Accessing ProcessOrganisationClean function of organisationCleanService and passing string Filepath, string NewFileName and string Loc to it */
                  // organisationCleanService.ProcessOrganisationClean(strFilePath, strNewFileName, strLoc);

                    organisationCleanService.ProcessOrganisationClean(strTempDir, strNewFileName, strLoc);
                }
                
                /* To create .cmd file with Date Time stamp for Organisation Import operation */
                string strFileName = Path.Combine(strTempDir, Constants.IMP_ORGANISATION + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_CMD);

                /* To create logfile at Tempfolder location for the import operation */
                string strLogFile = Path.Combine(strTempDir, Constants.IMPORT_ORGANISATION_LOG + DateTime.Now.ToString(Constants.TIME_STAMP) + Constants.FILE_EXT_TXT);

                if (strNewFileName.Length > 0)
                {
                    /* Constructing tc  cmd line Arguments for Organization import operation when Cleaned Operation is selected */
                    string strTcArguments = "dsa_util -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -f=import  -filename=" + strTempDir + Constants.FILE_PATH + strNewFileName;

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strTcArguments, strOutput);

                    /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Import operation */
                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strTcArguments, strFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Import Operation and string LogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                    foreach (var file in Directory.GetFiles(ConfigConstants.OrganizationPath, Constants.ALL_EXT_LOG))
                    {
                        /* Getting and storing log  file name in string Filename */
                        string strFilename = Path.GetFileName(file);

                         /* Moving selected file from Repository Location to Temp Folder Location */
                        File.Move(file, strTempDir + Constants.FILE_PATH + strFilename);
                    }
                }
                else
                {
                     /* Constructing tc  cmd line Arguments for Organization import operation when Cleaned Operation is not selected */
                    string strArguments = "dsa_util -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -f=import  -filename=" + strLoc;

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    /*Calling Class Execute_File_Write and passing string FileName to write .cmd file for the Import operation */
                    Execute_File_Write.Process_Execute_File_Write(strFileName);

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Import Operation and string LogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strLogFile);

                    foreach (var file in Directory.GetFiles(ConfigConstants.OrganizationPath, Constants.ALL_EXT_LOG))
                    {
                        /* Getting and storing log file name in string Filename */
                        string filename = Path.GetFileName(file);

                        /* Moving selected file from Repository Location to Temp Folder Location */
                        File.Move(file, strTempDir + Constants.FILE_PATH + filename);
                    }
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                    logger.error(ex.Message);
            }
        }    
    }
}
