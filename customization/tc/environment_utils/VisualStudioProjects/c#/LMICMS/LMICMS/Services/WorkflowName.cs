﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: WorkflowName.cs

Description: This file contains operations related to Workflow Names

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Text;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.Modules.Configuration;

namespace LMICMS.Services
{
    class WorkflowName
    {
        string strOutput = string.Empty;
        private Logger logger = Logger.getInstance();
        public static List<WorkflowModel> lWorkflowNames = null;
        Execute_cmsutils_exe execute_Cmsutils_Exe = new Execute_cmsutils_exe();

        /* This function gets invoked to fulfill Refresh Operation Request to fetch Workflownames from TC */
        public void ProcessWorkflowName(string strFileName)
        {
            lWorkflowNames = new List<WorkflowModel>();

            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* Constructing tc cmd line Arguments for LMICMSUtils exe for Workflownames refresh operation */
                string strArguments = "LMICMSUTILS -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -mode=workflow -operation=export_workflow_name -tcconfigfilename=" + strFileName + " -filepath=" + strTempDir;

                /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to LMICMSUtils exe */
                execute_Cmsutils_Exe.Process_Execute_cmsutils_exe(strArguments, strOutput);

                /* Read Stylesheetname file created at Temp Folder location line by line and store it as array filelines */
                string[] filelines = System.IO.File.ReadAllLines(strTempDir + Constants.FILE_PATH + strFileName);

                for (int iDx = 0; iDx < filelines.Length; iDx++)
                {
                    /* Initialize a new instance of StringBuilder Class */
                    StringBuilder stringbuilder = new StringBuilder(filelines[iDx]);

                    /* Call function Process_Replace_String_Characters of Replace_String_Characters Class and pass stringBuilder to it */
                    stringbuilder = Replace_String_Characters.Process_Replace_String_Characters(stringbuilder);

                    /* Add WorkflowNames  and file names to list lWorkflowNames  to populate in datagrid */
                    lWorkflowNames.Add(new WorkflowModel() { WorkflowName = filelines[iDx], FileName = stringbuilder.ToString() });
                }
            }
            catch (Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }
        }
    }
}

