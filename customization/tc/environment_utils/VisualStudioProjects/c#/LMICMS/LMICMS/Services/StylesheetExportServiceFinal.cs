﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: StylesheetExportServiceFinal.cs

Description: This file contains operations related to Stylesheet Export
========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using System.Text;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;
using LMICMS.ReqResp;

namespace LMICMS.Services
{
    class StylesheetExportServiceFinal
    {
        string strText = string.Empty;
        public string strOutput  = string.Empty;
        public static string strAppendedOutput = string.Empty;
        public static string strUnusedFilenames = string.Empty;
        Execute_tc_exe execute_Tc_Exe = new Execute_tc_exe();
        private Logger logger = Logger.getInstance();
      
        /*This function gets invoked to fulfill Export Operation Request to export Stylesheet to a set location */
        public void StylesheetProcessExportFinal(string strFinalFileName, string strStylesheetPath, string strFinalLog, StylesheetExportModel stylesheetExportModel, string strTCConfigFileName)
        {
            try
            {
                /* To access the location of Temp folder and storing it in string TempDir */
                string strTempDir = ConfigService.strTempFolder;

                /* Storing String TCConfigFileName along with its full path in string File */
                string strFile = strTempDir + Constants.FILE_PATH + strTCConfigFileName;

                /* To create logfile at Tempfolder location for the export operation */
                string strLog = strTempDir + Constants.FILE_PATH + Constants.STYLESHEET_EXPORT_LOG;

                /* Initialising new Instance of DirectoryInfo with StylesheetPath */
                DirectoryInfo directory = new DirectoryInfo(ConfigConstants.StylesheetPath);

                /* Get all files of file ext .xml and store as array */
                FileInfo[] files = directory.GetFiles(Constants.FILE_EXT_ALL_XML);

                foreach (var file in files)
                {
                    /* constructing string Filenames with file and New line */
                    strUnusedFilenames = strUnusedFilenames + file + Environment.NewLine;
                }

                /* Initializes new instance of FileStream class with specified path , creation mode and read write permission */
                var fileStream = new FileStream(strFile, FileMode.Open, FileAccess.Read);

                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    /* Read file FileStream and store it in string Text */
                    strText = streamReader.ReadToEnd();
                }

                /* Tokenizing string Text with new line seperator and storing it in array parts */
                string[] parts = strText.Split(new[] { Constants.NEW_LINE_ONE, Constants.NEW_LINE_TWO }, StringSplitOptions.RemoveEmptyEntries);

                int iDx = 0;
                int jDx = 0;

                Boolean bIsFirstQuery = true;

                for (iDx = 0, jDx = 1; iDx < parts.Length && jDx < parts.Length; iDx += 2, jDx += 2)
                {
                    /* Storing Token value in string ValueOne */
                    string strValueOne = parts[iDx];

                    /* Storing token in String ValueThree with full filepath and enclosed within Double Quotes */
                    string strValueThree = Constants.DOUBLE_QUOTES + strStylesheetPath + Constants.FILE_PATH + strValueOne + Constants.DOUBLE_QUOTES;

                    /* Storing Token Value in string ValueTwo */
                    string strValueTwo = parts[jDx];

                    /* Constructing tc  cmd line Arguments for Stylesheet export operation */
                    string strArguments = "plmxml_export -u=" + ConfigConstants.UserName + " -p=" + ConfigConstants.UserPassword + " -g=" + ConfigConstants.UserGroup + " -xml_file=" + strValueThree + Constants.FILE_EXT_XML + " -transfermode=ConfiguredDataFilesExportDefault" + " -uid=" + strValueTwo + " -log=" + strLog;

                    /*Calling Teamcenter cmd line process and passing tc cmd line Arguments to it */
                    strOutput = execute_Tc_Exe.Process_Execute_tc_exe(strArguments, strOutput);

                    strAppendedOutput = strAppendedOutput + strOutput;

                    if (bIsFirstQuery == true)
                    {
                        bIsFirstQuery = false;

                        /*Calling Class Execute_File_Write and passing string FinalFileName to write .cmd file for the Export operation */
                        Execute_File_Write.Process_Execute_File_Write(strFinalFileName);
                    }

                    /* Calling  Execute_File_Write_Arguments and passing tc cmd line aruments along with string FinalFileName to it */
                    Execute_File_Write_Arguments.Process_Execute_File_Write_Arguments(strArguments, strFinalFileName);

                    /* Calling Execute_Log_Write_File class and passing string Output containing tc cmd line output for Export Operation and string FinalLogFile containing logfilename along with location */
                    Execute_Log_Write_File.Process_Execute_Log_Write_File(strOutput, strFinalLog);

                    /* Performing Delete operation to delete file at set location */
                    File.Delete(strStylesheetPath + Constants.FILE_PATH + strValueOne + Constants.FILE_EXT_XML);

                    //Calling   ProcessFolder delete function of  StylesheetFolderDelete class and passing strStylesheetPath, strValueOne to it */
                    StylesheetFolderDelete.ProcessFolderDelete(strStylesheetPath, strValueOne);

                    /*Performing Directory delete operation to delete directory */
                    Directory.Delete(strStylesheetPath + Constants.FILE_PATH + strValueOne);
                }
            }
            catch(Exception ex)
            {
                /* Calling object logger and passing exception message to write it in Error log file */
                logger.error(ex.Message);
            }  
        }
    }
}
