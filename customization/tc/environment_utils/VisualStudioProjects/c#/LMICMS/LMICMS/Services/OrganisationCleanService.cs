﻿/*======================================================================================================================================================================
                                    Copyright 2020  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================
File Description:

Filename: OrganisationCleanService.cs

Description: This file contains operations related to Organisation Clean.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using System.Xml;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
    class OrganisationCleanService
    {
        Logger objLog = null;
        public static string strOutput       = string.Empty;
        public static string strOutputString = string.Empty;
        FileNameUtils objFNameUtils  = new FileNameUtils();
        

        public void ProcessOrganisationClean(string strOutFilePath, string strFileName, string strInpFilePath)
        {
            String strLogFilePath = string.Empty;
            String strLogFileName = string.Empty;
            try
            {
                /*  Constructing Log File Name */
                strLogFileName = objFNameUtils.constructGenericFileNames("Log_Clean_", Constants.FEX_LOG);

                /*  Constructing Log File Path */
                strLogFilePath = strOutFilePath + Constants.FILE_PATH + strLogFileName;

                /*  Initializing Logger*/
                objLog = Logger.getInstance();

                objLog.init(Constants.LOG_VERBOSE, strLogFilePath);

                if (strInpFilePath != null && strInpFilePath.Length > 0 && strOutFilePath != null && strOutFilePath.Length > 0 && strFileName != null && strFileName.Length > 0)
                {
                   // objLog.info("Starting Clean Operation");

                    cleanOrgMain(strInpFilePath, strOutFilePath, strFileName);
                }
            }
            catch (Exception exException)
            {
               
            }
        }

        /*  Entry point function for cleaning organisation, The Organization file is cleaned in below order
         *  1) Site ID clearing
         *  2) Person Tag removal
         *  3) Organization Member Removal
         *  4) User Tag Removal
         *  5) Header Node Cleared, unnecessary header ID's removed
         *  6) Date, Time, Schema Version, Author clearing from TOP PLMXML tag
         *  */
        public void cleanOrgMain(string strInpFilePath, string strOutFile, string strFileName)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                /*  Reading Input XML File */
                xmlDoc.Load(strInpFilePath);

                if (xmlDoc != null)
                {
                    XmlNamespaceManager xnsManager = new XmlNamespaceManager(xmlDoc.NameTable);

                    xnsManager.AddNamespace(Constants.XML_NS_NAME, Constants.XML_NS);

                    string strOrgMEMstartID = "";

                    cleanSiteIdInfo(ref xmlDoc, ref xnsManager);

                    cleanPersonsTag(ref xmlDoc, ref xnsManager);

                    strOrgMEMstartID = cleanOrgMemberTag(ref xmlDoc, ref xnsManager);

                    cleanUsersTag(ref xmlDoc, ref xnsManager);

                    cleanHeaderNode(ref xmlDoc, strOrgMEMstartID, ref xnsManager);

                    cleanPLMXMLTag(ref xmlDoc, ref xnsManager);

                    objLog.info("Clean Operation Completed");

                    /* Create Output File path */
                    string strtemp = strOutFile;

                    strtemp.Replace('\\', '/');

                    if (Path.HasExtension(strFileName))
                    {
                        xmlDoc.Save(strOutFile + Constants.FILE_PATH + strFileName);

                        strOutputString = string.Empty;

                        strOutputString = "operation successfully completed" + strOutFile + strFileName;
                    }
                    else
                    {
                        /*  Saving Output File */
                        xmlDoc.Save(strOutFile + Constants.FILE_PATH + strFileName + Constants.FILE_EXT_XML);

                        strOutputString = string.Empty;

                        strOutputString = "operation successfully completed" + strOutFile + strFileName;
                    }
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        /*  Clean Site ID information */
        public void cleanSiteIdInfo(ref XmlDocument xmlDoc, ref XmlNamespaceManager xnsManager)
        {
            if (xmlDoc != null)
            {
                objLog.info("Cleaning Site ID Information");

                /* Removing UserData Tag containing site Id */
                XmlNodeList nodeUserData = xmlDoc.DocumentElement.SelectNodes(Constants.TAG_USER_DATA, xnsManager);

                for (int iDx = 0; iDx < nodeUserData.Count; iDx++)
                {
                    XmlNode tempNode = nodeUserData[iDx];

                    /*  The first user Data tag contains site ID information */
                    if (iDx == 0)
                    {
                        if (tempNode != null)
                        {
                            objLog.info("Removed Node " + tempNode.Name + " containing site id information");

                            tempNode.ParentNode.RemoveChild(tempNode);

                            break;
                        }
                    }
                }
            }
        }

        /*  Clean Person information */
        public void cleanPersonsTag(ref XmlDocument xmlDoc, ref XmlNamespaceManager xnsManager)
        {
           // objLog.info("Cleaning Person Information");

            /* Removing Person Tags */
            XmlNodeList nodePersons = xmlDoc.DocumentElement.SelectNodes(Constants.TAG_PERSONS, xnsManager);

            if (nodePersons.Count == 0)
            {
               // objLog.info("No Person Information Exists in File");
            }

            foreach (XmlNode node in nodePersons)
            {
                node.ParentNode.RemoveChild(node);
            }
        }

        /*  Clean Organisation Tag*/
        public string cleanOrgMemberTag(ref XmlDocument xmlDoc, ref XmlNamespaceManager xnsManager)
        {
            int iIndex = 0;
            string strFirstMemID = null;

         //   objLog.info("Cleaning Organization Member tag");

            XmlNodeList nodOrgMem = xmlDoc.DocumentElement.SelectNodes(Constants.TAG_ORG_MEM, xnsManager);

            if (nodOrgMem.Count == 0)
            {
               // objLog.info("No Organization Member tag Exists in File");
            }

            foreach (XmlNode node in nodOrgMem)
            {
                if (iIndex == 0)
                {
                    /*  Getting 1st Organisation Member ID because it needs to be removed from traverseRootRefs attribute in header */
                    strFirstMemID = node.Attributes["id"].Value;

                //    objLog.info("First Organization member ID is " + strFirstMemID);

                    iIndex++;
                }

                node.ParentNode.RemoveChild(node);
            }

            return strFirstMemID;
        }

        /*  Clean Users Information */
        public void cleanUsersTag(ref XmlDocument xmlDoc, ref XmlNamespaceManager xnsManager)
        {
            //objLog.info("Cleaning User Information");

            /* Removing Users */
            XmlNodeList nodeUsers = xmlDoc.DocumentElement.SelectNodes(Constants.TAG_USER, xnsManager);

            if (nodeUsers.Count == 0)
            {
                objLog.info("No User Information Exists in File");
            }

            foreach (XmlNode node in nodeUsers)
            {
                node.ParentNode.RemoveChild(node);
            }
        }

        /*  Clean Header Nodes */
        public void cleanHeaderNode(ref XmlDocument xmlDoc, string strFirstMemID, ref XmlNamespaceManager xnsManager)
        {
            objLog.info("Cleaning Header ID's");

            /*  Deleting id's from traverse RootRefs from Headers, All ID's from Firts Organization Member ID i.e strHeaderID to end are removed */
            XmlNodeList nodeHeader = xmlDoc.DocumentElement.SelectNodes(Constants.TAG_HEADER, xnsManager);

            if (nodeHeader != null && nodeHeader[0] != null)
            {
                string strHeaderID = nodeHeader[0].Attributes[Constants.ATTR_TRAVERSE_ROOT_REFS].Value;

                if (strFirstMemID != null)
                {
                    int iHeaderIndex = strHeaderID.IndexOf(strFirstMemID);

                    string strTemp = strHeaderID.Substring(0, iHeaderIndex - 2);

                    nodeHeader[0].Attributes[Constants.ATTR_TRAVERSE_ROOT_REFS].Value = strTemp;

                    objLog.info("New Header ID's are " + strTemp);

                }
                else
                {
                    objLog.info("Heaser ID's already cleaned ");
                }
                /* Delete TransferContext attribute if Present */
                XmlAttribute xATTR = nodeHeader[0].Attributes[Constants.ATTR_TRANSFER_CONTEXT];

                nodeHeader[0].Attributes.Remove(xATTR);
            }
        }

        /*  Cleaning PLMXML tag */
        public void cleanPLMXMLTag(ref XmlDocument xmlDoc, ref XmlNamespaceManager xnsManager)
        {
            objLog.info("Cleaning PLM XML TAG, removing time, schema vaersion, author, and date info");

            /*  Deleting id's from traverse RootRefs from Headers */
            XmlNodeList nodePLMXML = xmlDoc.DocumentElement.SelectNodes(Constants.TAG_PLMXML, xnsManager);

            if (nodePLMXML != null && nodePLMXML[0] != null)
            {
                /* Extra attributes from Header node, time date, schema version and author */
                XmlAttribute xTime = nodePLMXML[0].Attributes[Constants.ATTR_TIME];
                XmlAttribute xSchemaVer = nodePLMXML[0].Attributes[Constants.ATTR_SCHEMA_VERSION];
                XmlAttribute xAuthor = nodePLMXML[0].Attributes[Constants.ATTR_AUTHOR];
                XmlAttribute xDate = nodePLMXML[0].Attributes[Constants.ATTR_DATE];

                if (xTime != null)
                {
                    nodePLMXML[0].Attributes.Remove(xTime);
                }
                if (xSchemaVer != null)
                {
                    nodePLMXML[0].Attributes.Remove(xSchemaVer);
                }
                if (xAuthor != null)
                {
                    nodePLMXML[0].Attributes.Remove(xAuthor);
                }
                if (xDate != null)
                {
                    nodePLMXML[0].Attributes.Remove(xDate);
                }
            }
        }
    }
}
