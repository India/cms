﻿/*======================================================================================================================================================================
                                            Copyright 2020  LMtec India
                                            Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Execute_File_Write.cs

Description: This file contains operations for writing File.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Constants;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
    class Execute_File_Write
    {
        public static void Process_Execute_File_Write(string strFileName)
        {
            try
            {
                File.AppendAllText(strFileName, ConfigConstants.SET_TC_ROOT + ConfigConstants.TCRoot + Environment.NewLine);

                File.AppendAllText(strFileName, ConfigConstants.SET_TC_DATA + ConfigConstants.TCData + Environment.NewLine);

                File.AppendAllText(strFileName, ConfigConstants.TC_DATA_TC_PROFILEVARS + Environment.NewLine);
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
        }
    }
}
