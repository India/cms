﻿using LMICMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMICMS.Services
{
    public class StylesheetFileWrite
    {
        public static void Process_Execute_Stylesheet_Write_File(string str, string str2, string strPath)
        {
            str = str.Replace("\"", "");
            string[] parts1 = str.Split(new[] { Constants.SEP_COMMA }, StringSplitOptions.RemoveEmptyEntries);
            string[] parts2 = str2.Split(new[] { Constants.SEP_COMMA }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < parts1.Length && i < parts2.Length; i++)
            {


                string Final = string.Concat(parts1[i], ",", " ", parts2[i] + Constants.FILE_EXT_XML);
                StreamWriter writer = new StreamWriter(strPath + Constants.FILE_PATH + "StylesheetName.txt", true);

                // writer.WriteLine();

                writer.Write(Final + Environment.NewLine);

                //  writer.WriteLine(Constants.NEW_LINE_SEP);

                writer.Close();
            }
        }
    }
}
