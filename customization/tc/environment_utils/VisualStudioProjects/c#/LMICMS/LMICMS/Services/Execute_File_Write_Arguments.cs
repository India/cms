﻿/*======================================================================================================================================================================
                                        Copyright 2020  LMtec India
                                        Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: Execute_File_Write_Arguments.cs

Description: This file contains operations related to file write.

========================================================================================================================================================================

Date				Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-July-2020      Mohit Singh	           Initial Release

========================================================================================================================================================================*/
using System;
using System.IO;
using LMICMS.Common.Utilities;

namespace LMICMS.Services
{
  public class Execute_File_Write_Arguments
    {
        public static void Process_Execute_File_Write_Arguments(string strArguments, string strFileName)
        {
            try
            {
                File.AppendAllText(strFileName, strArguments + Environment.NewLine);
            }
            catch(Exception ex)
            {
                Logger.getInstance().error(ex.Message);
            }
        }
    }
}
